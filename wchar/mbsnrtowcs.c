/* Convert a multi-byte string to a wide character string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"
#include "small.h"
#include "utf8-ucs4.h"

size_t
gl_mbsnrtowcs (wchar_t *restrict dest, const char **restrict srcp, size_t nms, size_t len,
	       mbstate_t *restrict ps, gl_locale_t locale)
{
  gl_mbstate_t *pstate =
    (ps != NULL
     ? (gl_mbstate_t *) ps
     : &_gl_thread_specific (locale)->state_for_mbsnrtowcs);
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  iconv_t cd = facet->cd_to_utf8;
  const char *src = *srcp;
  size_t result;

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    {
      gl_lock_lock (facet->iconv_to_lock);
      iconv (cd, NULL, NULL, NULL, NULL);
    }

  if (dest != NULL)
    {
      wchar_t *destptr = dest;
      for (; ; destptr++, len--)
	if (nms > 0 && len > 0)
	  {
	    char buf[2*GL_MB_LEN_MAX];
	    size_t incoming =
	      gl_strnlen1 (src, nms < GL_MB_LEN_MAX ? nms : GL_MB_LEN_MAX);
	    size_t old = pstate->mbcount;
	    unsigned int uc;
	    memcpy_small (buf, pstate->mbuf, old);
	    memcpy_small (buf + old, src, incoming);
	    result = facet->mbtouc (&uc, buf, old + incoming, old + 1, cd);
	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      {
		errno = EILSEQ;
		 break;
	      }
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence.  */
	      {
		/* We must have old + incoming < GL_MB_LEN_MAX.  */
		if (old + incoming >= GL_MB_LEN_MAX)
		  abort ();
		memcpy_small (pstate->mbuf + old, src, incoming);
		pstate->mbcount = old + incoming;
		src += incoming;
		result = destptr - dest;
		break;
	      }
	    else
	      {
		/* Successful conversion to UCS-4.  */
		size_t inbytes = result - old;
		src += inbytes;
		nms -= inbytes;
		pstate->mbcount = 0;
		*destptr = uc;
		if (uc == 0)
		  {
		    src = NULL;
		    result = destptr - dest;
		    break;
		  }
	      }
	  }
	else
	  {
	    result = destptr - dest;
	    break;
	  }
    }
  else
    {
      /* Ignore dest and len.  */
      size_t totalcount = 0;
      for (; ; totalcount++)
	if (nms > 0)
	  {
	    char buf[2*GL_MB_LEN_MAX];
	    size_t incoming =
	      gl_strnlen1 (src, nms < GL_MB_LEN_MAX ? nms : GL_MB_LEN_MAX);
	    size_t old = pstate->mbcount;
	    unsigned int uc;
	    memcpy_small (buf, pstate->mbuf, old);
	    memcpy_small (buf + old, src, incoming);
	    result = facet->mbtouc (&uc, buf, old + incoming, old + 1, cd);
	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      {
		errno = EILSEQ;
		break;
	      }
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence.  */
	      {
		/* We must have old + incoming < GL_MB_LEN_MAX.  */
		if (old + incoming >= GL_MB_LEN_MAX)
		  abort ();
		memcpy_small (pstate->mbuf + old, src, incoming);
		pstate->mbcount = old + incoming;
		src += incoming;
		result = totalcount;
		break;
	      }
	    else
	      {
		/* Successful conversion to UCS-4.  */
		size_t inbytes = result - old;
		src += inbytes;
		nms -= inbytes;
		pstate->mbcount = 0;
		if (uc == 0)
		  {
		    src = NULL;
		    result = totalcount;
		    break;
		  }
	      }
	  }
	else
	  {
	    result = totalcount;
	    break;
	  }
    }

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    gl_lock_unlock (facet->iconv_to_lock);

  *srcp = src;
  return result;
}

#endif
