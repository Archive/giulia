/* Determination of the screen width of a wide character string.
   Copyright (C) 1999, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>

#if GLOCALE_ENABLE_WIDE_CHAR

int
gl_wcswidth (const wchar_t *s, size_t n, gl_locale_t locale)
{
  int count = 0;
  for (; n > 0; s++, n--)
    {
      wchar_t c = *s;
      if (c == (wchar_t)'\0')
	break;
      {
	int width = gl_wcwidth (c, locale);
	if (width < 0)
	  return -1;
	count += width;
      }
    }
  return count;
}

#endif
