/* Conversion from Unicode to a single 'char'.
   Copyright (C) 1999, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>
#include "glocale-private.h"

#include <stdlib.h>
#include "ucs4-utf8.h"

int
_gl_wctob (unsigned int c, gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;

  if (facet->is_utf8)
    {
      /* Shortcut for UTF-8.  */
      if (c < 0x80)
	return c;
      else
	return EOF;
    }
  else
    {
      int value = EOF;
      iconv_t cd = facet->cd_from_utf8;

      if (cd != (iconv_t)(-1))
	{
	  /* Convert c to UTF-8.  */
	  unsigned char buf[6];
	  int bufcount;

	  bufcount = u8_uctomb (buf, c, sizeof (buf));
	  if (bufcount >= 0)
	    {
	      /* Convert it to facet->encoding.  */
	      char out[64];

	      gl_lock_lock (facet->iconv_from_lock);

	      iconv (cd, NULL, NULL, NULL, NULL);
	      {
		const char *inbuf = (const char *) buf;
		size_t inbytesleft = bufcount;
		char *outbuf = out;
		size_t outbytesleft = sizeof (out);
		size_t result = iconv (cd,
				       (ICONV_CONST char **)&inbuf, &inbytesleft,
				       &outbuf, &outbytesleft);
		if (result != (size_t)(-1))
		  result = iconv (cd, NULL, NULL, &outbuf, &outbytesleft);
		/* Ignore conversions with errors or transliteration.  */
		if (result == 0 && inbytesleft == 0)
		  {
		    size_t outbytes = sizeof (out) - outbytesleft;
		    /* Ignore conversions that produced more than one byte.  */
		    if (outbytes == 1)
		      value = (unsigned char) out[0];
		  }
	      }

	      gl_lock_unlock (facet->iconv_from_lock);
	    }
	}
      else
	{
	  /* Assume facet->encoding is equivalent to ASCII.  */
	  if (c < 0x80)
	    value = c;
	}

      return value;
    }
}

#if GLOCALE_ENABLE_WIDE_CHAR

int
gl_wctob (wint_t c, gl_locale_t locale)
{
  if (c != WEOF)
    return _gl_wctob (c, locale);
  else
    return EOF;
}

#endif
