/* Convert a Unicode character to an UTF-8 encoded multi-byte character.
   Copyright (C) 1999-2002, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

size_t
_gl_uctomb_utf8 (char *ss, unsigned int uc, size_t n, iconv_t cd)
{
  unsigned char *s = (unsigned char *)ss;

  if (uc < 0x80)
    {
      s[0] = uc;
      return 1;
    }
  else
    {
      size_t count;

      if (uc < 0x800)
	count = 2;
      else if (uc < 0x10000)
	count = 3;
      else if (uc < 0x110000)
	count = 4;
      else
	return (size_t)(-1);

      if (n < count)
	return (size_t)(-2);

      switch (count) /* note: code falls through cases! */
	{
	case 4: s[3] = 0x80 | (uc & 0x3f); uc = uc >> 6; uc |= 0x10000;
	case 3: s[2] = 0x80 | (uc & 0x3f); uc = uc >> 6; uc |= 0x800;
	case 2: s[1] = 0x80 | (uc & 0x3f); uc = uc >> 6; uc |= 0xc0;
	case 1: s[0] = uc;
	}
      return count;
    }
}
