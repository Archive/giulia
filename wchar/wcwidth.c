/* Determination of the screen width of a wide character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>
#include "glocale-private.h"

#include "wchar-lookup.h"

#if GLOCALE_ENABLE_WIDE_CHAR
int
gl_wcwidth (wchar_t wc, gl_locale_t locale)
#else
int
_gl_ucwidth (unsigned int wc, gl_locale_t locale)
#endif
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  wcwidth_table table = facet->table_width;

  if (table == NULL)
    table = _gl_init_width_table (facet, locale);

  return wcwidth_table_lookup (table, wc);
}
