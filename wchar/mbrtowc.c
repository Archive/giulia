/* Convert a multi-byte character to a wide character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"
#include "small.h"

size_t
gl_mbrtowc (wchar_t *restrict pwc, const char *restrict s, size_t n,
	    mbstate_t *ps, gl_locale_t locale)
{
  gl_mbstate_t *pstate =
    (ps != NULL
     ? (gl_mbstate_t *) ps
     : &_gl_thread_specific (locale)->state_for_mbrtowc);
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  iconv_t cd = facet->cd_to_utf8;
  size_t result;

  if (s != NULL)
    {
      if (n > 0)
	{
	  if (!facet->is_utf8 && cd != (iconv_t)(-1))
	    {
	      gl_lock_lock (facet->iconv_to_lock);
	      iconv (cd, NULL, NULL, NULL, NULL);
	    }

	  {
	    char buf[2*GL_MB_LEN_MAX];
	    size_t incoming = (n < GL_MB_LEN_MAX ? n : GL_MB_LEN_MAX);
	    size_t old = pstate->mbcount;
	    unsigned int uc;
	    memcpy_small (buf, pstate->mbuf, old);
	    memcpy_small (buf + old, s, incoming);
	    result = facet->mbtouc (&uc, buf, old + incoming, old + 1, cd);
	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      errno = EILSEQ;
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence.  */
	      {
		/* We must have old + incoming < GL_MB_LEN_MAX
		   and therefore incoming = n.  */
		if (old + n >= GL_MB_LEN_MAX)
		  abort ();
		memcpy_small (pstate->mbuf + old, s, n);
		pstate->mbcount = old + n;
	      }
	    else
	      {
		/* Successful conversion to UCS-4.  */
		if (pwc != NULL)
		  *pwc = uc;
		pstate->mbcount = 0;
		result = (uc == 0 ? 0 : result - old);
	      }
	  }

	  if (!facet->is_utf8 && cd != (iconv_t)(-1))
	    gl_lock_unlock (facet->iconv_to_lock);
	}
      else
	/* n = 0.  */
	result = (size_t)(-2);
    }
  else
    {
      /* Do as if pwc = NULL, s = "", n = 1.  */
      if (facet->is_utf8)
	{
	  /* Shortcut for UTF-8.  */
	  if (pstate->mbcount > 0)
	    {
	      errno = EILSEQ;
	      result = (size_t)(-1);
	    }
	  else
	    {
	      pstate->mbcount = 0;
	      result = 0;
	    }
	}
      else
	{
	  if (cd != (iconv_t)(-1))
	    {
	      gl_lock_lock (facet->iconv_to_lock);
	      iconv (cd, NULL, NULL, NULL, NULL);
	    }

	  {
	    size_t old = pstate->mbcount;
	    unsigned int uc;
	    pstate->mbuf[old] = '\0';
	    result = facet->mbtouc (&uc, pstate->mbuf, old + 1, old + 1, cd);

	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      errno = EILSEQ;
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence - shouldn't happen.  */
	      abort ();
	    else
	      {
		/* Successful conversion to UCS-4.  */
		pstate->mbcount = 0;
		result = (uc == 0 ? 0 : 1);
	      }
	  }

	  if (cd != (iconv_t)(-1))
	    gl_lock_unlock (facet->iconv_to_lock);
	}
    }
  return result;
}

#endif
