/* Convert an ASCII encoded multi-byte character to a Unicode character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

size_t
_gl_mbtouc_ascii (unsigned int *puc, const char *ss, size_t n, size_t nmin, iconv_t cd)
{
  const unsigned char *s = (const unsigned char *) ss;
  unsigned char c = *s;

  if (c < 0x80)
    {
      *puc = c;
      return 1;
    }
  else
    {
      /* invalid multibyte character */
      return (size_t)(-1);
    }
}
