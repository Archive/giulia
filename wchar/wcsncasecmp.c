/* Case-insensitive comparison of wide character strings.
   Copyright (C) 1998-1999, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <glocale/wctype.h>

int
gl_wcsncasecmp (const wchar_t *s1, const wchar_t *s2, size_t n,
		gl_locale_t locale)
{
  wchar_t c1, c2;

  if (s1 == s2 || n == 0)
    return 0;

  do
    {
      c1 = gl_towlower (*s1, locale);
      c2 = gl_towlower (*s2, locale);

      if (--n == 0 || c1 == 0)
	break;

      s1++;
      s2++;
    }
  while (c1 == c2);

  return (c1 > c2 ? 1 : c1 < c2 ? -1 : 0);
}

#endif
