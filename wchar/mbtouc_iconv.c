/* Convert a multi-byte character to a Unicode character using iconv().
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <errno.h>
#include <stdlib.h>
#include "small.h"
#include "utf8-ucs4.h"

size_t
_gl_mbtouc_iconv (unsigned int *puc, const char *s, size_t n, size_t nmin, iconv_t cd)
{
  char out[16];
  size_t incoming = (n < GL_MB_LEN_MAX ? n : GL_MB_LEN_MAX);
  const char *inbuf = s;
  size_t insize;
  char *outbuf = out;
  size_t outbytesleft = sizeof (out);
  size_t result;

  /* We must have 0 < nmin <= n and nmin <= GL_MB_LEN_MAX.  */
  if (nmin == 0 || nmin > incoming)
    abort ();

  for (insize = nmin; ; insize++)
    {
      size_t inbytesleft = insize;
      result = iconv (cd,
		      (ICONV_CONST char **)&inbuf, &inbytesleft,
		      &outbuf, &outbytesleft);
      if (result != (size_t)(-1))
	result = iconv (cd, NULL, NULL, &outbuf, &outbytesleft);
      if (!(result == (size_t)(-1) && errno == EINVAL))
	break;
      /* We expect that no input bytes have been consumed so far.  */
      if (inbuf != s)
	abort ();
      if (outbuf != out)
	abort ();
      if (insize == incoming)
	break;
    }
  if (result == (size_t)(-1))
    {
      if (errno == EILSEQ)
	/* Invalid multibyte sequence.  */
	return (size_t)(-1);
      else if (errno == EINVAL)
	/* Incomplete multibyte sequence.  */
	{
	  /* We must have n < GL_MB_LEN_MAX.  */
	  if (n >= GL_MB_LEN_MAX)
	    abort ();
	  return (size_t)(-2);
	}
      else
	abort ();
    }
  else
    {
      /* Successful conversion.  Don't care whether the
	 conversion was reversible or not.  */
      int outbytes = sizeof (out) - outbytesleft;
      if (outbytes == 0)
	{
	  /* Strange: iconv() consumed some bytes but produced nothing!
	     TODO: loop... */
	  errno = EILSEQ;
	  return (size_t)(-1);
	}
      else
	{
	  unsigned int uc;
	  if (u8_mbtouc (&uc, (unsigned char *) out, outbytes) != outbytes)
	    {
	      /* Strange: iconv() produced multiple Unicode characters!  */
	      errno = EILSEQ;
	      return (size_t)(-1);
	    }
	  else
	    {
	      /* Successful conversion to UCS-4.  */
	      *puc = uc;
	      return insize;
	    }
	}
    }
}
