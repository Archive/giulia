/* Return a locale and thread specific memory area.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <stdlib.h>

#if GLOCALE_ENABLE_WIDE_CHAR

static inline void
_gl_init_thread_specific_locked (gl_locale_t locale)
{
  /* If another thread already initialized it, we're done.  */
  if (locale->thread_specific_initialized)
    return;
  gl_tls_key_init (locale->thread_specific_key, free);
  locale->thread_specific_initialized = 1;
}

struct gl_thread_specific *
_gl_thread_specific (gl_locale_t locale)
{
  struct gl_thread_specific *pointer;

  if (!locale->thread_specific_initialized)
    {
      gl_lock_lock (locale->thread_specific_lock);
      _gl_init_thread_specific_locked (locale);
      gl_lock_unlock (locale->thread_specific_lock);
    }
#if 0 /* Not needed, since gl_tls_key_init cannot return failing.  */
  if (locale->thread_specific_initialized < 0)
    /* Was out of resources earlier.  Use the fallback.  */
    return &locale->thread_specific_fallback;
#endif

  pointer =
    (struct gl_thread_specific *) gl_tls_get (locale->thread_specific_key);
  if (pointer == NULL)
    {
      pointer =
	(struct gl_thread_specific *)
	calloc (1, sizeof (struct gl_thread_specific));
      if (pointer == NULL)
	/* Out of memory.  Use the fallback.  */
	pointer = &locale->thread_specific_fallback;
      gl_tls_set (locale->thread_specific_key, pointer);
    }

  return pointer;
}

#endif
