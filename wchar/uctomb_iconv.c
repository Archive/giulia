/* Convert a Unicode character to a multi-byte character using iconv().
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <errno.h>
#include <stdlib.h>
#include "ucs4-utf8.h"
#include "small.h"

size_t
_gl_uctomb_iconv (char *ss, unsigned int uc, size_t n, iconv_t cd)
{
  unsigned char buf[6];
  int bufcount;

  /* Convert uc to UTF-8.  */
  bufcount = u8_uctomb (buf, uc, sizeof (buf));
  if (bufcount >= 0)
    {
      /* Convert it to the desired encoding.  */
      char out[64];
      const char *inbuf = (const char *) buf;
      size_t inbytesleft = bufcount;
      char *outbuf = out;
      size_t outbytesleft = sizeof (out);
      size_t result = iconv (cd,
			     (ICONV_CONST char **)&inbuf, &inbytesleft,
			     &outbuf, &outbytesleft);
      if (result != (size_t)(-1))
	result = iconv (cd, NULL, NULL, &outbuf, &outbytesleft);
      if (result == (size_t)(-1))
	{
	  if (errno == EILSEQ)
	    return (size_t)(-1);
	  else if (errno == E2BIG)
	    return (size_t)(-2);
	  else
	    abort ();
	}
      else
	{
	  /* Successful conversion.  Don't care whether the conversion was
	     reversible or not.  */
	  size_t outbytes = sizeof (out) - outbytesleft;
	  if (outbytes == 0)
	    {
	      /* Strange: iconv() consumed some bytes but produced nothing!
		 TODO: loop... */
	      return (size_t)(-1);
	    }
	  else if (n < outbytes)
	    return (size_t)(-2);
	  else
	    {
	      memcpy_small (ss, out, outbytes);
	      return outbytes;
	    }
	}
    }
  else
    return (size_t)(-1);
}
