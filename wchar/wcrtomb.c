/* Convert a wide character to a multi-byte character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"
#include "small.h"

size_t
gl_wcrtomb (char *restrict s, wchar_t wc,
	    mbstate_t *restrict ps, gl_locale_t locale)
{
  gl_mbstate_t *pstate =
    (ps != NULL
     ? (gl_mbstate_t *) ps
     : &_gl_thread_specific (locale)->state_for_wcrtomb);
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  size_t prefix_count;
  size_t result;

  if (s != NULL)
    {
      iconv_t cd = facet->cd_from_utf8;

      if (!facet->is_utf8 && cd != (iconv_t)(-1))
	{
	  gl_lock_lock (facet->iconv_from_lock);
	  iconv (cd, NULL, NULL, NULL, NULL);
	}

      prefix_count = pstate->mbcount;
      if (prefix_count > 0)
	{
	  memcpy_small (s, pstate->mbuf, prefix_count);
	  s += prefix_count;
	  pstate->mbcount = 0;
	}

      result = facet->uctomb (s, wc, GL_MB_LEN_MAX, cd);
      if (result == (size_t)(-1))
	/* Invalid wide character or conversion failure.  */
	errno = EILSEQ;
      else if (result == (size_t)(-2))
	/* Output buffer too small.  */
	abort ();
      else
	result += prefix_count;

      if (!facet->is_utf8 && cd != (iconv_t)(-1))
	gl_lock_unlock (facet->iconv_from_lock);
    }
  else
    {
      /* Do as if wc = 0.  This corresponds to the single byte '\0',
	 since all locale encodings are stateless.  */
      prefix_count = pstate->mbcount;
      pstate->mbcount = 0;
      result = prefix_count + 1;
    }

  return result;
}

#endif
