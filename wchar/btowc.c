/* Conversion from a single 'char' to Unicode.
   Copyright (C) 1999, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wchar.h>
#include "glocale-private.h"

#include <stdlib.h>
#include "utf8-ucs4.h"

unsigned int
_gl_btowc (unsigned char c, gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;

  if (facet->is_utf8)
    {
      /* Shortcut for UTF-8.  */
      if (c < 0x80)
	return c;
      else
	return (unsigned int) -1;
    }
  else
    {
      unsigned int value = (unsigned int) -1;
      iconv_t cd = facet->cd_to_utf8;

      if (cd != (iconv_t)(-1))
	{
	  char buf[1];
	  unsigned char out[64];
	  buf[0] = c;

	  gl_lock_lock (facet->iconv_to_lock);

	  iconv (cd, NULL, NULL, NULL, NULL);
	  {
	    const char *inbuf = buf;
	    size_t inbytesleft = 1;
	    char *outbuf = (char *) out;
	    size_t outbytesleft = sizeof (out);
	    size_t result = iconv (cd,
				   (ICONV_CONST char **)&inbuf, &inbytesleft,
				   &outbuf, &outbytesleft);
	    if (result != (size_t)(-1))
	      result = iconv (cd, NULL, NULL, &outbuf, &outbytesleft);
	    /* Ignore conversions with errors or transliteration.  */
	    if (result == 0 && inbytesleft == 0)
	      {
		size_t outbytes = sizeof (out) - outbytesleft;
		unsigned int uc;
		/* Ignore conversions that produced multiple Unicode
		   characters.  */
		if (u8_mbtouc (&uc, out, outbytes) == outbytes)
		  value = uc;
	      }
	  }

	  gl_lock_unlock (facet->iconv_to_lock);
	}
      else
	{
	  /* Assume facet->encoding is equivalent to ASCII.  */
	  if (c < 0x80)
	    value = c;
	}

      return value;
    }
}

#if GLOCALE_ENABLE_WIDE_CHAR

wint_t
gl_btowc (int c, gl_locale_t locale)
{
  if (c != EOF)
    {
      unsigned int value = _gl_btowc ((unsigned char) c, locale);
      if (value != (unsigned int) -1)
	return (wint_t) value;
    }
  return WEOF;
}

#endif
