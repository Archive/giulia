/* Convert an UTF-8 encoded multi-byte character to a Unicode character.
   Copyright (C) 1999-2002, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

size_t
_gl_mbtouc_utf8 (unsigned int *puc, const char *ss, size_t n, size_t nmin, iconv_t cd)
{
  const unsigned char *s = (const unsigned char *) ss;
  unsigned char c = *s;

  if (c < 0x80)
    {
      *puc = c;
      return 1;
    }
  else
    {
      if (c >= 0xc2)
	{
	  if (c < 0xe0)
	    {
	      if (n >= 2)
		{
		  if ((s[1] ^ 0x80) < 0x40)
		    {
		      *puc = ((unsigned int) (c & 0x1f) << 6)
			     | (unsigned int) (s[1] ^ 0x80);
		      return 2;
		    }
		  /* invalid multibyte character */
		}
	      else
		{
		  /* incomplete multibyte character */
		  return (size_t)(-2);
		}
	    }
	  else if (c < 0xf0)
	    {
	      if (n >= 3)
		{
		  if ((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
		      && (c >= 0xe1 || s[1] >= 0xa0))
		    {
		      *puc = ((unsigned int) (c & 0x0f) << 12)
			     | ((unsigned int) (s[1] ^ 0x80) << 6)
			     | (unsigned int) (s[2] ^ 0x80);
		      return 3;
		    }
		  /* invalid multibyte character */
		}
	      else
		{
		  /* incomplete multibyte character */
		  return (size_t)(-2);
		}
	    }
	  else if (c < 0xf8)
	    {
	      if (n >= 4)
		{
		  if ((s[1] ^ 0x80) < 0x40 && (s[2] ^ 0x80) < 0x40
		      && (s[3] ^ 0x80) < 0x40
		      && (c >= 0xf1 || s[1] >= 0x90)
		      && (c < 0xf4 || (c == 0xf4 && s[1] < 0x90)))
		    {
		      *puc = ((unsigned int) (c & 0x07) << 18)
			     | ((unsigned int) (s[1] ^ 0x80) << 12)
			     | ((unsigned int) (s[2] ^ 0x80) << 6)
			     | (unsigned int) (s[3] ^ 0x80);
		      return 4;
		    }
		  /* invalid multibyte character */
		}
	      else
		{
		  /* incomplete multibyte character */
		  return (size_t)(-2);
		}
	    }
	}
      /* invalid multibyte character */
      return (size_t)(-1);
    }
}
