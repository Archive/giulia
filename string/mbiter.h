/* Iterating through multibyte strings: macros for multi-byte encodings.
   Copyright (C) 2001, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>.  */

/* The macros in this file implement forward iteration through a
   multi-byte string.

   With these macros, an iteration loop that looks like

      char *iter;
      for (iter = buf; iter < buf + buflen; iter++)
        {
          do_something (*iter);
        }

   becomes

      mbi_iterator_t iter;
      for (mbi_init (iter, buf, buflen); mbi_avail (iter, locale); mbi_advance (iter))
        {
          do_something (mbi_cur_ptr (iter), mb_len (mbi_cur (iter)));
        }

   The benefit of these macros over plain use of gl_mbrtowc is:
   - Handling of invalid multibyte sequences is possible without
     making the code more complicated, while still preserving the
     invalid multibyte sequences.

   mbi_iterator_t
     is a type usable for variable declarations.

   mbi_init (iter, startptr, length)
     initializes the iterator, starting at startptr and crossing length bytes.

   mbi_avail (iter, locale)
     returns true if there are more multibyte chracters available before
     the end of string is reached. In this case, mbi_cur (iter) is
     initialized to the next multibyte chracter.

   mbi_advance (iter)
     advances the iterator by one multibyte character.

   mbi_cur (iter)
     returns the current multibyte character, of type mbchar_t.  All the
     macros defined in mbchar.h can be used on it.

   mbi_cur_ptr (iter)
     return a pointer to the beginning of the current multibyte character.

   mbi_reloc (iter, ptrdiff)
     relocates iterator when the string is moved by ptrdiff bytes.

   Here are the function prototypes of the macros.

   extern void		mbi_init (mbi_iterator_t iter,
				  const char *startptr, size_t length);
   extern bool		mbi_avail (mbi_iterator_t iter, gl_locale_t locale);
   extern void		mbi_advance (mbi_iterator_t iter);
   extern mbchar_t	mbi_cur (mbi_iterator_t iter);
   extern const char *	mbi_cur_ptr (mbi_iterator_t iter);
   extern void		mbi_reloc (mbi_iterator_t iter, ptrdiff_t ptrdiff);
 */

#ifndef _MBITER_H
#define _MBITER_H 1

#include <assert.h>
#include <stdbool.h>

#include "mbchar.h"
#include "glocale-private.h"

struct mbiter_multi
{
  const char *limit;	/* pointer to end of string */
  bool next_done;	/* true if mbi_avail has already filled the following */
  struct mbchar cur;	/* the current character:
	const char *cur.ptr		pointer to current character
	The following are only valid after mbi_avail.
	size_t cur.bytes		number of bytes of current character
	bool cur.uc_valid		true if uc is a valid Unicode character
	unsigned int cur.uc		if uc_valid: the current character
	*/
};

static inline void
mbiter_multi_next (struct mbiter_multi *iter, gl_locale_t locale)
{
  if (iter->next_done)
    return;
  /* Handle most ASCII characters quickly, without calling facet->mbtouc().  */
  if (is_basic (*iter->cur.ptr))
    {
      /* These characters are part of the basic character set, hence ASCII.  */
      iter->cur.bytes = 1;
      iter->cur.uc = *iter->cur.ptr;
      iter->cur.uc_valid = true;
    }
  else
    {
      struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
      iconv_t cd = facet->cd_to_utf8;

      if (!facet->is_utf8 && cd != (iconv_t)(-1))
	{
	  gl_lock_lock (facet->iconv_to_lock);
	  iconv (cd, NULL, NULL, NULL, NULL);
	}

      iter->cur.bytes =
	facet->mbtouc (&iter->cur.uc, iter->cur.ptr,
		       iter->limit - iter->cur.ptr, 1,
		       cd);
      if (iter->cur.bytes == (size_t) -1)
	{
	  /* An invalid multibyte sequence was encountered.  */
	  iter->cur.bytes = 1;
	  iter->cur.uc_valid = false;
	}
      else if (iter->cur.bytes == (size_t) -2)
	{
	  /* An incomplete multibyte character at the end.  */
	  iter->cur.bytes = iter->limit - iter->cur.ptr;
	  iter->cur.uc_valid = false;
	}
      else
	{
	  if (iter->cur.bytes == 0)
	    abort ();
	  iter->cur.uc_valid = true;
	}

      if (!facet->is_utf8 && cd != (iconv_t)(-1))
	gl_lock_unlock (facet->iconv_to_lock);
    }
  iter->next_done = true;
}

static inline void
mbiter_multi_reloc (struct mbiter_multi *iter, ptrdiff_t ptrdiff)
{
  iter->cur.ptr += ptrdiff;
  iter->limit += ptrdiff;
}

/* Iteration macros.  */
typedef struct mbiter_multi mbi_iterator_t;
#define mbi_init(iter, startptr, length) \
  ((iter).cur.ptr = (startptr), (iter).limit = (iter).cur.ptr + (length), \
   (iter).next_done = false)
#define mbi_avail(iter, locale) \
  ((iter).cur.ptr < (iter).limit && (mbiter_multi_next (&(iter), locale), true))
#define mbi_advance(iter) \
  ((iter).cur.ptr += (iter).cur.bytes, (iter).next_done = false)

/* Access to the current character.  */
#define mbi_cur(iter) (iter).cur
#define mbi_cur_ptr(iter) (iter).cur.ptr

/* Relocation.  */
#define mbi_reloc(iter, ptrdiff) mbiter_multi_reloc (&iter, ptrdiff)

#endif /* _MBITER_H */
