/* Multibyte character I/O: macros for multi-byte encodings.
   Copyright (C) 2001, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Mitsuru Chinen <mchinen@yamato.ibm.com>
   and Bruno Haible <bruno@clisp.org>.  */

/* The macros in this file implement multi-byte character input from a
   stream.

   mb_file_t
     is the type for multibyte character input stream, usable for variable
     declarations.

   mbf_char_t
     is the type for multibyte character or EOF, usable for variable
     declarations.

   mbf_init (mbf, stream)
     initializes the MB_FILE for reading from stream.

   mbf_getc (mbc, mbf, locale)
     reads the next multibyte character from mbf and stores it in mbc.

   mb_iseof (mbc)
     returns true if mbc represents the EOF value.

   Here are the function prototypes of the macros.

   extern void		mbf_init (mb_file_t mbf, FILE *stream);
   extern void		mbf_getc (mbf_char_t mbc, mb_file_t mbf, gl_locale_t locale);
   extern bool		mb_iseof (const mbf_char_t mbc);
 */

#ifndef _MBFILE_H
#define _MBFILE_H 1

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "mbchar.h"

struct mbfile_multi {
  FILE *fp;
  bool eof_seen;
  bool have_pushback;
  unsigned int bufcount;
  char buf[MBCHAR_BUF_SIZE];
  struct mbchar pushback;
};

static inline void
mbfile_multi_getc (struct mbchar *mbc, struct mbfile_multi *mbf,
		   gl_locale_t locale)
{
  struct gl_locale_ctype *facet;
  iconv_t cd;
  size_t minbufcount;
  size_t bytes;

  /* If EOF has already been seen, don't use getc.  This matters if
     mbf->fp is connected to an interactive tty.  */
  if (mbf->eof_seen)
    goto eof;

  /* Return character pushed back, if there is one.  */
  if (mbf->have_pushback)
    {
      mb_copy (mbc, &mbf->pushback);
      mbf->have_pushback = false;
      return;
    }

  /* Before using facet->mbtouc, we need at least one byte.  */
  if (mbf->bufcount == 0)
    {
      int c = getc (mbf->fp);
      if (c == EOF)
	{
	  mbf->eof_seen = true;
	  goto eof;
	}
      mbf->buf[0] = (unsigned char) c;
      mbf->bufcount++;
    }

  /* Handle most ASCII characters quickly, without calling facet->mbtouc().  */
  if (mbf->bufcount == 1 && is_basic (mbf->buf[0]))
    {
      /* These characters are part of the basic character set, hence ASCII.  */
      mbc->uc = mbc->buf[0] = mbf->buf[0];
      mbc->uc_valid = true;
      mbc->ptr = &mbc->buf[0];
      mbc->bytes = 1;
      mbf->bufcount = 0;
      return;
    }

  facet = locale->facet_LC_CTYPE;
  cd = facet->cd_to_utf8;

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    {
      gl_lock_lock (facet->iconv_to_lock);
      iconv (cd, NULL, NULL, NULL, NULL);
    }

  /* Use facet->mbtouc on an increasing number of bytes.  Read only as many
     bytes from mbf->fp as needed.  This is needed to give reasonable
     interactive behaviour when mbf->fp is connected to an interactive tty.  */
  minbufcount = 1;
  for (;;)
    {
      bytes = facet->mbtouc (&mbc->uc, &mbf->buf[0], mbf->bufcount,
			     minbufcount, cd, locale);

      if (bytes == (size_t) -1)
	{
	  /* An invalid multibyte sequence was encountered.  */
	  /* Return a single byte.  */
	  bytes = 1;
	  mbc->uc_valid = false;
	  break;
	}
      else if (bytes == (size_t) -2)
	{
	  /* An incomplete multibyte character.  */
	  if (mbf->bufcount == MBCHAR_BUF_SIZE)
	    {
	      /* An overlong incomplete multibyte sequence was encountered.  */
	      /* Return a single byte.  */
	      bytes = 1;
	      mbc->uc_valid = false;
	      break;
	    }
	  else
	    {
	      /* Read one more byte and retry facet->mbtouc.  */
	      int c = getc (mbf->fp);
	      if (c == EOF)
		{
		  /* An incomplete multibyte character at the end.  */
		  mbf->eof_seen = true;
		  bytes = mbf->bufcount;
		  mbc->uc_valid = false;
		  break;
		}
	      mbf->buf[mbf->bufcount] = (unsigned char) c;
	      mbf->bufcount++;
	      minbufcount = mbf->bufcount;
	    }
        }
      else
	{
	  if (bytes == 0)
	    abort ();
	  mbc->uc_valid = true;
	  break;
	}
    }

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    gl_lock_unlock (facet->iconv_to_lock);

  /* Return the multibyte sequence mbf->buf[0..bytes-1].  */
  mbc->ptr = &mbc->buf[0];
  memcpy (&mbc->buf[0], &mbf->buf[0], bytes);
  mbc->bytes = bytes;

  mbf->bufcount -= bytes;
  if (mbf->bufcount > 0)
    {
      /* It's not worth calling memmove() for so few bytes.  */
      unsigned int count = mbf->bufcount;
      char *p = &mbf->buf[0];

      do
	{
	  *p = *(p + bytes);
	  p++;
	}
      while (--count > 0);
    }
  return;

eof:
  /* An mbchar_t with bytes == 0 is used to indicate EOF.  */
  mbc->ptr = NULL;
  mbc->bytes = 0;
  mbc-> = false;
  return;
}

static inline void
mbfile_multi_ungetc (const struct mbchar *mbc, struct mbfile_multi *mbf)
{
  mb_copy (&mbf->pushback, mbc);
  mbf->have_pushback = true;
}

typedef struct mbfile_multi mb_file_t;

typedef mbchar_t mbf_char_t;

#define mbf_init(mbf, stream)						\
  ((mbf).fp = (stream),							\
   (mbf).eof_seen = false,						\
   (mbf).have_pushback = false,						\
   (mbf).bufcount = 0)

#define mbf_getc(mbc, mbf, locale) mbfile_multi_getc (&(mbc), &(mbf), locale)

#define mbf_ungetc(mbc, mbf) mbfile_multi_ungetc (&(mbc), &(mbf))

#define mb_iseof(mbc) ((mbc).bytes == 0)

#endif /* _MBFILE_H */
