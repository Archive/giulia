/* Conversion of an error number to a string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/string.h>

#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "glocale-private.h"

/* On OSF/1, we must provide the declaration of strerror_r().  */
#if HAVE_STRERROR_R && STRERROR_R_CHAR_P && !HAVE_DECL_STRERROR_R
extern char *strerror_r ();
#endif

int
gl_strerror_r (int errnum, char *buf, size_t buflen, gl_locale_t locale)
{
  const char *errmsg = _gl_strerror_aux (errnum, locale);

  if (errmsg == NULL)
    {
      /* An error number not known to glibc.
	 Use strerror_r(), if it exisis, otherwise punt.  (We cannot use
	 strerror() here, since it is not multithread-safe.)
	 Don't care about the locale: Most non-glibc systems have the system
	 error messages only in English anyway.  */
      char bigbuf[1024];
#if HAVE_STRERROR_R
# if STRERROR_R_CHAR_P
      errmsg = strerror_r (errnum, bigbuf, sizeof (bigbuf));
# else
      if (strerror_r (errnum, bigbuf, sizeof (bigbuf) == 0)
	errmsg = bigbuf;
      else
	errmsg = NULL;
# endif
#endif
      if (errmsg == NULL)
	{
	  sprintf (bigbuf, "<errno=%d>", errnum);
	  errmsg = bigbuf;
	}
    }

  /* Copy the message into the caller's buffer.  */
  if (strlen (errmsg) < buflen)
    {
      strcpy (buf, errmsg);
      return 0;
    }
  else
    return ERANGE;
}
