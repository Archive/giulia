/* Determination of the screen width of a string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/string.h>

#include "mbiter.h"

int
gl_strnwidth (const char *buf, size_t nbytes, gl_locale_t locale)
{
  mbi_iterator_t iter;
  int count = 0;

  mbi_init (iter, buf, nbytes);

  while (mbi_avail (iter, locale))
    {
      if (mb_isnul (mbi_cur (iter)))
	break;

      {
	int width = mb_width (mbi_cur (iter), locale);
#if 0 /* mb_width never return a negative value, unlike wcwidth.  */
	if (width < 0)
	  return -1;
#endif
        count += width;
      }

      mbi_advance (iter);
    }

  return count;
}
