/* Conversion of an error number to a string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <errno.h>
#include <locale.h>
#include <stdlib.h>

const char *
_gl_strerror_aux (int errnum, gl_locale_t locale)
{
  const char *errname;
  const char *errmsg;

  switch (errnum)
    {
    /* Errno values in the same order as in cook/read-glibc.c.  */
#ifdef EPERM
    case EPERM:
      errname = "EPERM";
      break;
#endif
#ifdef ENOENT
    case ENOENT:
      errname = "ENOENT";
      break;
#endif
#ifdef ESRCH
    case ESRCH:
      errname = "ESRCH";
      break;
#endif
#ifdef EINTR
    case EINTR:
      errname = "EINTR";
      break;
#endif
#ifdef EIO
    case EIO:
      errname = "EIO";
      break;
#endif
#ifdef ENXIO
    case ENXIO:
      errname = "ENXIO";
      break;
#endif
#ifdef E2BIG
    case E2BIG:
      errname = "E2BIG";
      break;
#endif
#ifdef ENOEXEC
    case ENOEXEC:
      errname = "ENOEXEC";
      break;
#endif
#ifdef EBADF
    case EBADF:
      errname = "EBADF";
      break;
#endif
#ifdef ECHILD
    case ECHILD:
      errname = "ECHILD";
      break;
#endif
#ifdef EDEADLK
    case EDEADLK:
      errname = "EDEADLK";
      break;
#endif
#ifdef ENOMEM
    case ENOMEM:
      errname = "ENOMEM";
      break;
#endif
#ifdef EACCES
    case EACCES:
      errname = "EACCES";
      break;
#endif
#ifdef EFAULT
    case EFAULT:
      errname = "EFAULT";
      break;
#endif
#ifdef ENOTBLK
    case ENOTBLK:
      errname = "ENOTBLK";
      break;
#endif
#ifdef EBUSY
    case EBUSY:
      errname = "EBUSY";
      break;
#endif
#ifdef EEXIST
    case EEXIST:
      errname = "EEXIST";
      break;
#endif
#ifdef EXDEV
    case EXDEV:
      errname = "EXDEV";
      break;
#endif
#ifdef ENODEV
    case ENODEV:
      errname = "ENODEV";
      break;
#endif
#ifdef ENOTDIR
    case ENOTDIR:
      errname = "ENOTDIR";
      break;
#endif
#ifdef EISDIR
    case EISDIR:
      errname = "EISDIR";
      break;
#endif
#ifdef EINVAL
    case EINVAL:
      errname = "EINVAL";
      break;
#endif
#ifdef EMFILE
    case EMFILE:
      errname = "EMFILE";
      break;
#endif
#ifdef ENFILE
    case ENFILE:
      errname = "ENFILE";
      break;
#endif
#ifdef ENOTTY
    case ENOTTY:
      errname = "ENOTTY";
      break;
#endif
#ifdef ETXTBSY
    case ETXTBSY:
      errname = "ETXTBSY";
      break;
#endif
#ifdef EFBIG
    case EFBIG:
      errname = "EFBIG";
      break;
#endif
#ifdef ENOSPC
    case ENOSPC:
      errname = "ENOSPC";
      break;
#endif
#ifdef ESPIPE
    case ESPIPE:
      errname = "ESPIPE";
      break;
#endif
#ifdef EROFS
    case EROFS:
      errname = "EROFS";
      break;
#endif
#ifdef EMLINK
    case EMLINK:
      errname = "EMLINK";
      break;
#endif
#ifdef EPIPE
    case EPIPE:
      errname = "EPIPE";
      break;
#endif
#ifdef EDOM
    case EDOM:
      errname = "EDOM";
      break;
#endif
#ifdef ERANGE
    case ERANGE:
      errname = "ERANGE";
      break;
#endif
#ifdef EAGAIN
    case EAGAIN:
      errname = "EAGAIN";
      break;
#endif
#ifdef EINPROGRESS
    case EINPROGRESS:
      errname = "EINPROGRESS";
      break;
#endif
#ifdef EALREADY
    case EALREADY:
      errname = "EALREADY";
      break;
#endif
#ifdef ENOTSOCK
    case ENOTSOCK:
      errname = "ENOTSOCK";
      break;
#endif
#ifdef EMSGSIZE
    case EMSGSIZE:
      errname = "EMSGSIZE";
      break;
#endif
#ifdef EPROTOTYPE
    case EPROTOTYPE:
      errname = "EPROTOTYPE";
      break;
#endif
#ifdef ENOPROTOOPT
    case ENOPROTOOPT:
      errname = "ENOPROTOOPT";
      break;
#endif
#ifdef EPROTONOSUPPORT
    case EPROTONOSUPPORT:
      errname = "EPROTONOSUPPORT";
      break;
#endif
#ifdef ESOCKTNOSUPPORT
    case ESOCKTNOSUPPORT:
      errname = "ESOCKTNOSUPPORT";
      break;
#endif
#ifdef EOPNOTSUPP
    case EOPNOTSUPP:
      errname = "EOPNOTSUPP";
      break;
#endif
#ifdef EPFNOSUPPORT
    case EPFNOSUPPORT:
      errname = "EPFNOSUPPORT";
      break;
#endif
#ifdef EAFNOSUPPORT
    case EAFNOSUPPORT:
      errname = "EAFNOSUPPORT";
      break;
#endif
#ifdef EADDRINUSE
    case EADDRINUSE:
      errname = "EADDRINUSE";
      break;
#endif
#ifdef EADDRNOTAVAIL
    case EADDRNOTAVAIL:
      errname = "EADDRNOTAVAIL";
      break;
#endif
#ifdef ENETDOWN
    case ENETDOWN:
      errname = "ENETDOWN";
      break;
#endif
#ifdef ENETUNREACH
    case ENETUNREACH:
      errname = "ENETUNREACH";
      break;
#endif
#ifdef ENETRESET
    case ENETRESET:
      errname = "ENETRESET";
      break;
#endif
#ifdef ECONNABORTED
    case ECONNABORTED:
      errname = "ECONNABORTED";
      break;
#endif
#ifdef ECONNRESET
    case ECONNRESET:
      errname = "ECONNRESET";
      break;
#endif
#ifdef ENOBUFS
    case ENOBUFS:
      errname = "ENOBUFS";
      break;
#endif
#ifdef EISCONN
    case EISCONN:
      errname = "EISCONN";
      break;
#endif
#ifdef ENOTCONN
    case ENOTCONN:
      errname = "ENOTCONN";
      break;
#endif
#ifdef EDESTADDRREQ
    case EDESTADDRREQ:
      errname = "EDESTADDRREQ";
      break;
#endif
#ifdef ESHUTDOWN
    case ESHUTDOWN:
      errname = "ESHUTDOWN";
      break;
#endif
#ifdef ETOOMANYREFS
    case ETOOMANYREFS:
      errname = "ETOOMANYREFS";
      break;
#endif
#ifdef ETIMEDOUT
    case ETIMEDOUT:
      errname = "ETIMEDOUT";
      break;
#endif
#ifdef ECONNREFUSED
    case ECONNREFUSED:
      errname = "ECONNREFUSED";
      break;
#endif
#ifdef ELOOP
    case ELOOP:
      errname = "ELOOP";
      break;
#endif
#ifdef ENAMETOOLONG
    case ENAMETOOLONG:
      errname = "ENAMETOOLONG";
      break;
#endif
#ifdef EHOSTDOWN
    case EHOSTDOWN:
      errname = "EHOSTDOWN";
      break;
#endif
#ifdef EHOSTUNREACH
    case EHOSTUNREACH:
      errname = "EHOSTUNREACH";
      break;
#endif
#ifdef ENOTEMPTY
    case ENOTEMPTY:
      errname = "ENOTEMPTY";
      break;
#endif
#ifdef EPROCLIM
    case EPROCLIM:
      errname = "EPROCLIM";
      break;
#endif
#ifdef EUSERS
    case EUSERS:
      errname = "EUSERS";
      break;
#endif
#ifdef EDQUOT
    case EDQUOT:
      errname = "EDQUOT";
      break;
#endif
#ifdef ESTALE
    case ESTALE:
      errname = "ESTALE";
      break;
#endif
#ifdef EREMOTE
    case EREMOTE:
      errname = "EREMOTE";
      break;
#endif
#ifdef EBADRPC
    case EBADRPC:
      errname = "EBADRPC";
      break;
#endif
#ifdef ERPCMISMATCH
    case ERPCMISMATCH:
      errname = "ERPCMISMATCH";
      break;
#endif
#ifdef EPROGUNAVAIL
    case EPROGUNAVAIL:
      errname = "EPROGUNAVAIL";
      break;
#endif
#ifdef EPROGMISMATCH
    case EPROGMISMATCH:
      errname = "EPROGMISMATCH";
      break;
#endif
#ifdef EPROCUNAVAIL
    case EPROCUNAVAIL:
      errname = "EPROCUNAVAIL";
      break;
#endif
#ifdef ENOLCK
    case ENOLCK:
      errname = "ENOLCK";
      break;
#endif
#ifdef EFTYPE
    case EFTYPE:
      errname = "EFTYPE";
      break;
#endif
#ifdef EAUTH
    case EAUTH:
      errname = "EAUTH";
      break;
#endif
#ifdef ENEEDAUTH
    case ENEEDAUTH:
      errname = "ENEEDAUTH";
      break;
#endif
#ifdef ENOSYS
    case ENOSYS:
      errname = "ENOSYS";
      break;
#endif
#ifdef EILSEQ
    case EILSEQ:
      errname = "EILSEQ";
      break;
#endif
#ifdef EBACKGROUND
    case EBACKGROUND:
      errname = "EBACKGROUND";
      break;
#endif
#ifdef EDIED
    case EDIED:
      errname = "EDIED";
      break;
#endif
#if JOKE
#ifdef ED
    case ED:
      errname = "ED";
      break;
#endif
#ifdef EGREGIOUS
    case EGREGIOUS:
      errname = "EGREGIOUS";
      break;
#endif
#ifdef EIEIO
    case EIEIO:
      errname = "EIEIO";
      break;
#endif
#endif /* JOKE */
#ifdef EGRATUITOUS
    case EGRATUITOUS:
      errname = "EGRATUITOUS";
      break;
#endif
#ifdef EBADMSG
    case EBADMSG:
      errname = "EBADMSG";
      break;
#endif
#ifdef EIDRM
    case EIDRM:
      errname = "EIDRM";
      break;
#endif
#ifdef EMULTIHOP
    case EMULTIHOP:
      errname = "EMULTIHOP";
      break;
#endif
#ifdef ENODATA
    case ENODATA:
      errname = "ENODATA";
      break;
#endif
#ifdef ENOLINK
    case ENOLINK:
      errname = "ENOLINK";
      break;
#endif
#ifdef ENOMSG
    case ENOMSG:
      errname = "ENOMSG";
      break;
#endif
#ifdef ENOSR
    case ENOSR:
      errname = "ENOSR";
      break;
#endif
#ifdef ENOSTR
    case ENOSTR:
      errname = "ENOSTR";
      break;
#endif
#ifdef EOVERFLOW
    case EOVERFLOW:
      errname = "EOVERFLOW";
      break;
#endif
#ifdef EPROTO
    case EPROTO:
      errname = "EPROTO";
      break;
#endif
#ifdef ETIME
    case ETIME:
      errname = "ETIME";
      break;
#endif
#ifdef ECANCELED
    case ECANCELED:
      errname = "ECANCELED";
      break;
#endif
#ifdef ERESTART
    case ERESTART:
      errname = "ERESTART";
      break;
#endif
#ifdef ECHRNG
    case ECHRNG:
      errname = "ECHRNG";
      break;
#endif
#ifdef EL2NSYNC
    case EL2NSYNC:
      errname = "EL2NSYNC";
      break;
#endif
#ifdef EL3HLT
    case EL3HLT:
      errname = "EL3HLT";
      break;
#endif
#ifdef EL3RST
    case EL3RST:
      errname = "EL3RST";
      break;
#endif
#ifdef ELNRNG
    case ELNRNG:
      errname = "ELNRNG";
      break;
#endif
#ifdef EUNATCH
    case EUNATCH:
      errname = "EUNATCH";
      break;
#endif
#ifdef ENOCSI
    case ENOCSI:
      errname = "ENOCSI";
      break;
#endif
#ifdef EL2HLT
    case EL2HLT:
      errname = "EL2HLT";
      break;
#endif
#ifdef EBADE
    case EBADE:
      errname = "EBADE";
      break;
#endif
#ifdef EBADR
    case EBADR:
      errname = "EBADR";
      break;
#endif
#ifdef EXFULL
    case EXFULL:
      errname = "EXFULL";
      break;
#endif
#ifdef ENOANO
    case ENOANO:
      errname = "ENOANO";
      break;
#endif
#ifdef EBADRQC
    case EBADRQC:
      errname = "EBADRQC";
      break;
#endif
#ifdef EBADSLT
    case EBADSLT:
      errname = "EBADSLT";
      break;
#endif
#ifdef EBFONT
    case EBFONT:
      errname = "EBFONT";
      break;
#endif
#ifdef ENONET
    case ENONET:
      errname = "ENONET";
      break;
#endif
#ifdef ENOPKG
    case ENOPKG:
      errname = "ENOPKG";
      break;
#endif
#ifdef EADV
    case EADV:
      errname = "EADV";
      break;
#endif
#ifdef ESRMNT
    case ESRMNT:
      errname = "ESRMNT";
      break;
#endif
#ifdef ECOMM
    case ECOMM:
      errname = "ECOMM";
      break;
#endif
#ifdef EDOTDOT
    case EDOTDOT:
      errname = "EDOTDOT";
      break;
#endif
#ifdef ENOTUNIQ
    case ENOTUNIQ:
      errname = "ENOTUNIQ";
      break;
#endif
#ifdef EBADFD
    case EBADFD:
      errname = "EBADFD";
      break;
#endif
#ifdef EREMCHG
    case EREMCHG:
      errname = "EREMCHG";
      break;
#endif
#ifdef ELIBACC
    case ELIBACC:
      errname = "ELIBACC";
      break;
#endif
#ifdef ELIBBAD
    case ELIBBAD:
      errname = "ELIBBAD";
      break;
#endif
#ifdef ELIBSCN
    case ELIBSCN:
      errname = "ELIBSCN";
      break;
#endif
#ifdef ELIBMAX
    case ELIBMAX:
      errname = "ELIBMAX";
      break;
#endif
#ifdef ELIBEXEC
    case ELIBEXEC:
      errname = "ELIBEXEC";
      break;
#endif
#ifdef ESTRPIPE
    case ESTRPIPE:
      errname = "ESTRPIPE";
      break;
#endif
#ifdef EUCLEAN
    case EUCLEAN:
      errname = "EUCLEAN";
      break;
#endif
#ifdef ENOTNAM
    case ENOTNAM:
      errname = "ENOTNAM";
      break;
#endif
#ifdef ENAVAIL
    case ENAVAIL:
      errname = "ENAVAIL";
      break;
#endif
#ifdef EISNAM
    case EISNAM:
      errname = "EISNAM";
      break;
#endif
#ifdef EREMOTEIO
    case EREMOTEIO:
      errname = "EREMOTEIO";
      break;
#endif
#ifdef ENOMEDIUM
    case ENOMEDIUM:
      errname = "ENOMEDIUM";
      break;
#endif
#ifdef EMEDIUMTYPE
    case EMEDIUMTYPE:
      errname = "EMEDIUMTYPE";
      break;
#endif
    /* Common aliases.  */
#ifdef EWOULDBLOCK
# if EWOULDBLOCK != EAGAIN
    case EWOULDBLOCK:
      errname = "EAGAIN";
      break;
# endif
#endif
#ifdef EDEADLOCK
# if EDEADLOCK != EDEADLK
    case EDEADLOCK:
      errname = "EDEADLK";
      break;
# endif
#endif
    default:
      return NULL;
    }

  errmsg = _gl_defaulted_dcgettext (errname, NULL, LC_MESSAGES, locale);

  if (errmsg == NULL)
    {
      /* Not found.  This can happen for unsupported locales.
	 Try an English locale instead.  (Not "C", since dcgettext()
	 in the C locale doesn't look up the answer in a .mo file.)  */
      errmsg = _gl_dcgettext (DOMAIN, errname, LC_MESSAGES,
			      "en_US", locale->facet_LC_CTYPE->encoding);
      if (errmsg == errname)
	/* Not found.  This means the locale data file is invalid.  */
	abort ();
    }

  return errmsg;
}
