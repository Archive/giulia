/* Conversion of an error number to a string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/string.h>

#include <string.h>

#include "glocale-private.h"

char *
gl_strerror (int errnum, gl_locale_t locale)
{
  const char *errmsg = _gl_strerror_aux (errnum, locale);

  if (errmsg != NULL)
    /* The string returned by _gl_strerror_aux has indefinite extent.  */
    return (char *) errmsg;
  else
    /* An error number not known to glibc.
       Use strerror().
       Don't care about the locale: Most non-glibc systems have the system
       error messages only in English anyway.  */
   return strerror (errnum);
}
