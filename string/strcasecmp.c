/* Case-insensitive comparison of strings.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/string.h>

#include <glocale/ctype.h>
#include "mbuiter.h"
#include "glocale-private.h"

int
gl_strcasecmp (const char *s1, const char *s2, gl_locale_t locale)
{
  if (s1 == s2)
    return 0;

  /* Be careful not to look at the entire extent of s1 or s2 until needed.
     This is useful because when two strings differ, the difference is
     most often already in the very few first characters.  */
  if (locale->facet_LC_CTYPE->mb_cur_max > 1)
    {
      mbui_iterator_t iter1;
      mbui_iterator_t iter2;

      mbui_init (iter1, s1);
      mbui_init (iter2, s2);

      while (mbui_avail (iter1, locale) && mbui_avail (iter2, locale))
	{
	  int cmp = mb_casecmp (mbui_cur (iter1), mbui_cur (iter2), locale);

	  if (cmp != 0)
	    return cmp;

	  mbui_advance (iter1);
	  mbui_advance (iter2);
	}
      if (mbui_avail (iter1, locale))
	/* s2 terminated before s1.  */
	return 1;
      if (mbui_avail (iter2, locale))
	/* s1 terminated before s2.  */
	return -1;
      return 0;
    }
  else
    {
      unsigned char c1, c2;

      do
	{
	  c1 = gl_tolower ((unsigned char) *s1, locale);
	  c2 = gl_tolower ((unsigned char) *s2, locale);

	  if (c1 == '\0')
	    break;

	  s1++;
	  s2++;
	}
      while (c1 == c2);

      return c1 - c2;
    }
}
