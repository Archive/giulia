/* Retrieving locale-dependent strings.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/langinfo.h>

#include "glocale-private.h"

const char *
gl_nl_langinfo (nl_item item, gl_locale_t locale)
{
  switch (item)
  {

  /* LC_CTYPE items.  */
  case CODESET:
    return locale->facet_LC_CTYPE->encoding;

  /* LC_MESSAGES items.  */
  case YESEXPR:
    return _gl_defaulted_dcgettext ("YESEXPR", "^[yY]", LC_MESSAGES, locale);
  case NOEXPR:
    return _gl_defaulted_dcgettext ("NOEXPR", "^[nN]", LC_MESSAGES, locale);
#ifdef YESSTR
  case YESSTR:
    return _gl_defaulted_dcgettext ("YESSTR", "", LC_MESSAGES, locale);
#endif
#ifdef NOSTR
  case NOSTR:
    return _gl_defaulted_dcgettext ("NOSTR", "", LC_MESSAGES, locale);
#endif

  default:
    return "";
  }
}
