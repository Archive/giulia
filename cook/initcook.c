/* Creation of the initial locale data files.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <getopt.h>
#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "progname.h"
#include "basename.h"
#include "error.h"
#include "read-glibc.h"
#include "write-data.h"
#include "exit.h"

/* Forward declaration of local functions.  */
static void usage (int status)
#if defined __GNUC__ && ((__GNUC__ == 2 && __GNUC_MINOR__ >= 5) || __GNUC__ > 2)
     __attribute__ ((noreturn))
#endif
;

/* Display usage information and exit.  */
static void
usage (int status)
{
  if (status != EXIT_SUCCESS)
    fprintf (stderr, "Try `%s --help' for more information.\n", program_name);
  else
    {
      printf ("Usage: %s DESTDIR LOCALE LOCALEALIAS ENDIANNESS\n", program_name);
      printf ("\n");
      fputs ("Report bugs to <bug-gnu-gettext@gnu.org>.\n", stdout);
    }

  exit (status);
}

/* Long options.  */
static const struct option long_options[] =
{
  { "help", no_argument, NULL, 'h' },
  { "version", no_argument, NULL, 'V' },
  { NULL, 0, NULL, 0 }
};

int
main (int argc, char *argv[])
{
  /* Default values for command line options.  */
  int do_help = 0;
  int do_version = 0;

  int opt;

  /* Set program name for message texts.  */
  set_program_name (argv[0]);

  /* Parse command line options.  */
  while ((opt = getopt_long (argc, argv, "hV", long_options, NULL)) != EOF)
    switch (opt)
    {
    case '\0':		/* Long option.  */
      break;
    case 'h':
      do_help = 1;
      break;
    case 'V':
      do_version = 1;
      break;
    default:
      usage (EXIT_FAILURE);
    }

  /* Version information is requested.  */
  if (do_version)
    {
      printf ("%s (GNU %s) %s\n", basename (program_name), PACKAGE, VERSION);
      /* xgettext: no-wrap */
      printf ("Copyright (C) %s Free Software Foundation, Inc.\n\
This is free software; see the source for copying conditions.  There is NO\n\
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n\
",
	      "2005");
      printf ("Written by %s.\n", "Bruno Haible");
      exit (EXIT_SUCCESS);
    }

  /* Help is requested.  */
  if (do_help)
    usage (EXIT_SUCCESS);

  if (argc - optind > 4)
    error (EXIT_FAILURE, 0, "too many arguments");

  if (argc - optind < 4)
    error (EXIT_FAILURE, 0, "too few arguments");

  {
    const char *destdir = argv[optind++];
    const char *localename = argv[optind++];
    const char *localealias = argv[optind++];
    const char *endianness_arg = argv[optind++];
    int endianness = -1;
    struct locale_data *data;

    if (strcmp (endianness_arg, "big") == 0)
      endianness = 1;
    else if (strcmp (endianness_arg, "little") == 0)
      endianness = 0;
    else
      error (EXIT_FAILURE, 0, "invalid endianness: %s", endianness_arg);

    /* Set the locale name for glibc.  */
    if (setlocale (LC_ALL, localealias) == NULL)
      {
	fprintf (stderr, "Cannot set locale to %s\n", localealias);
	exit (EXIT_FAILURE);
      }
    /* Unset some environment variables that can influence dgettext().  */
    unsetenv ("LANGUAGE");
    unsetenv ("OUTPUT_CHARSET");
    unsetenv ("GETTEXT_LOG_UNTRANSLATED");

    /* Extract the locale data from glibc.  */
    data = read_from_glibc ();

    /* Write it out.  */
    write_data_files (data, destdir, localename, endianness);
  }

  exit (EXIT_SUCCESS);
}
