/* Write locale data from memory to mmapable data files.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef WRITE_DATA_H
#define WRITE_DATA_H

#include "data.h"

/* Writes a locale data to mmapable data files.
   ENDIANNESS is 1 for big-endian output, 0 for little-endian output.  */
extern void write_data_files (struct locale_data *data, const char *prefix,
			      const char *localename, int endianness);

#endif /* WRITE_DATA_H */
