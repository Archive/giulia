/* Read locale data from glibc into memory.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "read-glibc.h"

#include <ctype.h>
#include <errno.h>
#include <langinfo.h>
#include <libintl.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>
#include "xalloc.h"

static void
errno_count (int errnum, const char *errname, const char *errmsg, void *p)
{
  *(size_t *)p += 1;
}

static void
errno_get_message (int errnum, const char *errname, const char *errmsg, void *p)
{
  struct locale_data *data = (struct locale_data *)p;
  struct strerror_data *nextp = &data->strerror_array[data->strerror_count];
  const char *translated_errmsg;
  /* Try the direct lookup in libc's catalog.  */
  translated_errmsg = dgettext ("libc", errmsg);
  if (translated_errmsg == errmsg)
    {
      /* Either libc's message catalog provides no translation, or
	 the current version of glibc uses a different English string.
	 Try using strerror here.  It's platform dependent, but let's hope
	 the best.  */
      if (errnum >= 0)
	translated_errmsg = xstrdup (strerror (errnum));
    }
  nextp->errname = errname;
  nextp->errmsg = translated_errmsg;
  data->strerror_count++;
}

/* Iterate over all errno values known to glibc (both Linux and Hurd).
   The list is taken from glibc-2.3.4/sysdeps/gnu/errlist.c.  */
static void
errno_iterate (void (*func) (int errnum, const char *errstr, const char *errmsg, void *p),
	       void *p)
{
  func (
#ifdef EPERM
	EPERM,
#else
	-1,
#endif
	"EPERM",
	"Operation not permitted",
	p);
  func (
#ifdef ENOENT
	ENOENT,
#else
	-1,
#endif
	"ENOENT",
	"No such file or directory",
	p);
  func (
#ifdef ESRCH
	ESRCH,
#else
	-1,
#endif
	"ESRCH",
	"No such process",
	p);
  func (
#ifdef EINTR
	EINTR,
#else
	-1,
#endif
	"EINTR",
	"Interrupted system call",
	p);
  func (
#ifdef EIO
	EIO,
#else
	-1,
#endif
	"EIO",
	"Input/output error",
	p);
  func (
#ifdef ENXIO
	ENXIO,
#else
	-1,
#endif
	"ENXIO",
	"No such device or address",
	p);
  func (
#ifdef E2BIG
	E2BIG,
#else
	-1,
#endif
	"E2BIG",
	"Argument list too long",
	p);
  func (
#ifdef ENOEXEC
	ENOEXEC,
#else
	-1,
#endif
	"ENOEXEC",
	"Exec format error",
	p);
  func (
#ifdef EBADF
	EBADF,
#else
	-1,
#endif
	"EBADF",
	"Bad file descriptor",
	p);
  func (
#ifdef ECHILD
	ECHILD,
#else
	-1,
#endif
	"ECHILD",
	"No child processes",
	p);
  func (
#ifdef EDEADLK
	EDEADLK,
#else
	-1,
#endif
	"EDEADLK",
	"Resource deadlock avoided",
	p);
  func (
#ifdef ENOMEM
	ENOMEM,
#else
	-1,
#endif
	"ENOMEM",
	"Cannot allocate memory",
	p);
  func (
#ifdef EACCES
	EACCES,
#else
	-1,
#endif
	"EACCES",
	"Permission denied",
	p);
  func (
#ifdef EFAULT
	EFAULT,
#else
	-1,
#endif
	"EFAULT",
	"Bad address",
	p);
  func (
#ifdef ENOTBLK
	ENOTBLK,
#else
	-1,
#endif
	"ENOTBLK",
	"Block device required",
	p);
  func (
#ifdef EBUSY
	EBUSY,
#else
	-1,
#endif
	"EBUSY",
	"Device or resource busy",
	p);
  func (
#ifdef EEXIST
	EEXIST,
#else
	-1,
#endif
	"EEXIST",
	"File exists",
	p);
  func (
#ifdef EXDEV
	EXDEV,
#else
	-1,
#endif
	"EXDEV",
	"Invalid cross-device link",
	p);
  func (
#ifdef ENODEV
	ENODEV,
#else
	-1,
#endif
	"ENODEV",
	"No such device",
	p);
  func (
#ifdef ENOTDIR
	ENOTDIR,
#else
	-1,
#endif
	"ENOTDIR",
	"Not a directory",
	p);
  func (
#ifdef EISDIR
	EISDIR,
#else
	-1,
#endif
	"EISDIR",
	"Is a directory",
	p);
  func (
#ifdef EINVAL
	EINVAL,
#else
	-1,
#endif
	"EINVAL",
	"Invalid argument",
	p);
  func (
#ifdef EMFILE
	EMFILE,
#else
	-1,
#endif
	"EMFILE",
	"Too many open files",
	p);
  func (
#ifdef ENFILE
	ENFILE,
#else
	-1,
#endif
	"ENFILE",
	"Too many open files in system",
	p);
  func (
#ifdef ENOTTY
	ENOTTY,
#else
	-1,
#endif
	"ENOTTY",
	"Inappropriate ioctl for device",
	p);
  func (
#ifdef ETXTBSY
	ETXTBSY,
#else
	-1,
#endif
	"ETXTBSY",
	"Text file busy",
	p);
  func (
#ifdef EFBIG
	EFBIG,
#else
	-1,
#endif
	"EFBIG",
	"File too large",
	p);
  func (
#ifdef ENOSPC
	ENOSPC,
#else
	-1,
#endif
	"ENOSPC",
	"No space left on device",
	p);
  func (
#ifdef ESPIPE
	ESPIPE,
#else
	-1,
#endif
	"ESPIPE",
	"Illegal seek",
	p);
  func (
#ifdef EROFS
	EROFS,
#else
	-1,
#endif
	"EROFS",
	"Read-only file system",
	p);
  func (
#ifdef EMLINK
	EMLINK,
#else
	-1,
#endif
	"EMLINK",
	"Too many links",
	p);
  func (
#ifdef EPIPE
	EPIPE,
#else
	-1,
#endif
	"EPIPE",
	"Broken pipe",
	p);
  func (
#ifdef EDOM
	EDOM,
#else
	-1,
#endif
	"EDOM",
	"Numerical argument out of domain",
	p);
  func (
#ifdef ERANGE
	ERANGE,
#else
	-1,
#endif
	"ERANGE",
	"Numerical result out of range",
	p);
  func (
#ifdef EAGAIN
	EAGAIN,
#else
	-1,
#endif
	"EAGAIN",
	"Resource temporarily unavailable",
	p);
  func (
#ifdef EINPROGRESS
	EINPROGRESS,
#else
	-1,
#endif
	"EINPROGRESS",
	"Operation now in progress",
	p);
  func (
#ifdef EALREADY
	EALREADY,
#else
	-1,
#endif
	"EALREADY",
	"Operation already in progress",
	p);
  func (
#ifdef ENOTSOCK
	ENOTSOCK,
#else
	-1,
#endif
	"ENOTSOCK",
	"Socket operation on non-socket",
	p);
  func (
#ifdef EMSGSIZE
	EMSGSIZE,
#else
	-1,
#endif
	"EMSGSIZE",
	"Message too long",
	p);
  func (
#ifdef EPROTOTYPE
	EPROTOTYPE,
#else
	-1,
#endif
	"EPROTOTYPE",
	"Protocol wrong type for socket",
	p);
  func (
#ifdef ENOPROTOOPT
	ENOPROTOOPT,
#else
	-1,
#endif
	"ENOPROTOOPT",
	"Protocol not available",
	p);
  func (
#ifdef EPROTONOSUPPORT
	EPROTONOSUPPORT,
#else
	-1,
#endif
	"EPROTONOSUPPORT",
	"Protocol not supported",
	p);
  func (
#ifdef ESOCKTNOSUPPORT
	ESOCKTNOSUPPORT,
#else
	-1,
#endif
	"ESOCKTNOSUPPORT",
	"Socket type not supported",
	p);
  func (
#ifdef EOPNOTSUPP
	EOPNOTSUPP,
#else
	-1,
#endif
	"EOPNOTSUPP",
	"Operation not supported",
	p);
  func (
#ifdef EPFNOSUPPORT
	EPFNOSUPPORT,
#else
	-1,
#endif
	"EPFNOSUPPORT",
	"Protocol family not supported",
	p);
  func (
#ifdef EAFNOSUPPORT
	EAFNOSUPPORT,
#else
	-1,
#endif
	"EAFNOSUPPORT",
	"Address family not supported by protocol",
	p);
  func (
#ifdef EADDRINUSE
	EADDRINUSE,
#else
	-1,
#endif
	"EADDRINUSE",
	"Address already in use",
	p);
  func (
#ifdef EADDRNOTAVAIL
	EADDRNOTAVAIL,
#else
	-1,
#endif
	"EADDRNOTAVAIL",
	"Cannot assign requested address",
	p);
  func (
#ifdef ENETDOWN
	ENETDOWN,
#else
	-1,
#endif
	"ENETDOWN",
	"Network is down",
	p);
  func (
#ifdef ENETUNREACH
	ENETUNREACH,
#else
	-1,
#endif
	"ENETUNREACH",
	"Network is unreachable",
	p);
  func (
#ifdef ENETRESET
	ENETRESET,
#else
	-1,
#endif
	"ENETRESET",
	"Network dropped connection on reset",
	p);
  func (
#ifdef ECONNABORTED
	ECONNABORTED,
#else
	-1,
#endif
	"ECONNABORTED",
	"Software caused connection abort",
	p);
  func (
#ifdef ECONNRESET
	ECONNRESET,
#else
	-1,
#endif
	"ECONNRESET",
	"Connection reset by peer",
	p);
  func (
#ifdef ENOBUFS
	ENOBUFS,
#else
	-1,
#endif
	"ENOBUFS",
	"No buffer space available",
	p);
  func (
#ifdef EISCONN
	EISCONN,
#else
	-1,
#endif
	"EISCONN",
	"Transport endpoint is already connected",
	p);
  func (
#ifdef ENOTCONN
	ENOTCONN,
#else
	-1,
#endif
	"ENOTCONN",
	"Transport endpoint is not connected",
	p);
  func (
#ifdef EDESTADDRREQ
	EDESTADDRREQ,
#else
	-1,
#endif
	"EDESTADDRREQ",
	"Destination address required",
	p);
  func (
#ifdef ESHUTDOWN
	ESHUTDOWN,
#else
	-1,
#endif
	"ESHUTDOWN",
	"Cannot send after transport endpoint shutdown",
	p);
  func (
#ifdef ETOOMANYREFS
	ETOOMANYREFS,
#else
	-1,
#endif
	"ETOOMANYREFS",
	"Too many references: cannot splice",
	p);
  func (
#ifdef ETIMEDOUT
	ETIMEDOUT,
#else
	-1,
#endif
	"ETIMEDOUT",
	"Connection timed out",
	p);
  func (
#ifdef ECONNREFUSED
	ECONNREFUSED,
#else
	-1,
#endif
	"ECONNREFUSED",
	"Connection refused",
	p);
  func (
#ifdef ELOOP
	ELOOP,
#else
	-1,
#endif
	"ELOOP",
	"Too many levels of symbolic links",
	p);
  func (
#ifdef ENAMETOOLONG
	ENAMETOOLONG,
#else
	-1,
#endif
	"ENAMETOOLONG",
	"File name too long",
	p);
  func (
#ifdef EHOSTDOWN
	EHOSTDOWN,
#else
	-1,
#endif
	"EHOSTDOWN",
	"Host is down",
	p);
  func (
#ifdef EHOSTUNREACH
	EHOSTUNREACH,
#else
	-1,
#endif
	"EHOSTUNREACH",
	"No route to host",
	p);
  func (
#ifdef ENOTEMPTY
	ENOTEMPTY,
#else
	-1,
#endif
	"ENOTEMPTY",
	"Directory not empty",
	p);
  func (
#ifdef EPROCLIM
	EPROCLIM,
#else
	-1,
#endif
	"EPROCLIM",
	"Too many processes",
	p);
  func (
#ifdef EUSERS
	EUSERS,
#else
	-1,
#endif
	"EUSERS",
	"Too many users",
	p);
  func (
#ifdef EDQUOT
	EDQUOT,
#else
	-1,
#endif
	"EDQUOT",
	"Disk quota exceeded",
	p);
  func (
#ifdef ESTALE
	ESTALE,
#else
	-1,
#endif
	"ESTALE",
	"Stale NFS file handle",
	p);
  func (
#ifdef EREMOTE
	EREMOTE,
#else
	-1,
#endif
	"EREMOTE",
	"Object is remote",
	p);
  func (
#ifdef EBADRPC
	EBADRPC,
#else
	-1,
#endif
	"EBADRPC",
	"RPC struct is bad",
	p);
  func (
#ifdef ERPCMISMATCH
	ERPCMISMATCH,
#else
	-1,
#endif
	"ERPCMISMATCH",
	"RPC version wrong",
	p);
  func (
#ifdef EPROGUNAVAIL
	EPROGUNAVAIL,
#else
	-1,
#endif
	"EPROGUNAVAIL",
	"RPC program not available",
	p);
  func (
#ifdef EPROGMISMATCH
	EPROGMISMATCH,
#else
	-1,
#endif
	"EPROGMISMATCH",
	"RPC program version wrong",
	p);
  func (
#ifdef EPROCUNAVAIL
	EPROCUNAVAIL,
#else
	-1,
#endif
	"EPROCUNAVAIL",
	"RPC bad procedure for program",
	p);
  func (
#ifdef ENOLCK
	ENOLCK,
#else
	-1,
#endif
	"ENOLCK",
	"No locks available",
	p);
  func (
#ifdef EFTYPE
	EFTYPE,
#else
	-1,
#endif
	"EFTYPE",
	"Inappropriate file type or format",
	p);
  func (
#ifdef EAUTH
	EAUTH,
#else
	-1,
#endif
	"EAUTH",
	"Authentication error",
	p);
  func (
#ifdef ENEEDAUTH
	ENEEDAUTH,
#else
	-1,
#endif
	"ENEEDAUTH",
	"Need authenticator",
	p);
  func (
#ifdef ENOSYS
	ENOSYS,
#else
	-1,
#endif
	"ENOSYS",
	"Function not implemented",
	p);
  func (
#ifdef EILSEQ
	EILSEQ,
#else
	-1,
#endif
	"EILSEQ",
	"Invalid or incomplete multibyte or wide character",
	p);
  func (
#ifdef EBACKGROUND
	EBACKGROUND,
#else
	-1,
#endif
	"EBACKGROUND",
	"Inappropriate operation for background process",
	p);
  func (
#ifdef EDIED
	EDIED,
#else
	-1,
#endif
	"EDIED",
	"Translator died",
	p);
#if JOKE
  func (
#ifdef ED
	ED,
#else
	-1,
#endif
	"ED",
	"?",
	p);
  func (
#ifdef EGREGIOUS
	EGREGIOUS,
#else
	-1,
#endif
	"EGREGIOUS",
	"You really blew it this time",
	p);
  func (
#ifdef EIEIO
	EIEIO,
#else
	-1,
#endif
	"EIEIO",
	"Computer bought the farm",
	p);
#endif /* JOKE */
  func (
#ifdef EGRATUITOUS
	EGRATUITOUS,
#else
	-1,
#endif
	"EGRATUITOUS",
	"Gratuitous error",
	p);
  func (
#ifdef EBADMSG
	EBADMSG,
#else
	-1,
#endif
	"EBADMSG",
	"Bad message",
	p);
  func (
#ifdef EIDRM
	EIDRM,
#else
	-1,
#endif
	"EIDRM",
	"Identifier removed",
	p);
  func (
#ifdef EMULTIHOP
	EMULTIHOP,
#else
	-1,
#endif
	"EMULTIHOP",
	"Multihop attempted",
	p);
  func (
#ifdef ENODATA
	ENODATA,
#else
	-1,
#endif
	"ENODATA",
	"No data available",
	p);
  func (
#ifdef ENOLINK
	ENOLINK,
#else
	-1,
#endif
	"ENOLINK",
	"Link has been severed",
	p);
  func (
#ifdef ENOMSG
	ENOMSG,
#else
	-1,
#endif
	"ENOMSG",
	"No message of desired type",
	p);
  func (
#ifdef ENOSR
	ENOSR,
#else
	-1,
#endif
	"ENOSR",
	"Out of streams resources",
	p);
  func (
#ifdef ENOSTR
	ENOSTR,
#else
	-1,
#endif
	"ENOSTR",
	"Device not a stream",
	p);
  func (
#ifdef EOVERFLOW
	EOVERFLOW,
#else
	-1,
#endif
	"EOVERFLOW",
	"Value too large for defined data type",
	p);
  func (
#ifdef EPROTO
	EPROTO,
#else
	-1,
#endif
	"EPROTO",
	"Protocol error",
	p);
  func (
#ifdef ETIME
	ETIME,
#else
	-1,
#endif
	"ETIME",
	"Timer expired",
	p);
  func (
#ifdef ECANCELED
	ECANCELED,
#else
	-1,
#endif
	"ECANCELED",
	"Operation canceled",
	p);
  func (
#ifdef ERESTART
	ERESTART,
#else
	-1,
#endif
	"ERESTART",
	"Interrupted system call should be restarted",
	p);
  func (
#ifdef ECHRNG
	ECHRNG,
#else
	-1,
#endif
	"ECHRNG",
	"Channel number out of range",
	p);
  func (
#ifdef EL2NSYNC
	EL2NSYNC,
#else
	-1,
#endif
	"EL2NSYNC",
	"Level 2 not synchronized",
	p);
  func (
#ifdef EL3HLT
	EL3HLT,
#else
	-1,
#endif
	"EL3HLT",
	"Level 3 halted",
	p);
  func (
#ifdef EL3RST
	EL3RST,
#else
	-1,
#endif
	"EL3RST",
	"Level 3 reset",
	p);
  func (
#ifdef ELNRNG
	ELNRNG,
#else
	-1,
#endif
	"ELNRNG",
	"Link number out of range",
	p);
  func (
#ifdef EUNATCH
	EUNATCH,
#else
	-1,
#endif
	"EUNATCH",
	"Protocol driver not attached",
	p);
  func (
#ifdef ENOCSI
	ENOCSI,
#else
	-1,
#endif
	"ENOCSI",
	"No CSI structure available",
	p);
  func (
#ifdef EL2HLT
	EL2HLT,
#else
	-1,
#endif
	"EL2HLT",
	"Level 2 halted",
	p);
  func (
#ifdef EBADE
	EBADE,
#else
	-1,
#endif
	"EBADE",
	"Invalid exchange",
	p);
  func (
#ifdef EBADR
	EBADR,
#else
	-1,
#endif
	"EBADR",
	"Invalid request descriptor",
	p);
  func (
#ifdef EXFULL
	EXFULL,
#else
	-1,
#endif
	"EXFULL",
	"Exchange full",
	p);
  func (
#ifdef ENOANO
	ENOANO,
#else
	-1,
#endif
	"ENOANO",
	"No anode",
	p);
  func (
#ifdef EBADRQC
	EBADRQC,
#else
	-1,
#endif
	"EBADRQC",
	"Invalid request code",
	p);
  func (
#ifdef EBADSLT
	EBADSLT,
#else
	-1,
#endif
	"EBADSLT",
	"Invalid slot",
	p);
  func (
#ifdef EBFONT
	EBFONT,
#else
	-1,
#endif
	"EBFONT",
	"Bad font file format",
	p);
  func (
#ifdef ENONET
	ENONET,
#else
	-1,
#endif
	"ENONET",
	"Machine is not on the network",
	p);
  func (
#ifdef ENOPKG
	ENOPKG,
#else
	-1,
#endif
	"ENOPKG",
	"Package not installed",
	p);
  func (
#ifdef EADV
	EADV,
#else
	-1,
#endif
	"EADV",
	"Advertise error",
	p);
  func (
#ifdef ESRMNT
	ESRMNT,
#else
	-1,
#endif
	"ESRMNT",
	"Srmount error",
	p);
  func (
#ifdef ECOMM
	ECOMM,
#else
	-1,
#endif
	"ECOMM",
	"Communication error on send",
	p);
  func (
#ifdef EDOTDOT
	EDOTDOT,
#else
	-1,
#endif
	"EDOTDOT",
	"RFS specific error",
	p);
  func (
#ifdef ENOTUNIQ
	ENOTUNIQ,
#else
	-1,
#endif
	"ENOTUNIQ",
	"Name not unique on network",
	p);
  func (
#ifdef EBADFD
	EBADFD,
#else
	-1,
#endif
	"EBADFD",
	"File descriptor in bad state",
	p);
  func (
#ifdef EREMCHG
	EREMCHG,
#else
	-1,
#endif
	"EREMCHG",
	"Remote address changed",
	p);
  func (
#ifdef ELIBACC
	ELIBACC,
#else
	-1,
#endif
	"ELIBACC",
	"Can not access a needed shared library",
	p);
  func (
#ifdef ELIBBAD
	ELIBBAD,
#else
	-1,
#endif
	"ELIBBAD",
	"Accessing a corrupted shared library",
	p);
  func (
#ifdef ELIBSCN
	ELIBSCN,
#else
	-1,
#endif
	"ELIBSCN",
	".lib section in a.out corrupted",
	p);
  func (
#ifdef ELIBMAX
	ELIBMAX,
#else
	-1,
#endif
	"ELIBMAX",
	"Attempting to link in too many shared libraries",
	p);
  func (
#ifdef ELIBEXEC
	ELIBEXEC,
#else
	-1,
#endif
	"ELIBEXEC",
	"Cannot exec a shared library directly",
	p);
  func (
#ifdef ESTRPIPE
	ESTRPIPE,
#else
	-1,
#endif
	"ESTRPIPE",
	"Streams pipe error",
	p);
  func (
#ifdef EUCLEAN
	EUCLEAN,
#else
	-1,
#endif
	"EUCLEAN",
	"Structure needs cleaning",
	p);
  func (
#ifdef ENOTNAM
	ENOTNAM,
#else
	-1,
#endif
	"ENOTNAM",
	"Not a XENIX named type file",
	p);
  func (
#ifdef ENAVAIL
	ENAVAIL,
#else
	-1,
#endif
	"ENAVAIL",
	"No XENIX semaphores available",
	p);
  func (
#ifdef EISNAM
	EISNAM,
#else
	-1,
#endif
	"EISNAM",
	"Is a named type file",
	p);
  func (
#ifdef EREMOTEIO
	EREMOTEIO,
#else
	-1,
#endif
	"EREMOTEIO",
	"Remote I/O error",
	p);
  func (
#ifdef ENOMEDIUM
	ENOMEDIUM,
#else
	-1,
#endif
	"ENOMEDIUM",
	"No medium found",
	p);
  func (
#ifdef EMEDIUMTYPE
	EMEDIUMTYPE,
#else
	-1,
#endif
	"EMEDIUMTYPE",
	"Wrong medium type",
	p);
}

struct locale_data *
read_from_glibc (void)
{
  struct locale_data *data =
    (struct locale_data *) xmalloc (sizeof (struct locale_data));

  init_locale_data (data);

  /* Character classes.  */
  {
    /* There's no API for listing all character classes.  But
       "grep charclass localedata/glibc/?*"
       shows all existing character classes.  */
    static const char *possible_wctype_names[] =
      {
	/* C89 <ctype.h> */
	"alnum", "alpha", "cntrl", "digit", "graph", "lower", "print", "punct",
	"space", "upper", "xdigit",
	/* C99 <ctype.h> */
	"blank",
	/* glibc ja_JP locale */
	"jspace", "jhira", "jkata", "jkanji", "jdigit",
	/* glibc ko_KR locale */
	"hangul", "hanja",
      };
    size_t maxcount = sizeof (possible_wctype_names) / sizeof (possible_wctype_names[0]);
    struct wctype_data *array =
      (struct wctype_data *) xmalloc (maxcount * sizeof (struct wctype_data));
    size_t i, count;

    count = 0;
    for (i = 0; i < maxcount; i++)
      {
	wctype_t desc = wctype (possible_wctype_names[i]);
	if (desc)
	  {
	    unsigned char *bits = (unsigned char *) xcalloc (0x110000 / 8, 1);
	    unsigned int uc;

	    for (uc = 0; uc < 0x110000; uc++)
	      if (iswctype (uc, desc))
		bits[uc / 8] |= 1 << (uc % 8);

	    array[count].name = possible_wctype_names[i];
	    array[count].bits = bits;
	    count++;
	  }
      }
    data->wctype_count = count;
    data->wctype_array = array;
  }

  /* Character mappings.  */
  {
    /* There's no API for listing all character mappings.  But
       "grep charconv localedata/glibc/?*"
       shows all existing character classes.  */
    static const char *possible_wctrans_names[] =
      {
	/* C89 <ctype.h> */
	"tolower", "toupper",
	/* glibc ja_JP locale */
	"tojhira", "tojkata",
      };
    size_t maxcount = sizeof (possible_wctrans_names) / sizeof (possible_wctrans_names[0]);
    struct wctrans_data *array =
      (struct wctrans_data *) xmalloc (maxcount * sizeof (struct wctrans_data));
    size_t i, count;

    count = 0;
    for (i = 0; i < maxcount; i++)
      {
	wctrans_t desc = wctrans (possible_wctrans_names[i]);
	if (desc)
	  {
	    unsigned int *values =
	      (unsigned int *) xmalloc (0x110000 * sizeof (unsigned int));
	    unsigned int uc;

	    for (uc = 0; uc < 0x110000; uc++)
	      values[uc] = towctrans (uc, desc);

	    array[count].name = possible_wctrans_names[i];
	    array[count].values = values;
	    count++;
	  }
      }
    data->wctrans_count = count;
    data->wctrans_array = array;
  }

  /* Character width.  */
  {
    signed char *values =
      (signed char *) xmalloc (0x110000 * sizeof (signed char));
    unsigned int uc;

    for (uc = 0; uc < 0x110000; uc++)
      values[uc] = wcwidth (uc);

    data->wcwidth_array = values;
  }

  /* Response interpretation.  */
  data->str_YESEXPR = nl_langinfo (YESEXPR);
  data->str_NOEXPR = nl_langinfo (NOEXPR);
  data->str_YESSTR = nl_langinfo (YESSTR);
  data->str_NOSTR = nl_langinfo (NOSTR);

  /* Error messages.  */
  bindtextdomain ("libc", "/usr/share/locale");
  {
    /* In a first pass, simply count how many errno values exist.  */
    size_t count = 0;
    errno_iterate (errno_count, &count);
    data->strerror_array =
      (struct strerror_data *) xmalloc (count * sizeof (struct strerror_data));
    /* In a second pass, actually fill the array with the error messages.  */
    data->strerror_count = 0;
    errno_iterate (errno_get_message, data);
    if (!(data->strerror_count == count))
      abort ();
  }

  return data;
}
