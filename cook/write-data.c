/* Write locale data from memory to mmapable data files.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "write-data.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pathname.h"
#include "pipe.h"
#include "wait-process.h"
#include "fwriteerror.h"
#include "xalloc.h"
#include "file-ctype.h"

#define uint32_t unsigned int

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

/* These output routines assume sizeof (unsigned short) == 2 and
   sizeof (unsigned int) == 4.  If you encounter a system where this is
   not the case, you need to review this entire file.  */
typedef int verify_sizeof_ushort[(sizeof (unsigned short) == 2) * 2 - 1];
typedef int verify_sizeof_uint[(sizeof (unsigned int) == 4) * 2 - 1];

static void
fwrite_byte (unsigned char value, FILE *file)
{
  fputc (value, file);
}

static void
fwrite_2bytes (unsigned short value, int endianness, FILE *file)
{
  if (endianness)
    {
      /* Big endian output.  */
      fputc ((unsigned char) (value >> 8), file);
      fputc ((unsigned char) (value & 0xff), file);
    }
  else
    {
      /* Little endian output.  */
      fputc ((unsigned char) (value & 0xff), file);
      fputc ((unsigned char) (value >> 8), file);
    }
}

static void
fwrite_4bytes (unsigned int value, int endianness, FILE *file)
{
  if (endianness)
    {
      /* Big endian output.  */
      fputc ((unsigned char) (value >> 24), file);
      fputc ((unsigned char) ((value >> 16) & 0xff), file);
      fputc ((unsigned char) ((value >> 8) & 0xff), file);
      fputc ((unsigned char) (value & 0xff), file);
    }
  else
    {
      /* Little endian output.  */
      fputc ((unsigned char) (value & 0xff), file);
      fputc ((unsigned char) ((value >> 8) & 0xff), file);
      fputc ((unsigned char) ((value >> 16) & 0xff), file);
      fputc ((unsigned char) (value >> 24), file);
    }
}

/* Open a data file for writing.  */
static FILE *
fopen_data_file (const char *filename)
{
  FILE *file = fopen (filename, "wb");

  if (file == NULL)
    {
      fprintf (stderr, "Could not create file %s\n", filename);
      exit (1);
    }
  return file;
}

/* Close a data file opened for writing.  */
static void
fclose_data_file (const char *filename, FILE *file)
{
  if (ferror (file) || fclose (file))
    {
      fprintf (stderr, "Error writing file %s\n", filename);
      exit (1);
    }
}

static char padding[4] = { 0, 0, 0, 0 };

/* Ensure a directory exists.  */
static void
ensure_directory (const char *directory)
{
  struct stat statbuf;
  if (!(stat (directory, &statbuf) >= 0 && S_ISDIR (statbuf.st_mode)))
    if (mkdir (directory, 0777) < 0)
      {
	fprintf (stderr, "Could not create directory %s\n", directory);
	exit (1);
      }
}

/* Output routines for PO files.
   It's not worth using "gettext-po.h" here.  */

/* Write the header of a PO file.  */
static void
fwrite_po_header (FILE *file)
{
  fprintf (file, "msgid \"\"\n");
  fprintf (file, "msgstr \"\"\n\
\"Project-Id-Version: GNU glocale\\n\"\
\"Last-Translator: Automatically generated\\n\"\
\"Language-Team: none\\n\"\
\"MIME-Version: 1.0\\n\"\
\"Content-Type: text/plain; charset=UTF-8\\n\"\
\"Content-Transfer-Encoding: 8bit\\n\"\
");
}

/* Write a string in escaped form to a PO file.  Assumes UTF-8 encoding.  */
static void
fwrite_po_string (const char *s, FILE *file)
{
  fputc ('"', file);
  for (; *s != '\0'; s++)
    switch (*s)
      {
      case '\a': fputs ("\\a", file); break;
      case '\b': fputs ("\\b", file); break;
      case '\f': fputs ("\\f", file); break;
      case '\n': fputs ("\\n", file); break;
      case '\r': fputs ("\\r", file); break;
      case '\t': fputs ("\\t", file); break;
      case '\v': fputs ("\\v", file); break;
      case '"': fputs ("\\\"", file); break;
      default: fputc ((unsigned char) *s, file); break;
      }
  fputc ('"', file);
}

/* Write a normal message to a PO file.  Assumes UTF-8 encoding.  */
static void
fwrite_po_message (const char *msgid, const char *msgstr, FILE *file)
{
  fputs ("\nmsgid ", file);
  fwrite_po_string (msgid, file);
  fputs ("\nmsgstr ", file);
  fwrite_po_string (msgstr, file);
  fputs ("\n", file);
}

/* ======================= LC_CTYPE locale data file ======================= */

#define TABLE wctype_table
#include "3levelbit.h"

struct wctype_temporary
{
  size_t header_offset;
  size_t header_size;
  size_t data_offset;
  size_t data_size;
  struct wctype_table threelevel_table;
};

#define TABLE wctrans_table
#define ELEMENT short
#define DEFAULT 0
#include "3level.h"

struct wctrans_temporary
{
  size_t header_offset;
  size_t header_size;
  size_t data_offset;
  size_t data_size;
  struct wctrans_table threelevel_table;
};

#define TABLE wcwidth_table
#define ELEMENT signed char
#define DEFAULT -1
#include "3level.h"

struct wcwidth_temporary
{
  size_t data_offset;
  size_t data_size;
  struct wcwidth_table threelevel_table;
};

static void
write_data_ctype (struct locale_data * data, int endianness, FILE *file)
{
  size_t header_offset;
  size_t header_wctype_offset;
  size_t header_wctrans_offset;
  size_t data_offset;
  size_t header_size;
  struct wctype_temporary *wctype_temp;
  struct wctrans_temporary *wctrans_temp;
  struct wcwidth_temporary wcwidth_temp;
  size_t i;

  /* ------ First run, to prepare and compute the offsets. ------  */
  header_offset = 3 * 4;
  data_offset = 0;

  /* Prepare the wctype entries.  */
  header_wctype_offset = header_offset;
  wctype_temp =
    (struct wctype_temporary *)
    xmalloc (data->wctype_count * sizeof (struct wctype_temporary));
  for (i = 0; i < data->wctype_count; i++)
    {
      struct wctype_temporary *temp = &wctype_temp[i];
      unsigned int uc;

      temp->threelevel_table.p = 4; /* or: 5 */
      temp->threelevel_table.q = 7; /* or: 6 */
      wctype_table_init (&temp->threelevel_table);
      for (uc = 0; uc < 0x110000; uc++)
	if ((data->wctype_array[i].bits[uc / 8] >> (uc % 8)) & 1)
	  wctype_table_add (&temp->threelevel_table, uc);
      wctype_table_finalize (&temp->threelevel_table);

      temp->header_offset = header_offset;
      temp->header_size = strlen (data->wctype_array[i].name) + 1;
      temp->data_offset = data_offset;
      temp->data_size = temp->threelevel_table.result_size;

      header_offset += temp->header_size;
      if (header_offset % 4)
	header_offset += 4 - (header_offset % 4);
      header_offset += 4;

      data_offset += temp->data_size;
      if (data_offset % 4)
        data_offset += 4 - (data_offset % 4);
    }
  header_offset += 4;

  /* Prepare the wctrans entries.  */
  header_wctrans_offset = header_offset;
  wctrans_temp =
    (struct wctrans_temporary *)
    xmalloc (data->wctrans_count * sizeof (struct wctrans_temporary));
  for (i = 0; i < data->wctrans_count; i++)
    {
      struct wctrans_temporary *temp = &wctrans_temp[i];
      unsigned int uc;

      temp->threelevel_table.p = 7;
      temp->threelevel_table.q = 9;
      wctrans_table_init (&temp->threelevel_table);
      for (uc = 0; uc < 0x110000; uc++)
	if (data->wctrans_array[i].values[uc] != uc)
	  {
	    int difference = data->wctrans_array[i].values[uc] - uc;
	    if ((short) difference != difference)
	      abort ();
	    wctrans_table_add (&temp->threelevel_table, uc, difference);
	  }
      wctrans_table_finalize (&temp->threelevel_table);

      temp->header_offset = header_offset;
      temp->header_size = strlen (data->wctrans_array[i].name) + 1;
      temp->data_offset = data_offset;
      temp->data_size = temp->threelevel_table.result_size;

      header_offset += temp->header_size;
      if (header_offset % 4)
	header_offset += 4 - (header_offset % 4);
      header_offset += 4;

      data_offset += temp->data_size;
      if (data_offset % 4)
        data_offset += 4 - (data_offset % 4);
    }
  header_offset += 4;

  /* Prepare the wcwidth entry.  */
  {
    struct wcwidth_temporary *temp = &wcwidth_temp;
    unsigned int uc;

    temp->threelevel_table.p = 7;
    temp->threelevel_table.q = 9;
    wcwidth_table_init (&temp->threelevel_table);
    for (uc = 0; uc < 0x110000; uc++)
      {
	signed char value = data->wcwidth_array[uc];
	if (value != -1)
	  wcwidth_table_add (&temp->threelevel_table, uc, value);
      }
    wcwidth_table_finalize (&temp->threelevel_table);

    temp->data_offset = data_offset;
    temp->data_size = temp->threelevel_table.result_size;

    data_offset += temp->data_size;
    if (data_offset % 4)
      data_offset += 4 - (data_offset % 4);
  }

  /* We now know the header's size.  */
  header_size = header_offset;

  /* ------ Second run, to write out the header. ------  */
  header_offset = 0;

  fwrite_4bytes (header_wctype_offset, endianness, file);
  header_offset += 4;

  fwrite_4bytes (header_wctrans_offset, endianness, file);
  header_offset += 4;

  fwrite_4bytes (header_size + wcwidth_temp.data_offset, endianness, file);
  header_offset += 4;

  /* Write out the wctype entries.  */
  for (i = 0; i < data->wctype_count; i++)
    {
      struct wctype_temporary *temp = &wctype_temp[i];

      ASSERT (header_offset == temp->header_offset);
      fwrite (data->wctype_array[i].name, 1, temp->header_size, file);
      header_offset += temp->header_size;
      if (header_offset % 4)
	{
	  fwrite (padding, 1, 4 - (header_offset % 4), file);
	  header_offset += 4 - (header_offset % 4);
	}
      fwrite_4bytes (header_size + temp->data_offset, endianness, file);
      header_offset += 4;
    }
  fwrite (padding, 1, 4, file);
  header_offset += 4;

  /* Write out the wctrans entries.  */
  for (i = 0; i < data->wctrans_count; i++)
    {
      struct wctrans_temporary *temp = &wctrans_temp[i];

      ASSERT (header_offset == temp->header_offset);
      fwrite (data->wctrans_array[i].name, 1, temp->header_size, file);
      header_offset += temp->header_size;
      if (header_offset % 4)
	{
	  fwrite (padding, 1, 4 - (header_offset % 4), file);
	  header_offset += 4 - (header_offset % 4);
	}
      fwrite_4bytes (header_size + temp->data_offset, endianness, file);
      header_offset += 4;
    }
  fwrite (padding, 1, 4, file);
  header_offset += 4;

  /* Nothing to write out for the wcwidth entry.  */

  ASSERT (header_offset == header_size);

  /* ------ Third run, to write out the data. ------  */
  data_offset = 0;

  /* Write out the wctype entries.  */
  for (i = 0; i < data->wctype_count; i++)
    {
      struct wctype_temporary *temp = &wctype_temp[i];
      unsigned int j;

      ASSERT (data_offset == temp->data_offset);
      for (j = 0; j < temp->threelevel_table.result_size / 4; j++)
	fwrite_4bytes (((unsigned int *)temp->threelevel_table.result)[j],
		       endianness, file);
      data_offset += temp->data_size;
    }

  /* Write out the wctrans entries.  */
  for (i = 0; i < data->wctrans_count; i++)
    {
      struct wctrans_temporary *temp = &wctrans_temp[i];
      unsigned int uint_count;
      unsigned int short_count;
      short *level3_start;
      unsigned int j;

      ASSERT (data_offset == temp->data_offset);
      uint_count =
	5
	+ temp->threelevel_table.level1_size
	+ (temp->threelevel_table.level2_size << temp->threelevel_table.q);
      short_count =
	(temp->threelevel_table.result_size - uint_count * sizeof (uint32_t))
	/ sizeof (short);
      for (j = 0; j < uint_count; j++)
	fwrite_4bytes (((unsigned int *)temp->threelevel_table.result)[j],
		       endianness, file);
      level3_start =
	(short *)((unsigned int *)temp->threelevel_table.result + uint_count);
      for (j = 0; j < short_count; j++)
	fwrite_2bytes (level3_start[j], endianness, file);
      data_offset += temp->data_size;
    }

  /* Write out the wcwidth entry.  */
  {
    struct wcwidth_temporary *temp = &wcwidth_temp;
    unsigned int uint_count;
    unsigned int schar_count;
    signed char *level3_start;
    unsigned int j;

    ASSERT (data_offset == temp->data_offset);
    uint_count =
      5
      + temp->threelevel_table.level1_size
      + (temp->threelevel_table.level2_size << temp->threelevel_table.q);
    schar_count =
      (temp->threelevel_table.result_size - uint_count * sizeof (uint32_t))
      / sizeof (signed char);
    for (j = 0; j < uint_count; j++)
      fwrite_4bytes (((unsigned int *)temp->threelevel_table.result)[j],
		     endianness, file);
    level3_start =
      (signed char *)((unsigned int *)temp->threelevel_table.result + uint_count);
    for (j = 0; j < schar_count; j++)
      fwrite_byte (level3_start[j], file);
    data_offset += temp->data_size;
  }

  free (wctrans_temp);
  free (wctype_temp);
}

/* ====================== LC_MESSAGES locale data file ===================== */

static void
write_po_messages (struct locale_data *data, FILE *file)
{
  size_t i;

  fwrite_po_header (file);

  /* Write out the response interpretation entries.  */
  fwrite_po_message ("YESEXPR", data->str_YESEXPR, file);
  fwrite_po_message ("NOEXPR", data->str_NOEXPR, file);
  fwrite_po_message ("YESSTR", data->str_YESSTR, file);
  fwrite_po_message ("NOSTR", data->str_NOSTR, file);

  /* Write out the error message entries.  */
  for (i = 0; i < data->strerror_count; i++)
    fwrite_po_message (data->strerror_array[i].errname,
		       data->strerror_array[i].errmsg,
		       file);
}

/* ========================================================================= */

static void
write_mo_file (struct locale_data *data, const char *directory, int endianness,
	       void (*write_po) (struct locale_data *, FILE *))
{
  char *filename = concatenated_pathname (directory, "glocale", ".mo");
  const char *progname;
  const char *prog_path;
  char *prog_argv[8];
  pid_t child;
  int fd[1];
  FILE *file;
  int exitstatus;

  /* Open a pipe to msgfmt.  */

  progname = MSGFMT; /* found through autoconf, set through the Makefile */
  prog_path = progname; /* no need for find_in_path optimization */
  prog_argv[0] = (char *) prog_path;
  prog_argv[1] = (char *) "--check-format";
  prog_argv[2] = (char *) "--use-untranslated";
  prog_argv[3] =
    (char *) (endianness ? "--endianness=big" : "--endianness=little");
  prog_argv[4] = (char *) "-o";
  prog_argv[5] = filename;
  prog_argv[6] = (char *) "-";
  prog_argv[7] = NULL;

  child = create_pipe_out (progname, prog_path, prog_argv,
			   NULL, false, true, true, fd);
  file = fdopen (fd[0], "wb");
  if (file == NULL)
    {
      fprintf (stderr, "fdopen() failed\n");
      exit (1);
    }

  /* Feed it the data in PO file syntax.  */
  write_po (data, file);

  /* Close the pipe.  */
  if (fwriteerror (file))
    fprintf (stderr, "error while writing to %s subprocess\n", progname);
  exitstatus = wait_subprocess (child, progname, true, false, true, true);
  if (exitstatus != 0)
    {
      fprintf (stderr, "%s subprocess failed with exit code %d\n",
	       progname, exitstatus);
      exit (1);
    }

  free (filename);
}

void
write_data_files (struct locale_data *data, const char *prefix,
		  const char *localename, int endianness)
{
  /* LC_MESSAGES: .mo files are platform independent.  */
  char *share_directory =
    concatenated_pathname (prefix, "share", NULL);
  char *share_locale_directory =
    concatenated_pathname (share_directory, "locale", NULL);
  char *share_locale_this_directory =
    concatenated_pathname (share_locale_directory, localename, NULL);
  /* All other data files are platform dependent.  */
  char *lib_directory =
    concatenated_pathname (prefix, "lib", NULL);
  char *lib_glocale_directory =
    concatenated_pathname (lib_directory, "glocale", NULL);
  char *lib_glocale_this_directory =
    concatenated_pathname (lib_glocale_directory, localename, NULL);

  /* Ensure the directories exist.  */
  ensure_directory (prefix);
  ensure_directory (share_directory);
  ensure_directory (share_locale_directory);
  ensure_directory (share_locale_this_directory);
  ensure_directory (lib_directory);
  ensure_directory (lib_glocale_directory);
  ensure_directory (lib_glocale_this_directory);

  /* Write the LC_CTYPE data file.  */
  {
    char *directory = concatenated_pathname (lib_glocale_this_directory, "LC_CTYPE", NULL);

    /* Ensure the directory exists.  */
    ensure_directory (directory);

    {
      char *filename = concatenated_pathname (directory, "ctype", ".loc");
      /* Create the file.  */
      FILE *file = fopen_data_file (filename);

      /* Write its contents.  */
      write_data_ctype (data, endianness, file);

      fclose_data_file (filename, file);

      free (filename);
    }
    free (directory);
  }

  /* Write the LC_MESSAGES data file.  */
  {
    char *directory = concatenated_pathname (share_locale_this_directory, "LC_MESSAGES", NULL);

    /* Ensure the directory exists.  */
    ensure_directory (directory);

    /* Create the file.  */
    write_mo_file (data, directory, endianness, write_po_messages);

    free (directory);
  }
}
