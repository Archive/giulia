/* In-memory format of locale data.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef DATA_H
#define DATA_H

#include <stddef.h>

/* All numbers are in the machine's endianness.
   All strings are UTF-8 strings.  */

/* LC_CTYPE auxiliary definitions.  */

/* Describes a character class.  */
struct wctype_data
{
  const char *name;             /* Name */
  const unsigned char *bits;    /* Bit array of length 0x110000,
				   8 bits per element */
};

/* Describes a character mapping.  */
struct wctrans_data
{
  const char *name;             /* Name */
  const unsigned int *values;   /* Array of length 0x110000 */
};

/* LC_MESSAGES auxiliary definitions.  */

/* Describes an error message.  */
struct strerror_data
{
  const char *errname;          /* Symbolic name of error, starting with E */
  const char *errmsg;           /* Textual error message */
};

/* Main locale data structure.  */

struct locale_data
{
  /* LC_CTYPE part.  */

  /* Character classes.  */
  size_t wctype_count;
  struct wctype_data *wctype_array;

  /* Character mappings.  */
  size_t wctrans_count;
  struct wctrans_data *wctrans_array;

  /* Character width.  */
  signed char *wcwidth_array;   /* Array of length 0x110000 */

  /* LC_MESSAGES part.  */

  /* Response interpretation.  */
  const char *str_YESEXPR;
  const char *str_NOEXPR;
  const char *str_YESSTR;
  const char *str_NOSTR;

  /* Error messages.  */
  size_t strerror_count;
  struct strerror_data *strerror_array;
};

/* Empty initialization of a struct locale_data.  */
static inline void
init_locale_data (struct locale_data *data)
{
  data->wctype_count = 0;
  data->wctype_array = NULL;
  data->wctrans_count = 0;
  data->wctrans_array = NULL;
  data->wcwidth_array = NULL;
  data->str_YESEXPR = NULL;
  data->str_NOEXPR = NULL;
  data->str_YESSTR = NULL;
  data->str_NOSTR = NULL;
  data->strerror_count = 0;
  data->strerror_array = NULL;
}

#endif /* DATA_H */
