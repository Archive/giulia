/* GNU locale functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_H
#define _GNU_LOCALE_H

#include <glocale/locale.h>
#include <glocale/ctype.h>
#include <glocale/langinfo.h>
#include <glocale/libintl.h>
#include <glocale/monetary.h>
#include <glocale/regex.h>
#include <glocale/stdio.h>
#include <glocale/stdlib.h>
#include <glocale/string.h>
#include <glocale/time.h>
#include <glocale/wchar.h>
#include <glocale/wctype.h>

#endif /* _GNU_LOCALE_H */
