/* GNU locale - wide character classification and mapping functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_WCTYPE_H
#define _GNU_LOCALE_WCTYPE_H

#include <glocale/config.h>
#include <glocale/locale.h>
#include <wchar.h>

#ifdef __cplusplus
extern "C" {
#endif

#if GLOCALE_ENABLE_WIDE_CHAR

extern int gl_iswalnum (wint_t __wc, gl_locale_t __locale);
extern int gl_iswalpha (wint_t __wc, gl_locale_t __locale);
extern int gl_iswcntrl (wint_t __wc, gl_locale_t __locale);
extern int gl_iswdigit (wint_t __wc, gl_locale_t __locale);
extern int gl_iswgraph (wint_t __wc, gl_locale_t __locale);
extern int gl_iswlower (wint_t __wc, gl_locale_t __locale);
extern int gl_iswprint (wint_t __wc, gl_locale_t __locale);
extern int gl_iswpunct (wint_t __wc, gl_locale_t __locale);
extern int gl_iswspace (wint_t __wc, gl_locale_t __locale);
extern int gl_iswupper (wint_t __wc, gl_locale_t __locale);
extern int gl_iswxdigit (wint_t __wc, gl_locale_t __locale);
extern int gl_iswblank (wint_t __wc, gl_locale_t __locale);

typedef struct gl_wctype_struct *gl_wctype_t;

/* Construct value that describes a class of wide characters identified
   by the string argument PROPERTY.  */
extern gl_wctype_t gl_wctype (const char *__property, gl_locale_t __locale);

/* Determine whether the wide-character WC has the property described by
   DESC.  */
extern int gl_iswctype (wint_t __wc, gl_wctype_t __desc, gl_locale_t __locale);

/* Converts an uppercase letter to the corresponding lowercase letter.  */
extern wint_t gl_towlower (wint_t __wc, gl_locale_t __locale);

/* Converts an lowercase letter to the corresponding uppercase letter.  */
extern wint_t gl_towupper (wint_t __wc, gl_locale_t __locale);

typedef struct gc_wctrans_struct *gl_wctrans_t;

/* Construct value that describes a mapping between wide characters
   identified by the string argument PROPERTY.  */
extern gl_wctrans_t gl_wctrans (const char *__property, gl_locale_t __locale);

/* Map the wide character WC using the mapping described by DESC.  */
extern wint_t gl_towctrans (wint_t __wc, gl_wctrans_t __desc,
			    gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_WCTYPE_H */
