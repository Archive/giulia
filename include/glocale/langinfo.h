/* GNU locale - functions for accessing language dependent information.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_LANGINFO_H
#define _GNU_LOCALE_LANGINFO_H

#include <glocale/locale.h>
#include <langinfo.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Return the current locale's value for ITEM.
   If ITEM is invalid, an empty string is returned.
   The string returned will not change.  */
extern const char * gl_nl_langinfo (nl_item __item, gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_LANGINFO_H */
