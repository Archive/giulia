/* GNU locale - general conversion functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_STDLIB_H
#define _GNU_LOCALE_STDLIB_H

#include <glocale/config.h>
#include <glocale/locale.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Convert a string to a floating-point number.  */
extern float gl_strtof (const char *__nptr,
			char **__endptr,
			gl_locale_t __locale);

/* Convert a string to a floating-point number.  */
extern double gl_strtod (const char *__nptr,
			 char **__endptr,
			 gl_locale_t __locale);

#if GLOCALE_HAVE_LONG_DOUBLE

/* Convert a string to a floating-point number.  */
extern long double gl_strtold (const char *__nptr,
			       char **__endptr,
			       gl_locale_t __locale);

#endif

/* Convert a string to a long integer.  */
extern long gl_strtol (const char *__nptr,
		       char **__endptr, int __base,
		       gl_locale_t __locale);
/* Convert a string to an unsigned long integer.  */
extern unsigned long gl_strtoul (const char *__nptr,
				 char **__endptr, int __base,
				 gl_locale_t __locale);

#if GLOCALE_HAVE_LONG_LONG

/* Convert a string to a quadword integer.  */
extern long long gl_strtoll (const char *__nptr,
			     char **__endptr, int __base,
			     gl_locale_t __locale);
/* Convert a string to an unsigned quadword integer.  */
extern unsigned long long gl_strtoull (const char *__nptr,
				       char **__endptr, int __base,
				       gl_locale_t __locale);

#endif

#endif

/* Return the length of the multibyte character
   in S, which is no longer than N.  */
extern int gl_mblen (const char *__s, size_t __n, gl_locale_t __locale);

#if GLOCALE_ENABLE_WIDE_CHAR

/* Maximum length of a multibyte character.  */
extern size_t gl_mb_cur_max (gl_locale_t __locale);

/* Return the length of the given multibyte character,
   putting its 'wchar_t' representation in *PWC.  */
extern int gl_mbtowc (wchar_t *__pwc,
		      const char *__s, size_t __n,
		      gl_locale_t __locale);

/* Put the multibyte character represented
   by WCHAR in S, returning its length.  */
extern int gl_wctomb (char *__s, wchar_t __wchar,
		      gl_locale_t __locale);

/* Convert a multibyte string to a wide char string.  */
extern size_t gl_mbstowcs (wchar_t *__pwcs,
			   const char *__s, size_t __n,
			   gl_locale_t __locale);

/* Convert a wide char string to multibyte string.  */
extern size_t gl_wcstombs (char *__s,
			   const wchar_t *__pwcs, size_t __n,
			   gl_locale_t __locale);

#endif

#if NotYetImplemented

/* Determine whether the string value of RESPONSE matches the affirmation
   or negative response expression as specified by the LC_MESSAGES category
   in the program's current locale.  Returns 1 if affirmative, 0 if
   negative, and -1 if not matching.  */
extern int gl_rpmatch (const char *__response, gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_STDLIB_H */
