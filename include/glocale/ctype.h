/* GNU locale - char handling functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_CTYPE_H
#define _GNU_LOCALE_CTYPE_H

#include <glocale/locale.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int gl_isalnum (int __c, gl_locale_t __locale);
extern int gl_isalpha (int __c, gl_locale_t __locale);
extern int gl_iscntrl (int __c, gl_locale_t __locale);
extern int gl_isdigit (int __c, gl_locale_t __locale);
extern int gl_isgraph (int __c, gl_locale_t __locale);
extern int gl_islower (int __c, gl_locale_t __locale);
extern int gl_isprint (int __c, gl_locale_t __locale);
extern int gl_ispunct (int __c, gl_locale_t __locale);
extern int gl_isspace (int __c, gl_locale_t __locale);
extern int gl_isupper (int __c, gl_locale_t __locale);
extern int gl_isxdigit (int __c, gl_locale_t __locale);
extern int gl_isblank (int __c, gl_locale_t __locale);

extern int gl_toupper (int __c, gl_locale_t __locale);
extern int gl_tolower (int __c, gl_locale_t __locale);

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_CTYPE_H */
