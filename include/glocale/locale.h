/* GNU locale - locale management functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_LOCALE_H
#define _GNU_LOCALE_LOCALE_H

#include <locale.h>

/* Assume that all systems define the LC_* constants to nonnegative values.  */
#ifndef LC_MESSAGES
#define  LC_MESSAGES -1
#endif
#ifndef LC_PAPER
#define  LC_PAPER -2
#endif
#ifndef LC_NAME
#define  LC_NAME -3
#endif
#ifndef LC_ADDRESS
#define  LC_ADDRESS -4
#endif
#ifndef LC_TELEPHONE
#define  LC_TELEPHONE -5
#endif
#ifndef LC_MEASUREMENT
#define  LC_MEASUREMENT -6
#endif
#ifndef LC_IDENTIFICATION
#define  LC_IDENTIFICATION -7
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef struct glocale *gl_locale_t;

/* Create a new locale.  */
extern gl_locale_t gl_newlocale (void);

/* Duplicate a locale.  */
extern gl_locale_t gl_duplocale (gl_locale_t __old_locale);

/* Change a single category (or, if category is LC_ALL, all categories) of
   the given locale according to the given locale name.  */
extern const char * gl_setlocale (gl_locale_t __locale, int __category,
				  const char *__value);

/* Change the encoding of the given locale.
   This is equivalent to gl_setlocale (locale, LC_CTYPE, "en_US."+encoding).  */
extern void gl_setencoding (gl_locale_t __locale, const char *__encoding);

/* Free a locale.  */
extern void gl_freelocale (gl_locale_t __locale);

#if NotYetImplemented

/* Return the numeric/monetary information for the given locale.  */
extern struct lconv * gl_localeconv (gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_LOCALE_H */
