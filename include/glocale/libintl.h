/* GNU locale - functions accessing message catalogs.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_LIBINTL_H
#define _GNU_LOCALE_LIBINTL_H

#include <glocale/locale.h>

/* Declaration of bindtextdomain().  */
#include <libintl.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Look up MSGID in the DOMAINNAME message catalog for the current
   LC_MESSAGES locale.  */
extern const char * gl_dgettext (const char *__domainname,
				 const char *__msgid,
				 gl_locale_t __locale)
#if __GNUC__ >= 3
  __attribute__ ((__format_arg__ (2)))
#endif
;

/* Look up MSGID in the DOMAINNAME message catalog for the current CATEGORY
   locale.  */
extern const char * gl_dcgettext (const char *__domainname,
				  const char *__msgid,
				  int __category,
				  gl_locale_t __locale)
#if __GNUC__ >= 3
  __attribute__ ((__format_arg__ (2)))
#endif
;

/* Similar to 'gl_dgettext' but select the plural form corresponding to the
   number N.  */
extern const char * gl_dngettext (const char *__domainname,
				  const char *__msgid1, const char *__msgid2,
				  unsigned long int __n,
				  gl_locale_t __locale)
#if __GNUC__ >= 3
  __attribute__ ((__format_arg__ (2))) __attribute__ ((__format_arg__ (3)))
#endif
;

/* Similar to 'gl_dcgettext' but select the plural form corresponding to the
   number N.  */
extern const char * gl_dcngettext (const char *__domainname,
				   const char *__msgid1, const char *__msgid2,
				   unsigned long int __n,
				   int __category,
				   gl_locale_t __locale)
#if __GNUC__ >= 3
  __attribute__ ((__format_arg__ (2))) __attribute__ ((__format_arg__ (3)))
#endif
;

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_LIBINTL_H */
