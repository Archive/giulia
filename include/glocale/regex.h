/* GNU locale - regular expression matching functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_REGEX_H
#define _GNU_LOCALE_REGEX_H

#include <glocale/locale.h>
#include <regex.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

extern int gl_regcomp (regex_t *__preg,
		       const char *__pattern, int __cflags,
		       gl_locale_t __locale);

extern int gl_regexec (const regex_t *__preg,
		       const char *__string,
		       size_t __nmatch, regmatch_t __pmatch[__restrict_arr],
		       int __eflags,
		       gl_locale_t __locale);

extern size_t gl_regerror (int __errcode, const regex_t *__preg,
			   char *__errbuf, size_t __errbuf_size,
			   gl_locale_t __locale);

extern void gl_regfree (regex_t *__preg);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_REGEX_H */
