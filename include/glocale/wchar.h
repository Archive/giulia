/* GNU locale - wide string conversion and comparison functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_WCHAR_H
#define _GNU_LOCALE_WCHAR_H

#include <glocale/config.h>
#include <glocale/locale.h>
#include <wchar.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

#if GLOCALE_ENABLE_WIDE_CHAR

/* Compare S1 and S2, ignoring case.  */
extern int gl_wcscasecmp (const wchar_t *__s1, const wchar_t *__s2,
			  gl_locale_t __locale);

/* Compare no more than N chars of S1 and S2, ignoring case.  */
extern int gl_wcsncasecmp (const wchar_t *__s1, const wchar_t *__s2,
			   size_t __n,
			   gl_locale_t __locale);

#if NotYetImplemented

/* Compare S1 and S2, both interpreted as appropriate to the
   LC_COLLATE category of the current locale.  */
extern int gl_wcscoll (const wchar_t *__s1, const wchar_t *__s2,
		       gl_locale_t __locale);

/* Transform S2 into array pointed to by S1 such that if wcscmp is
   applied to two transformed strings the result is the as applying
   'wcscoll' to the original strings.  */
extern size_t gl_wcsxfrm (wchar_t *__s1,
			  const wchar_t *__s2,
			  size_t __n,
			  gl_locale_t __locale);

#endif

/* Determine whether C constitutes a valid (one-byte) multibyte
   character.  */
extern wint_t gl_btowc (int __c, gl_locale_t __locale);

/* Determine whether C corresponds to a member of the extended
   character set whose multibyte representation is a single byte.  */
extern int gl_wctob (wint_t __c, gl_locale_t __locale);

#endif

/* Determine whether PS points to an object representing the initial
   state.  */
extern int gl_mbsinit (const mbstate_t *__ps, gl_locale_t __locale);

#if GLOCALE_ENABLE_WIDE_CHAR

/* Write wide character representation of multibyte character pointed
   to by S to PWC.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_mbrtowc (wchar_t *__pwc,
			  const char *__s, size_t __n,
			  mbstate_t *__ps,
			  gl_locale_t __locale);

/* Write multibyte representation of wide character WC to S.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_wcrtomb (char *__s, wchar_t __wc,
			  mbstate_t *__ps,
			  gl_locale_t __locale);

#endif

/* Return number of bytes in multibyte character pointed to by S.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_mbrlen (const char *__s, size_t __n,
			 mbstate_t *__ps,
			 gl_locale_t __locale);

#if GLOCALE_ENABLE_WIDE_CHAR

/* Write wide character representation of multibyte character string
   SRC to DST.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_mbsrtowcs (wchar_t *__dst,
			    const char **__src,
			    size_t __len,
			    mbstate_t *__ps,
			    gl_locale_t __locale);

/* Write multibyte character representation of wide character string
   SRC to DST.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_wcsrtombs (char *__dst,
			    const wchar_t **__src,
			    size_t __len,
			    mbstate_t *__ps,
			    gl_locale_t __locale);

/* Write wide character representation of at most NMC bytes of the
   multibyte character string SRC to DST.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_mbsnrtowcs (wchar_t *__dst,
			     const char **__src,
			     size_t __nmc, size_t __len,
			     mbstate_t *__ps,
			     gl_locale_t __locale);

/* Write multibyte character representation of at most NWC characters
   from the wide character string SRC to DST.
   If PS is NULL, an pointer to a thread-specific state known only to this
   function is used instead.  */
extern size_t gl_wcsnrtombs (char *__dst,
			     const wchar_t **__src,
			     size_t __nwc, size_t __len,
			     mbstate_t *__ps,
			     gl_locale_t __locale);

/* Determine number of column positions required for C.  */
extern int gl_wcwidth (wchar_t __c,
		       gl_locale_t __locale);

/* Determine number of column positions required for first N wide
   characters (or fewer if S ends before this) in S.  */
extern int gl_wcswidth (const wchar_t *__s, size_t __n,
			gl_locale_t __locale);

#if NotYetImplemented

/* Convert initial portion of the wide string NPTR to 'float'
   representation.  */
extern float gl_wcstof (const wchar_t *__nptr,
			wchar_t **__endptr,
			gl_locale_t __locale);

/* Convert initial portion of the wide string NPTR to 'double'
   representation.  */
extern double gl_wcstod (const wchar_t *__nptr,
			 wchar_t **__endptr,
			 gl_locale_t __locale);

# if GLOCALE_HAVE_LONG_DOUBLE

/* Convert initial portion of the wide string NPTR to 'long double'
   representation.  */
extern long double gl_wcstold (const wchar_t *__nptr,
			       wchar_t **__endptr,
			       gl_locale_t __locale);

# endif

/* Convert initial portion of wide string NPTR to 'long int'
   representation.  */
extern long gl_wcstol (const wchar_t *__nptr,
		       wchar_t **__endptr, int __base,
		       gl_locale_t __locale);
/* Convert initial portion of wide string NPTR to 'unsigned long int'
   representation.  */
extern unsigned long gl_wcstoul (const wchar_t *__nptr,
				 wchar_t **__endptr, int __base,
				 gl_locale_t __locale);

# if GLOCALE_HAVE_LONG_LONG

/* Convert initial portion of wide string NPTR to 'long int'
   representation.  */
extern long long gl_wcstoll (const wchar_t *__nptr,
			     wchar_t **__endptr, int __base,
			     gl_locale_t __locale);

/* Convert initial portion of wide string NPTR to 'unsigned long long int'
   representation.  */
extern unsigned long long gl_wcstoull (const wchar_t *__nptr,
				       wchar_t **__endptr,
				       int __base,
				       gl_locale_t __locale);

# endif

/* Write formatted output to STREAM.  */
extern int gl_fwprintf (FILE *__stream,
			gl_locale_t __locale,
			const wchar_t *__format, ...);

/* Write formatted output to stdout.  */
extern int gl_wprintf (gl_locale_t __locale,
		       const wchar_t *__format, ...);

/* Write formatted output of at most N characters to S.  */
extern int gl_swprintf (wchar_t *__s, size_t __n,
			gl_locale_t __locale,
			const wchar_t *__format, ...);

/* Write formatted output to S from argument list ARG.  */
extern int gl_vfwprintf (FILE *__s,
			 gl_locale_t __locale,
			 const wchar_t *__format, va_list __arg);

/* Write formatted output to stdout from argument list ARG.  */
extern int gl_vwprintf (gl_locale_t __locale,
			const wchar_t *__format, va_list __arg);

/* Write formatted output of at most N character to S from argument
   list ARG.  */
extern int gl_vswprintf (wchar_t *__s, size_t __n,
			 gl_locale_t __locale,
			 const wchar_t *__format, va_list __arg);

/* Read formatted input from STREAM.  */
extern int gl_fwscanf (FILE *__stream,
		       gl_locale_t __locale,
		       const wchar_t *__format, ...);

/* Read formatted input from stdin.  */
extern int gl_wscanf (gl_locale_t __locale,
		      const wchar_t *__format, ...);

/* Read formatted input from S.  */
extern int gl_swscanf (const wchar_t *__s,
		       gl_locale_t __locale,
		       const wchar_t *__format, ...);

/* Read formatted input from S into argument list ARG.  */
extern int gl_vfwscanf (FILE *__s,
			gl_locale_t __locale,
			const wchar_t *__format, va_list __arg);

/* Read formatted input from stdin into argument list ARG.  */
extern int gl_vwscanf (gl_locale_t __locale,
		       const wchar_t *__format, va_list __arg);

/* Read formatted input from S into argument list ARG.  */
extern int gl_vswscanf (const wchar_t *__s,
			gl_locale_t __locale,
			const wchar_t *__format, va_list __arg);

/* Format TP into S according to FORMAT.
   Write no more than MAXSIZE wide characters and return the number
   of wide characters written, or 0 if it would exceed MAXSIZE.  */
extern size_t gl_wcsftime (wchar_t *__s, size_t __maxsize,
			   gl_locale_t __locale,
			   const wchar_t *__format,
			   const struct tm *__tp);

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_WCHAR_H */
