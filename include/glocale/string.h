/* GNU locale - string comparison functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_STRING_H
#define _GNU_LOCALE_STRING_H

#include <glocale/locale.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Compare the collated forms of S1 and S2.  */
extern int gl_strcoll (const char *__s1, const char *__s2,
		       gl_locale_t __locale);

/* Put a transformation of SRC into no more than N bytes of DEST.  */
extern size_t gl_strxfrm (char *__dest,
			  const char *__src, size_t __n,
			  gl_locale_t __locale);

#endif

/* Compare strings S1 and S2, ignoring case, returning less than, equal to or
   greater than zero if S1 is lexicographically less than, equal to or greater
   than S2.
   Note: This function may, in multibyte locales, return 0 for strings of
   different lengths!  */
extern int gl_strcasecmp (const char *__s1, const char *__s2,
			  gl_locale_t __locale);

#if NotImplementable

/* Compare no more than N characters of strings S1 and S2, ignoring case,
   returning less than, equal to or greater than zero if S1 is
   lexicographically less than, equal to or greater than S2.
   Note: This function is not implemented because it can not work correctly
   in multibyte locales.  */
extern int gl_strncasecmp (const char *__s1, const char *__s2, size_t __n,
			   gl_locale_t __locale);

#endif

/* Find the first occurrence of NEEDLE in HAYSTACK.  */
extern char *gl_strstr (const char *__haystack, const char *__needle,
			gl_locale_t __locale);

/* Find the first occurrence of NEEDLE in HAYSTACK, using case-insensitive
   comparison.
   Note: This function may, in multibyte locales, return success even if
   strlen (haystack) < strlen (needle) !  */
extern char *gl_strcasestr (const char *__haystack, const char *__needle,
			    gl_locale_t __locale);

/* Returns the number of screen columns needed for STRING.  */
extern int gl_strwidth (const char *__string, gl_locale_t __locale);

/* Returns the number of screen columns needed for the NBYTES bytes
   starting at BUF.  */
extern int gl_strnwidth (const char *__buf, size_t __nbytes,
			 gl_locale_t __locale);

/* Return a string describing the meaning of the 'errno' code in ERRNUM.  */
extern char *gl_strerror (int __errnum, gl_locale_t __locale);

/* Put a string describing the meaning of the 'errno' code in ERRNUM into
   BUF, using at most BUFLEN bytes of BUF.
   Note: This is similar to the POSIX strerror_r function.  The GNU libc's
   strerror_r function has a different calling convention!  */
extern int gl_strerror_r (int __errnum, char *__buf, size_t __buflen,
			  gl_locale_t __locale);

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_STRING_H */
