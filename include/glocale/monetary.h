/* GNU locale - monetary value formatting functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_MONETARY_H
#define _GNU_LOCALE_MONETARY_H

#include <glocale/locale.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Formatting a monetary value according to the current locale.  */
extern ssize_t gl_strfmon (char *__s, size_t __maxsize,
			   gl_locale_t __locale,
			   const char *__format, ...)
#if __GNUC__ >= 3
  __attribute__ ((__format__ (__strfmon__, 4, 5)))
#endif
;

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_MONETARY_H */
