/* GNU locale - standard input/output functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_STDIO_H
#define _GNU_LOCALE_STDIO_H

#include <glocale/locale.h>
#include <stdio.h>
#include <stdarg.h>

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 5) || __STRICT_ANSI__
#  define __attribute__(Spec) /* empty */
# endif
/* The __-protected variants of 'format' and 'printf' attributes
   are accepted by gcc versions 2.6.4 (effectively 2.7) and later.  */
# if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 7)
#  define __format__ format
#  define __printf__ printf
# endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Write formatted output to STREAM.  */
extern int gl_fprintf (FILE *__stream,
		       gl_locale_t __locale,
		       const char *__format, ...);

/* Write formatted output to stdout.  */
extern int gl_printf (gl_locale_t __locale,
		      const char *__format, ...);

/* Write formatted output to S.  */
extern int gl_sprintf (char *__s,
		       gl_locale_t __locale,
		       const char *__format, ...);

/* Write formatted output to S from argument list ARG.  */
extern int gl_vfprintf (FILE *__s,
			gl_locale_t __locale,
			const char *__format, va_list __arg);

/* Write formatted output to stdout from argument list ARG.  */
extern int gl_vprintf (gl_locale_t __locale,
		       const char *__format, va_list __arg);

/* Write formatted output to S from argument list ARG.  */
extern int gl_vsprintf (char *__s,
			gl_locale_t __locale,
			const char *__format, va_list __arg);

/* Maximum chars of output to write in MAXLEN.  */
extern int gl_snprintf (char *__s, size_t __maxlen,
			gl_locale_t __locale,
			const char *__format, ...)
     __attribute__ ((__format__ (__printf__, 4, 5)));

extern int gl_vsnprintf (char *__s, size_t __maxlen,
			 gl_locale_t __locale,
			 const char *__format, va_list __arg)
     __attribute__ ((__format__ (__printf__, 4, 0)));

/* Write formatted output to a string dynamically allocated with 'malloc'.
   Store the address of the string in *PTR.  */
extern int gl_vasprintf (char **__ptr,
			 gl_locale_t __locale,
			 const char *__format, va_list __arg)
     __attribute__ ((__format__ (__printf__, 3, 0)));

extern int gl_asprintf (char **__ptr,
			gl_locale_t __locale,
			const char *__format, ...)
     __attribute__ ((__format__ (__printf__, 3, 4)));

/* Read formatted input from STREAM.  */
extern int gl_fscanf (FILE *__stream,
		      gl_locale_t __locale,
		      const char *__format, ...);

/* Read formatted input from stdin.  */
extern int gl_scanf (gl_locale_t __locale,
		     const char *__format, ...);

/* Read formatted input from S.  */
extern int gl_sscanf (const char *__s,
		      gl_locale_t __locale,
		      const char *__format, ...);

/* Read formatted input from S into argument list ARG.  */
extern int gl_vfscanf (FILE *__s,
		       gl_locale_t __locale,
		       const char *__format, va_list __arg)
     __attribute__ ((__format__ (__scanf__, 3, 0)));

/* Read formatted input from stdin into argument list ARG.  */
extern int gl_vscanf (gl_locale_t __locale,
		      const char *__format, va_list __arg)
     __attribute__ ((__format__ (__scanf__, 2, 0)));

/* Read formatted input from S into argument list ARG.  */
extern int gl_vsscanf (const char *__s,
		       gl_locale_t __locale,
		       const char *__format, va_list __arg)
     __attribute__ ((__format__ (__scanf__, 3, 0)));

/* Print a message describing the meaning of the value of errno.  */
extern void gl_perror (const char *__s,
		       gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_STDIO_H */
