/* GNU locale - date and time formatting functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_TIME_H
#define _GNU_LOCALE_TIME_H

#include <glocale/locale.h>
#include <glocale/time.h>

#ifdef __cplusplus
extern "C" {
#endif

#if NotYetImplemented

/* Format TP into S according to FORMAT.
   Write no more than MAXSIZE characters and return the number
   of characters written, or 0 if it would exceed MAXSIZE.  */
extern size_t gl_strftime (char *__s, size_t __maxsize,
			   const char *__format,
			   const struct tm *__tp,
			   gl_locale_t __locale);

/* Parse S according to FORMAT and store binary time information in TP.
   The return value is a pointer to the first unparsed character in S.  */
extern char * gl_strptime (const char *__s,
			   const char *__format, struct tm *__tp,
			   gl_locale_t __locale);

#endif

#ifdef __cplusplus
}
#endif

#endif /* _GNU_LOCALE_TIME_H */
