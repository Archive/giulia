/* Convert a multi-byte string to a wide character string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/stdlib.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"

size_t
gl_mbstowcs (wchar_t *restrict dest, const char *restrict src, size_t len,
	     gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  iconv_t cd = facet->cd_to_utf8;
  size_t result;

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    {
      gl_lock_lock (facet->iconv_to_lock);
      iconv (cd, NULL, NULL, NULL, NULL);
    }

  if (dest != NULL)
    {
      wchar_t *destptr = dest;
      for (; ; destptr++, len--)
	if (len > 0)
	  {
	    size_t incoming = gl_strnlen1 (src, GL_MB_LEN_MAX);
	    unsigned int uc;
	    result = facet->mbtouc (&uc, src, incoming, 1, cd);
	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      {
		errno = EILSEQ;
		 break;
	      }
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence.  */
	      abort ();
	    else
	      {
		/* Successful conversion to UCS-4.  */
		size_t inbytes = result;
		src += inbytes;
		*destptr = uc;
		if (uc == 0)
		  {
		    result = destptr - dest;
		    break;
		  }
	      }
	  }
	else
	  {
	    result = destptr - dest;
	    break;
	  }
    }
  else
    {
      /* Ignore dest and len.  */
      size_t totalcount = 0;
      for (; ; totalcount++)
	{
	  size_t incoming = gl_strnlen1 (src, GL_MB_LEN_MAX);
	  unsigned int uc;
	  result = facet->mbtouc (&uc, src, incoming, 1, cd);
	  if (result == (size_t)(-1))
	    /* Invalid multibyte sequence.  */
	    {
	      errno = EILSEQ;
	      break;
	    }
	  else if (result == (size_t)(-2))
	    /* Incomplete multibyte sequence.  */
	    abort ();
	  else
	    {
	      /* Successful conversion to UCS-4.  */
	      size_t inbytes = result;
	      src += inbytes;
	      if (uc == 0)
		{
		  result = totalcount;
		  break;
		}
	    }
	}
    }

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    gl_lock_unlock (facet->iconv_to_lock);

  return result;
}

#endif
