/* Convert a multi-byte character to a wide character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/stdlib.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"

int
gl_mbtowc (wchar_t *restrict pwc, const char *restrict s, size_t n,
	   gl_locale_t locale)
{
  if (s != NULL)
    {
      if (n > 0)
	{
	  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
	  iconv_t cd = facet->cd_to_utf8;
	  int retval;

	  if (!facet->is_utf8 && cd != (iconv_t)(-1))
	    {
	      gl_lock_lock (facet->iconv_to_lock);
	      iconv (cd, NULL, NULL, NULL, NULL);
	    }

	  {
	    unsigned int uc;
	    size_t result = facet->mbtouc (&uc, s, n, 1, cd);
	    if (result == (size_t)(-1))
	      /* Invalid multibyte sequence.  */
	      {
		errno = EILSEQ;
		retval = -1;
	      }
	    else if (result == (size_t)(-2))
	      /* Incomplete multibyte sequence.  */
	      {
		/* We must have n < GL_MB_LEN_MAX.  */
		if (n >= GL_MB_LEN_MAX)
		  abort ();
		retval = -1;
	      }
	    else
	      {
		/* Successful conversion to UCS-4.  */
		if (pwc != NULL)
		  *pwc = uc;
		retval = (uc == 0 ? 0 : result);
	      }
	  }

	  if (!facet->is_utf8 && cd != (iconv_t)(-1))
	    gl_lock_unlock (facet->iconv_to_lock);

	  return retval;
	}
      else
	/* n = 0.  */
	return -1;
    }
  else
    /* All locale encodings are stateless.  */
    return 0;
}

#endif
