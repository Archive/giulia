/* Convert a wide string to a multi-byte character string.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/stdlib.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <errno.h>
#include <stdlib.h>
#include "glocale-private.h"
#include "small.h"

size_t
gl_wcstombs (char *restrict dest, const wchar_t *restrict src, size_t len,
	     gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  iconv_t cd = facet->cd_from_utf8;
  size_t result;

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    {
      gl_lock_lock (facet->iconv_from_lock);
      iconv (cd, NULL, NULL, NULL, NULL);
    }

  if (dest != NULL)
    {
      char *destptr = dest;

      for (; ; src++)
	if (len > 0)
	  {
	    unsigned int uc = *src;
	    result = facet->uctomb (destptr, uc, len, cd);
	    if (result == (size_t)(-1))
	      /* Invalid wide character or conversion failure.  */
	      {
		errno = EILSEQ;
		result = (size_t)(-1);
		break;
	      }
	    else if (result == (size_t)(-2))
	      /* Output buffer too small.  */
	      {
		/* Retry with a larger output buffer.  */
		char buf[GL_MB_LEN_MAX];
		result = facet->uctomb (buf, uc, GL_MB_LEN_MAX, cd);
		if (result == (size_t)(-1))
		  {
		    errno = EILSEQ;
		    result = (size_t)(-1);
		    break;
		  }
		else if (result == (size_t)(-2))
		  abort ();
		else
		  {
		    if (result > GL_MB_LEN_MAX)
		      abort ();
		    if (result <= len)
		      {
			/* Strange, that the first call didn't succeed.
			   But anyway...  Successful conversion.  */
			memcpy_small (destptr, buf, result);
		      }
		    else
		      {
			/* Store the first part of the converted character
			   at destptr and drop the second part.
			   XXX The wcstombs() spec does not specify this.  */
			memcpy_small (destptr, buf, len);
			destptr += len;
			result = destptr - dest;
			break;
		      }
		  }
	      }
	    /* Successful conversion.  */
	    if (uc == 0)
	      {
		result = destptr - dest;
		break;
	      }
	    destptr += result;
	    len -= result;
	  }
	else
	  {
	    result = destptr - dest;
	    break;
	  }
    }
  else
    {
      /* Ignore dest and len.  */
      size_t totalcount = 0;

      for (; ; src++)
	{
	  char buf[GL_MB_LEN_MAX];
	  unsigned int uc = *src;
	  result = facet->uctomb (buf, uc, GL_MB_LEN_MAX, cd);
	  if (result == (size_t)(-1))
	    /* Invalid wide character or conversion failure.  */
	    {
	      errno = EILSEQ;
	      result = (size_t)(-1);
	      break;
	    }
	  else if (result == (size_t)(-2))
	    /* Output buffer too small.  */
	    abort ();
	  /* Successful conversion.  */
	  if (uc == 0)
	    {
	      result = totalcount;
	      break;
	    }
	  totalcount += result;
	}
    }

  if (!facet->is_utf8 && cd != (iconv_t)(-1))
    gl_lock_unlock (facet->iconv_from_lock);

  return result;

}

#endif
