/* Implementation of the dgettext(3) function.
   Copyright (C) 1995-1997, 2000-2003, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/libintl.h>

#include <locale.h>
#include "gettextP.h"

/* Look up MSGID in the DOMAINNAME message catalog of the current
   LC_MESSAGES locale.  */
const char *
gl_dgettext (const char *domainname,
	     const char *msgid,
	     gl_locale_t locale)
{
  return gl_dcgettext (domainname, msgid, LC_MESSAGES, locale);
}
