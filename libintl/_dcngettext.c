/* Implementation of the dcngettext(3) function.
   Copyright (C) 1995-1999, 2000-2003, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include "gettextP.h"

const char *
_gl_dcngettext (const char *domainname,
		const char *msgid1, const char *msgid2, unsigned long int n,
		int category,
		const char *localename, const char *encoding)
{
  return gl_dcigettext (domainname, msgid1, msgid2, 1, n, category,
			localename, encoding);
}
