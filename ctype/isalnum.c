/* Testing a character property.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/ctype.h>

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "glocale-private.h"

int
gl_isalnum (int c, gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  ctype_bitarray array = facet->array_ctype[CTYPE_ALNUM];

  if (array == NULL)
    array = _gl_init_ctype_array (facet, CTYPE_ALNUM, "alnum", locale);

  if (c >= 0 && c <= UCHAR_MAX)
    return (array[c >> 5] >> (c & 0x1f)) & 1;
  else
    {
      if (c != EOF)
	abort ();
      return 0;
    }
}
