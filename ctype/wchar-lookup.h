/* Copyright (C) 2000, 2003, 2005 Free Software Foundation, Inc.
   This file is part of the GNU C Library.
   Contributed by Bruno Haible <haible@clisp.cons.org>, 2000.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, write to the Free
   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* Tables indexed by a wide character are compressed through the use
   of a multi-level lookup.  The compression effect comes from blocks
   that don't need particular data and from blocks that can share their
   data.  */

/* Bit tables are accessed by cutting wc in four blocks of bits:
   - the high 32-q-p bits,
   - the next q bits,
   - the next p bits,
   - the next 5 bits.

	    +------------------+-----+-----+-----+
     wc  =  +     32-q-p-5     |  q  |  p  |  5  |
	    +------------------+-----+-----+-----+

   p and q are variable.  For 16-bit Unicode it is sufficient to
   choose p and q such that q+p+5 <= 16.

   The table contains the following uint32_t words:
   - q+p+5,
   - s = upper exclusive bound for wc >> (q+p+5),
   - p+5,
   - 2^q-1,
   - 2^p-1,
   - 1st-level table: s offsets, pointing into the 2nd-level table,
   - 2nd-level table: k*2^q offsets, pointing into the 3rd-level table,
   - 3rd-level table: j*2^p words, each containing 32 bits of data.
*/

static inline int
wctype_table_lookup (const char *table, unsigned int wc)
{
  unsigned int shift1 = ((const unsigned int *) table)[0];
  unsigned int index1 = wc >> shift1;
  unsigned int bound = ((const unsigned int *) table)[1];
  if (index1 < bound)
    {
      unsigned int lookup1 = ((const unsigned int *) table)[5 + index1];
      if (lookup1 != 0)
	{
	  unsigned int shift2 = ((const unsigned int *) table)[2];
	  unsigned int mask2 = ((const unsigned int *) table)[3];
	  unsigned int index2 = (wc >> shift2) & mask2;
	  unsigned int lookup2 = ((const unsigned int *)(table + lookup1))[index2];
	  if (lookup2 != 0)
	    {
	      unsigned int mask3 = ((const unsigned int *) table)[4];
	      unsigned int index3 = (wc >> 5) & mask3;
	      unsigned int lookup3 = ((const unsigned int *)(table + lookup2))[index3];

	      return (lookup3 >> (wc & 0x1f)) & 1;
	    }
	}
    }
  return 0;
}

/* Byte tables are similar to bit tables, except that the addressing
   unit is a single byte, and no 5 bits are used as a word index.  */

static inline int
wcwidth_table_lookup (const char *table, unsigned int wc)
{
  unsigned int shift1 = ((const unsigned int *) table)[0];
  unsigned int index1 = wc >> shift1;
  unsigned int bound = ((const unsigned int *) table)[1];
  if (index1 < bound)
    {
      unsigned int lookup1 = ((const unsigned int *) table)[5 + index1];
      if (lookup1 != 0)
	{
	  unsigned int shift2 = ((const unsigned int *) table)[2];
	  unsigned int mask2 = ((const unsigned int *) table)[3];
	  unsigned int index2 = (wc >> shift2) & mask2;
	  unsigned int lookup2 = ((const unsigned int *)(table + lookup1))[index2];
	  if (lookup2 != 0)
	    {
	      unsigned int mask3 = ((const unsigned int *) table)[4];
	      unsigned int index3 = wc & mask3;
	      signed char lookup3 = ((const signed char *)(table + lookup2))[index3];

	      return lookup3;
	    }
	}
    }
  return -1;
}

/* Mapping tables are similar to bit tables, except that the
   addressing unit is a single signed 16-bit word, containing the
   difference between the desired result and the argument, and no 5
   bits are used as a word index.  */

static inline unsigned int
wctrans_table_lookup (const char *table, unsigned int wc)
{
  unsigned int shift1 = ((const unsigned int *) table)[0];
  unsigned int index1 = wc >> shift1;
  unsigned int bound = ((const unsigned int *) table)[1];
  if (index1 < bound)
    {
      unsigned int lookup1 = ((const unsigned int *) table)[5 + index1];
      if (lookup1 != 0)
	{
	  unsigned int shift2 = ((const unsigned int *) table)[2];
	  unsigned int mask2 = ((const unsigned int *) table)[3];
	  unsigned int index2 = (wc >> shift2) & mask2;
	  unsigned int lookup2 = ((const unsigned int *)(table + lookup1))[index2];
	  if (lookup2 != 0)
	    {
	      unsigned int mask3 = ((const unsigned int *) table)[4];
	      unsigned int index3 = wc & mask3;
	      int lookup3 = ((const short *)(table + lookup2))[index3];

	      return wc + lookup3;
	    }
	}
    }
  return wc;
}
