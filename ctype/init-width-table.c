/* Initialization of a wide character width table.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <glocale/libintl.h>

#include "file-ctype.h"

/* Initializes facet->table_width, where facet belongs to locale.
   Must only be called with facet->data_lock being held.
   Returns the new value of facet->table_width.  */
static inline wcwidth_table
_gl_init_width_table_locked (struct gl_locale_ctype *facet, gl_locale_t locale)
{
  /* If another thread already initialized it, return it.  */
  {
    wcwidth_table table = facet->table_width;
    if (table != NULL)
      return table;
  }

  /* Use the mmaped file.  */
  {
    const struct file_ctype *file = _gl_init_file_ctype_locked (facet);
    if (file != (const struct file_ctype *)-1)
      {
	unsigned int final_offset = file->wcwidth_offset;
	return facet->table_width =
		 (wcwidth_table)((const char *)file + final_offset);
      }

    /* Unknown locale.  Fall back to the C locale.  */
    return facet->table_width = gl_locale_ctype_C.table_width;
  }
}

wcwidth_table
_gl_init_width_table (struct gl_locale_ctype *facet, gl_locale_t locale)
{
  wcwidth_table table;

  gl_lock_lock (facet->data_lock);
  table = _gl_init_width_table_locked (facet, locale);
  gl_lock_unlock (facet->data_lock);
  return table;
}
