/* Initialization of a narrow character mapping array.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "wchar-lookup.h"

/* Initializes facet->array_trans[index], where facet belongs to locale.
   Must only be called with facet->data_lock being held.
   Returns the new value of facet->array_trans[index].  */
static inline trans_array
_gl_init_trans_array_locked (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale)
{
  /* If another thread already initialized it, return it.  */
  {
    trans_array array = facet->array_trans[index];
    if (array != NULL)
      return array;
  }

  /* Compute the contents from facet->table_trans[index] and
     facet->encoding.  */
  {
    wctrans_table table =
      _gl_init_trans_table_locked (facet, index, name, locale);
    unsigned char *array =
      (unsigned char *) malloc ((UCHAR_MAX + 1) * sizeof (unsigned char));
    if (array == NULL)
      /* Out of memory.  Fall back to the C locale.  */
      return facet->array_trans[index] = gl_locale_ctype_C.array_trans[index];
    {
      unsigned int c;
      for (c = 0; c <= UCHAR_MAX; c++)
	{
	  unsigned char value;
	  /* Convert c to Unicode.  */
	  unsigned int uc = _gl_btowc (c, locale);
	  if (uc == (unsigned int) -1)
	    /* Ignore failed conversion.  */
	    value = c;
	  else
	    {
	      /* Successfully converted c to Unicode.  */
	      unsigned int tuc = wctrans_table_lookup (table, uc);
	      if (tuc == uc)
		{
		  /* Shortcut: no need to call _gl_wctob here.  */
		  value = c;
		}
	      else
		{
		  /* Convert tuc back to facet->encoding.  */
		  int tc = _gl_wctob (tuc, locale);
		  if (tc == EOF)
		    /* tuc exists only as a wide character, not as a single
		       'char'.  */
		    value = c;
		  else
		    /* tc is the mapping result of c.  */
		    value = tc;
		}
	    }
	  array[c] = value;
	}
    }
    facet->array_trans[index] = array;
    return array;
  }
}

trans_array
_gl_init_trans_array (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale)
{
  trans_array array;

  gl_lock_lock (facet->data_lock);
  array = _gl_init_trans_array_locked (facet, index, name, locale);
  gl_lock_unlock (facet->data_lock);
  return array;
}
