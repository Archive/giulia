/* Mapping a character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/ctype.h>

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include "glocale-private.h"

int
gl_tolower (int c, gl_locale_t locale)
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  trans_array array = facet->array_trans[TRANS_TOLOWER];

  if (array == NULL)
    array = _gl_init_trans_array (facet, TRANS_TOLOWER, "tolower", locale);

  if (c >= 0 && c <= UCHAR_MAX)
    return array[c];
  else
    {
      if (c != EOF)
	abort ();
      return c;
    }
}
