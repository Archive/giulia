/* Initialization of a narrow character property array.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <limits.h>
#include <stdlib.h>
#include "wchar-lookup.h"

/* Initializes facet->array_ctype[index], where facet belongs to locale.
   Must only be called with facet->data_lock being held.
   Returns the new value of facet->array_ctype[index].  */
static inline ctype_bitarray
_gl_init_ctype_array_locked (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale)
{
  /* If another thread already initialized it, return it.  */
  {
    ctype_bitarray array = facet->array_ctype[index];
    if (array != NULL)
      return array;
  }

  /* Compute the contents from facet->table_ctype[index] and
     facet->encoding.  */
  {
    wctype_table table =
      _gl_init_ctype_table_locked (facet, index, name, locale);
    unsigned int *array =
      (unsigned int *) calloc ((UCHAR_MAX >> 5) + 1, sizeof (unsigned int));
    if (array == NULL)
      /* Out of memory.  Fall back to the C locale.  */
      return facet->array_ctype[index] = gl_locale_ctype_C.array_ctype[index];
    {
      unsigned int c;
      for (c = 0; c <= UCHAR_MAX; c++)
	{
	  int value;
	  /* Convert c to Unicode.  */
	  unsigned int uc = _gl_btowc (c, locale);
	  if (uc == (unsigned int) -1)
	    /* Ignore failed conversion.  */
	    value = 0;
	  else
	    /* Successfully converted c to Unicode.  */
	    value = wctype_table_lookup (table, uc);
	  if (value)
	    array[c >> 5] |= ((unsigned int) 1 << (c & 0x1f));
	}
    }
    facet->array_ctype[index] = array;
    return array;
  }
}

ctype_bitarray
_gl_init_ctype_array (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale)
{
  ctype_bitarray array;

  gl_lock_lock (facet->data_lock);
  array = _gl_init_ctype_array_locked (facet, index, name, locale);
  gl_lock_unlock (facet->data_lock);
  return array;
}
