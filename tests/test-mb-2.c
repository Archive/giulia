/* Test of multibyte to wide-character conversion functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/stdlib.h>
#include <glocale/wchar.h>
#include <glocale/locale.h>
#include <errno.h>
#include <langinfo.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

static void
test_mblen (void)
{
  gl_locale_t locale = gl_newlocale ();

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  ASSERT (gl_mblen ("", 0, locale) == -1);

  ASSERT (gl_mblen ("", 1, locale) == 0);

  ASSERT (gl_mblen ("x", 1, locale) == 1);

  ASSERT (gl_mblen ("\234", 1, locale) == -1);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  ASSERT (gl_mblen ("", 0, locale) == -1);

  ASSERT (gl_mblen ("", 1, locale) == 0);

  ASSERT (gl_mblen ("x", 1, locale) == 1);

  ASSERT (gl_mblen ("\234", 1, locale) == -1);

  ASSERT (gl_mblen ("\277", 1, locale) == -1);

  ASSERT (gl_mblen ("\302", 1, locale) == -1);

  ASSERT (gl_mblen ("\304\226", 2, locale) == 2);

  ASSERT (gl_mblen ("\377", 1, locale) == -1);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  ASSERT (gl_mblen ("", 0, locale) == -1);

  ASSERT (gl_mblen ("", 1, locale) == 0);

  ASSERT (gl_mblen ("x", 1, locale) == 1);

  ASSERT (gl_mblen ("\214", 1, locale) == -1);

  ASSERT (gl_mblen ("\217", 1, locale) == -1);

  ASSERT (gl_mblen ("\217\242", 2, locale) == -1);

  ASSERT (gl_mblen ("\217\242\354", 3, locale) == 3);

  ASSERT (gl_mblen ("\360", 1, locale) == -1);

  ASSERT (gl_mblen ("\360\241", 2, locale) == 2);

  ASSERT (gl_mblen ("\377", 1, locale) == -1);
}

static void
test_mbrlen (void)
{
  gl_locale_t locale = gl_newlocale ();
  mbstate_t state;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 0, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 1, &state, locale) == 0);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("x", 1, &state, locale) == 1);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\234", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 0, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 1, &state, locale) == 0);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("x", 1, &state, locale) == 1);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\234", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\277", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\302", 1, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\304\226", 2, &state, locale) == 2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\377", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 0, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("", 1, &state, locale) == 0);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("x", 1, &state, locale) == 1);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\214", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\217", 1, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\217\242", 2, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\217\242\354", 3, &state, locale) == 3);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\360", 1, &state, locale) == (size_t)-2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\360\241", 2, &state, locale) == 2);

  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrlen ("\377", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);
}

#if GLOCALE_ENABLE_WIDE_CHAR

static void
test_mbtowc (void)
{
  gl_locale_t locale = gl_newlocale ();
  wchar_t wc;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 0, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 1, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "x", 1, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\234", 1, locale) == -1);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 0, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 1, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "x", 1, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\234", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\277", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\302", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\304\226", 2, locale) == 2 && wc == 0x0116);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\377", 1, locale) == -1);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 0, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "", 1, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "x", 1, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\214", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\217", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\217\242", 2, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\217\242\354", 3, locale) == 3 && wc == 0x00AA);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\360", 1, locale) == -1);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\360\241", 2, locale) == 2 && wc == 0x965D);

  wc = 0xDEADBEEF;
  ASSERT (gl_mbtowc (&wc, "\377", 1, locale) == -1);
}

static void
test_mbrtowc (void)
{
  gl_locale_t locale = gl_newlocale ();
  mbstate_t state;
  wchar_t wc;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 0, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 1, &state, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "x", 1, &state, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\234", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 0, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 1, &state, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "x", 1, &state, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\234", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\277", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\302", 1, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\304\226", 2, &state, locale) == 2 && wc == 0x0116);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\304", 1, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\226", 1, &state, locale) == 1 && wc == 0x0116);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\377", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  /* Note: glibc's mbrtowc returns 0 here, not (size_t)-2.  */
  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 0, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "", 1, &state, locale) == 0 && wc == 0);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "x", 1, &state, locale) == 1 && wc == 'x');

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\214", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217", 1, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217\242", 2, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217\242\354", 3, &state, locale) == 3 && wc == 0x00AA);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217\242", 2, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\354", 1, &state, locale) == 1 && wc == 0x00AA);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217", 1, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\242\354", 2, &state, locale) == 2 && wc == 0x00AA);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\217", 1, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\242", 1, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\354", 1, &state, locale) == 1 && wc == 0x00AA);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\360", 1, &state, locale) == (size_t)-2);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\360\241", 2, &state, locale) == 2 && wc == 0x965D);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\360", 1, &state, locale) == (size_t)-2);
  ASSERT (gl_mbrtowc (&wc, "\241", 1, &state, locale) == 1 && wc == 0x965D);

  wc = 0xDEADBEEF;
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_mbrtowc (&wc, "\377", 1, &state, locale) == (size_t)-1 && errno == EILSEQ);
}

static void
test_mbstowcs (void)
{
  gl_locale_t locale = gl_newlocale ();
  wchar_t buf[10];

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  ASSERT (gl_mbstowcs (NULL, "", 0, locale) == 0);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 1, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "Sue", 0, locale) == 3);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "Sue", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "Sue", 4, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x0075 && buf[2] == 0x0065
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "S\235", 0, locale) == -1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "S\235", 2, locale) == -1);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  ASSERT (gl_mbstowcs (NULL, "", 0, locale) == 0);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 1, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "Süß", 0, locale) == 3);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "Süß", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "Süß", 4, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x00FC && buf[2] == 0x00DF
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "S\235", 0, locale) == -1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "S\235", 2, locale) == -1);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  ASSERT (gl_mbstowcs (NULL, "", 0, locale) == 0);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "", 1, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "\306\374\313\334\270\354", 0, locale) == 3);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "\306\374\313\334\270\354", 0, locale) == 0 && buf[0] == 0xDEADBEEF);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "\306\374\313\334\270\354", 4, locale) == 3
	  && buf[0] == 0x65E5 && buf[1] == 0x672C && buf[2] == 0x8A9E
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF);

  ASSERT (gl_mbstowcs (NULL, "S\235", 0, locale) == -1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  ASSERT (gl_mbstowcs (buf, "S\235", 2, locale) == -1);
}

static void
test_mbsrtowcs (void)
{
  gl_locale_t locale = gl_newlocale ();
  mbstate_t state;
  wchar_t buf[10];
  const char *src;
  const char *srcorig;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsrtowcs (buf, &src, 4, &state, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x0075 && buf[2] == 0x0065
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (buf, &src, 2, &state, locale) == -1
	  && src == srcorig + 1);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsrtowcs (buf, &src, 4, &state, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x00FC && buf[2] == 0x00DF
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (buf, &src, 2, &state, locale) == -1
	  && src == srcorig + 1);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsrtowcs (buf, &src, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsrtowcs (buf, &src, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsrtowcs (buf, &src, 4, &state, locale) == 3
	  && buf[0] == 0x65E5 && buf[1] == 0x672C && buf[2] == 0x8A9E
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (NULL, &src, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsrtowcs (buf, &src, 2, &state, locale) == -1
	  && src == srcorig + 1);
}

static void
test_mbsnrtowcs (void)
{
  gl_locale_t locale = gl_newlocale ();
  mbstate_t state;
  wchar_t buf[10];
  const char *src;
  const char *srcorig;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Sue";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 4, &state, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x0075 && buf[2] == 0x0065
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 2, &state, locale) == -1
	  && src == srcorig + 1);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 4, &state, locale) == 3
	  && buf[0] == 0x0053 && buf[1] == 0x00FC && buf[2] == 0x00DF
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "Süß";
  ASSERT (gl_mbsnrtowcs (buf, &src, 4, 4, &state, locale) == 2
	  && buf[0] == 0x0053 && buf[1] == 0x00FC && buf[2] == 0xDEADBEEF
	  && src == srcorig + 4);
  ASSERT (gl_mbsnrtowcs (buf + 2, &src, 2, 2, &state, locale) == 1
	  && buf[2] == 0x00DF && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 2, &state, locale) == -1
	  && src == srcorig + 1);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 0
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 1, &state, locale) == 0 && buf[0] == 0 && buf[1] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == 3
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 0, &state, locale) == 0 && buf[0] == 0xDEADBEEF
	  && src == srcorig);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 4, &state, locale) == 3
	  && buf[0] == 0x65E5 && buf[1] == 0x672C && buf[2] == 0x8A9E
	  && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "\306\374\313\334\270\354";
  ASSERT (gl_mbsnrtowcs (buf, &src, 3, 4, &state, locale) == 1
	  && buf[0] == 0x65E5 && buf[1] == 0xDEADBEEF
	  && src == srcorig + 3);
  ASSERT (gl_mbsnrtowcs (buf + 1, &src, 4, 3, &state, locale) == 2
	  && buf[1] == 0x672C && buf[2] == 0x8A9E && buf[3] == 0 && buf[4] == 0xDEADBEEF
	  && src == NULL);

  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (NULL, &src, strlen (src) + 1, 0, &state, locale) == -1
	  && src == srcorig + 1);

  wmemset (buf, 0xDEADBEEF, sizeof (buf) / sizeof (buf[0]));
  memset (&state, 0, sizeof (mbstate_t));
  src = srcorig = "S\235";
  ASSERT (gl_mbsnrtowcs (buf, &src, strlen (src) + 1, 2, &state, locale) == -1
	  && src == srcorig + 1);
}

#endif

int
main ()
{
  test_mblen ();
  test_mbrlen ();
#if GLOCALE_ENABLE_WIDE_CHAR
  test_mbtowc ();
  test_mbrtowc ();
  test_mbstowcs ();
  test_mbsrtowcs ();
  test_mbsnrtowcs ();
#endif

  return 0;
}
