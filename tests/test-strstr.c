/* Test of substring search functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/string.h>
#include <stdlib.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;

  /* Test the gl_strstr function.  */

  locale = gl_newlocale ();

  {
    const char *haystack = "";
    ASSERT (gl_strstr (haystack, "", locale) == haystack);
    ASSERT (gl_strstr (haystack, "a", locale) == NULL);
  }

  {
    const char *haystack = "blaa";
    ASSERT (gl_strstr (haystack, "", locale) == haystack);
    ASSERT (gl_strstr (haystack, "a", locale) == haystack + 2);
  }

  {
    const char *haystack = "ababababaaababbaaabbaba";
    ASSERT (gl_strstr (haystack, "aabb", locale) == haystack + 16);
    ASSERT (gl_strstr (haystack, "aabaa", locale) == NULL);
  }

  gl_setlocale (locale, LC_ALL, "de_DE");

  {
    const char *haystack = "";
    ASSERT (gl_strstr (haystack, "", locale) == haystack);
    ASSERT (gl_strstr (haystack, "a", locale) == NULL);
  }

  {
    const char *haystack = "blaa";
    ASSERT (gl_strstr (haystack, "", locale) == haystack);
    ASSERT (gl_strstr (haystack, "a", locale) == haystack + 2);
  }

  {
    const char *haystack = "ababababaaababbaaabbaba";
    ASSERT (gl_strstr (haystack, "aabb", locale) == haystack + 16);
    ASSERT (gl_strstr (haystack, "aabaa", locale) == NULL);
  }

  {
    const char *haystack = "\303\244\303\237\303\244\303\237\303\244\303\237\303\244\303\237\303\244\303\244\303\244\303\237\303\244\303\237\303\237\303\244\303\244\303\244\303\237\303\237\303\244\303\237\303\244";
    const char *needle = "\303\244\303\244\303\237\303\237";
    ASSERT (gl_strstr (haystack, needle, locale) == haystack + 32);
  }

  {
    const char *haystack = "\304\304\305\305\304\305";
    const char *needle = "\304\305";

    gl_setlocale (locale, LC_ALL, "de_DE.ISO-8859-1");

    ASSERT (gl_strstr (haystack, needle, locale) == haystack + 1);

    gl_setlocale (locale, LC_ALL, "zh_TW.BIG5");

    ASSERT (gl_strstr (haystack, needle, locale) == haystack + 4);
  }

  /* Test the gl_strcasestr function.  */

  locale = gl_newlocale ();

  {
    const char *haystack = "";
    ASSERT (gl_strcasestr (haystack, "", locale) == haystack);
    ASSERT (gl_strcasestr (haystack, "a", locale) == NULL);
  }

  {
    const char *haystack = "blaa";
    ASSERT (gl_strcasestr (haystack, "", locale) == haystack);
    ASSERT (gl_strcasestr (haystack, "a", locale) == haystack + 2);
  }

  {
    const char *haystack = "habit";
    ASSERT (gl_strcasestr (haystack, "ABI", locale) == haystack + 1);
  }

  ASSERT (gl_strcasestr ("Boogie", "boo", locale) != NULL);
  ASSERT (gl_strcasestr ("boo", "Boogie", locale) == NULL);

  gl_setlocale (locale, LC_ALL, "de_DE");

  {
    const char *haystack = "";
    ASSERT (gl_strcasestr (haystack, "", locale) == haystack);
    ASSERT (gl_strcasestr (haystack, "a", locale) == NULL);
  }

  {
    const char *haystack = "habit";
    ASSERT (gl_strcasestr (haystack, "ABI", locale) == haystack + 1);
  }

  ASSERT (gl_strcasestr ("Boogie", "boo", locale) != NULL);
  ASSERT (gl_strcasestr ("boo", "Boogie", locale) == NULL);

  /* In the turkish locale, there is no correspondence between 'i' and 'I',
     but rather between 'i' and 'İ' and between 'ı' and 'I'.  */

  gl_setlocale (locale, LC_ALL, "tr_TR");

  {
    const char *haystack = "";
    ASSERT (gl_strcasestr (haystack, "", locale) == haystack);
    ASSERT (gl_strcasestr (haystack, "a", locale) == NULL);
  }

  {
    const char *haystack = "blaa";
    ASSERT (gl_strcasestr (haystack, "", locale) == haystack);
    ASSERT (gl_strcasestr (haystack, "a", locale) == haystack + 2);
  }

  {
    const char *haystack = "habit";
    ASSERT (gl_strcasestr (haystack, "ABI", locale) == NULL);
  }

  {
    const char *haystack = "habit";
    ASSERT (gl_strcasestr (haystack, "AB\304\260", locale) == haystack + 1);
  }

  {
    const char *haystack = "hab\304\261t";
    ASSERT (gl_strcasestr (haystack, "ABI", locale) == haystack + 1);
  }

  return 0;
}
