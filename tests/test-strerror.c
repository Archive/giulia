/* Test of error string functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/string.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;
  char buf[1024];

  /* Test gl_strerror function.  */

  locale = gl_newlocale ();

  ASSERT (strcmp (gl_strerror (ERANGE, locale),
		  "Numerical result out of range") == 0);

  gl_setlocale (locale, LC_ALL, "de_DE");

  ASSERT (strcmp (gl_strerror (ERANGE, locale),
		  "Das numerische Ergebnis ist au\303\237erhalb des g\303\274ltigen Bereiches")
	  == 0);

  gl_setlocale (locale, LC_ALL, "de_DE.ISO-8859-1");

  ASSERT (strcmp (gl_strerror (ERANGE, locale),
		  "Das numerische Ergebnis ist au\337erhalb des g\374ltigen Bereiches")
	  == 0);

  /* Test gl_strerror_r function.  */

  locale = gl_newlocale ();

  ASSERT (gl_strerror_r (1729, buf, sizeof (buf), locale) == 0);
#if !HAVE_STRERROR_R
  ASSERT (gl_strerror_r (1729, buf, sizeof (buf), locale) == 0
	  && strcmp (buf, "<errno=1729>") == 0);
  ASSERT (gl_strerror_r (1729, buf, 13, locale) == 0
	  && strcmp (buf, "<errno=1729>") == 0);
  ASSERT (gl_strerror_r (1729, buf, 12, locale) == ERANGE);
  ASSERT (gl_strerror_r (1729, buf, 0, locale) == ERANGE);
#endif

  ASSERT (gl_strerror_r (ERANGE, buf, sizeof (buf), locale) == 0
	  && strcmp (buf, "Numerical result out of range") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 30, locale) == 0
	  && strcmp (buf, "Numerical result out of range") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 29, locale) == ERANGE);

  gl_setlocale (locale, LC_ALL, "de_DE");

  ASSERT (gl_strerror_r (ERANGE, buf, sizeof (buf), locale) == 0
	  && strcmp (buf, "Das numerische Ergebnis ist au\303\237erhalb des g\303\274ltigen Bereiches") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 63, locale) == 0
	  && strcmp (buf, "Das numerische Ergebnis ist au\303\237erhalb des g\303\274ltigen Bereiches") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 62, locale) == ERANGE);

  gl_setlocale (locale, LC_ALL, "de_DE.ISO-8859-1");

  ASSERT (gl_strerror_r (ERANGE, buf, sizeof (buf), locale) == 0
	  && strcmp (buf, "Das numerische Ergebnis ist au\337erhalb des g\374ltigen Bereiches") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 61, locale) == 0
	  && strcmp (buf, "Das numerische Ergebnis ist au\337erhalb des g\374ltigen Bereiches") == 0);
  ASSERT (gl_strerror_r (ERANGE, buf, 60, locale) == ERANGE);

  return 0;
}
