/* Test of locale creation/manipulation functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/locale.h>
#include <langinfo.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

/* Test the properties of a fresh locale.  */
static void
test_newlocale_1 (void)
{
  gl_locale_t locale = gl_newlocale ();
  ASSERT (strcmp (gl_setlocale (locale, LC_ADDRESS, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_COLLATE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_CTYPE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_IDENTIFICATION, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MEASUREMENT, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MESSAGES, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MONETARY, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NAME, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NUMERIC, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_PAPER, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TELEPHONE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TIME, NULL), "C") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "ASCII") == 0);
}

/* Test that two fresh locales can be manipulated independently.  */
static void
test_newlocale_2 (void)
{
  gl_locale_t locale1;
  gl_locale_t locale2;

  locale1 = gl_newlocale ();
  gl_setlocale (locale1, LC_NUMERIC, "de_DE");

  locale2 = gl_newlocale ();
  gl_setlocale (locale2, LC_TIME, "it_IT");

  gl_setlocale (locale1, LC_TIME, "de_CH");

  ASSERT (strcmp (gl_setlocale (locale1, LC_NUMERIC, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_TIME, NULL), "de_CH") == 0);
  ASSERT (strcmp (gl_setlocale (locale2, LC_NUMERIC, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale2, LC_TIME, NULL), "it_IT") == 0);
}

/* Test that setlocale modifies a single facet of a locale.  */
static void
test_setlocale_1 (void)
{
  gl_locale_t locale = gl_newlocale ();
  gl_setlocale (locale, LC_MESSAGES, "de_DE");
  ASSERT (strcmp (gl_setlocale (locale, LC_ADDRESS, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_COLLATE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_CTYPE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_IDENTIFICATION, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MEASUREMENT, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MESSAGES, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MONETARY, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NAME, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NUMERIC, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_PAPER, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TELEPHONE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TIME, NULL), "C") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "ASCII") == 0);
}

/* Test that setlocale with LC_ALL modifies all facets of a locale.  */
static void
test_setlocale_2 (void)
{
  gl_locale_t locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, "de_DE");
  ASSERT (strcmp (gl_setlocale (locale, LC_ADDRESS, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_COLLATE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_CTYPE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_IDENTIFICATION, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MEASUREMENT, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MESSAGES, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MONETARY, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NAME, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NUMERIC, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_PAPER, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TELEPHONE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TIME, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "UTF-8") == 0);
}

/* Test that duplocale creates a locale that can be manipulated
   independently.  */
static void
test_duplocale_1 (void)
{
  gl_locale_t locale1;
  gl_locale_t locale2;

  locale1 = gl_newlocale ();
  gl_setlocale (locale1, LC_ALL, "de_DE");

  locale2 = gl_duplocale (locale1);
  gl_setlocale (locale2, LC_TIME, "it_IT");

  gl_setlocale (locale1, LC_TIME, "de_CH");

  ASSERT (strcmp (gl_setlocale (locale1, LC_NUMERIC, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_TIME, NULL), "de_CH") == 0);
  ASSERT (strcmp (gl_setlocale (locale2, LC_NUMERIC, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale2, LC_TIME, NULL), "it_IT") == 0);
}

/* Test that freelocale doesn't impact locales that were created earlier
   or duplicated.  */
static void
test_freelocale_1 (void)
{
  gl_locale_t locale1;
  gl_locale_t locale2;
  gl_locale_t locale3;

  locale1 = gl_newlocale ();
  gl_setlocale (locale1, LC_ALL, "fr_FR");

  locale2 = gl_newlocale ();
  gl_setlocale (locale2, LC_ALL, "de_DE");

  locale3 = gl_duplocale (locale2);
  gl_setlocale (locale3, LC_TIME, "it_IT");

  gl_freelocale (locale2);

  ASSERT (strcmp (gl_setlocale (locale1, LC_ADDRESS, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_COLLATE, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_CTYPE, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_IDENTIFICATION, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_MEASUREMENT, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_MESSAGES, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_MONETARY, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_NAME, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_NUMERIC, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_PAPER, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_TELEPHONE, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_setlocale (locale1, LC_TIME, NULL), "fr_FR") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale1), "UTF-8") == 0);

  ASSERT (strcmp (gl_setlocale (locale3, LC_ADDRESS, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_COLLATE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_CTYPE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_IDENTIFICATION, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_MEASUREMENT, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_MESSAGES, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_MONETARY, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_NAME, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_NUMERIC, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_PAPER, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_TELEPHONE, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale3, LC_TIME, NULL), "it_IT") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale3), "UTF-8") == 0);
}

/* Test setencoding.  */
static void
test_setencoding_1 (void)
{
  gl_locale_t locale = gl_newlocale ();
  gl_setlocale (locale, LC_MESSAGES, "de_DE");
  gl_setencoding (locale, "ISO-8859-15");
  ASSERT (strcmp (gl_setlocale (locale, LC_ADDRESS, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_COLLATE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_CTYPE, NULL), "C.ISO-8859-15") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_IDENTIFICATION, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MEASUREMENT, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MESSAGES, NULL), "de_DE") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_MONETARY, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NAME, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_NUMERIC, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_PAPER, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TELEPHONE, NULL), "C") == 0);
  ASSERT (strcmp (gl_setlocale (locale, LC_TIME, NULL), "C") == 0);
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "ISO-8859-15") == 0);
}

int
main ()
{
  test_newlocale_1 ();
  test_newlocale_2 ();
  test_setlocale_1 ();
  test_setlocale_2 ();
  test_duplocale_1 ();
  test_freelocale_1 ();
  test_setencoding_1 ();

  return 0;
}
