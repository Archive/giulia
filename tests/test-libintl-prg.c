/* Test of libintl (d*gettext) functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/libintl.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

static void
test_dgettext (void)
{
  gl_locale_t locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, "fr_FR");
  gl_setencoding (locale, "UTF-8");
  ASSERT (strcmp (gl_dgettext ("test",
			       "'Your command, please?', asked the waiter.",
			       locale),
		  "«Votre commande, s'il vous plait», dit le garçon.")
	  == 0);
  gl_setencoding (locale, "ISO-8859-15");
  ASSERT (strcmp (gl_dgettext ("test",
			       "'Your command, please?', asked the waiter.",
			       locale),
		  "\253Votre commande, s'il vous plait\273, dit le gar\347on.")
	  == 0);
}

static void
test_dngettext (void)
{
  gl_locale_t locale;
  unsigned long n;

  locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, "fr_FR");
  gl_setencoding (locale, "UTF-8");
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dngettext("test",
				 "Ask the waiter for a piece of cake.",
				 "Ask the waiter for %d pieces of cake.",
				 n,
				 locale),
		    "Demande un morceau de gateau au garçon.")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dngettext("test",
				 "Ask the waiter for a piece of cake.",
				 "Ask the waiter for %d pieces of cake.",
				 n,
				 locale),
		    "Demande %d morceaux de gateau au garçon.")
	    == 0);
  gl_setencoding (locale, "ISO-8859-15");
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dngettext("test",
				 "Ask the waiter for a piece of cake.",
				 "Ask the waiter for %d pieces of cake.",
				 n,
				 locale),
		    "Demande un morceau de gateau au gar\347on.")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dngettext("test",
				 "Ask the waiter for a piece of cake.",
				 "Ask the waiter for %d pieces of cake.",
				 n,
				 locale),
		    "Demande %d morceaux de gateau au gar\347on.")
	    == 0);
}

static void
test_dcgettext (void)
{
  gl_locale_t locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, "fr_FR");
  gl_setencoding (locale, "UTF-8");
  ASSERT (strcmp (gl_dcgettext ("test",
				"after %d'",
				LC_MESSAGES,
				locale),
		  "après %d minute")
	  == 0);
  ASSERT (strcmp (gl_dcgettext ("test",
				"after %d'",
				LC_TIME,
				locale),
		  "après %d min.")
	  == 0);
  gl_setencoding (locale, "ISO-8859-15");
  ASSERT (strcmp (gl_dcgettext ("test",
				"after %d'",
				LC_MESSAGES,
				locale),
		  "apr\350s %d minute")
	  == 0);
  ASSERT (strcmp (gl_dcgettext ("test",
				"after %d'",
				LC_TIME,
				locale),
		  "apr\350s %d min.")
	  == 0);
}

static void
test_dcngettext (void)
{
  gl_locale_t locale;
  unsigned long n;

  locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, "fr_FR");
  gl_setencoding (locale, "UTF-8");
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_MESSAGES,
				  locale),
		    "après %d minute")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_MESSAGES,
				  locale),
		    "après %d minutes")
	    == 0);
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_TIME,
				  locale),
		    "après %d min.")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_TIME,
				  locale),
		    "après %d min.s")
	    == 0);
  gl_setencoding (locale, "ISO-8859-15");
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_MESSAGES,
				  locale),
		    "apr\350s %d minute")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_MESSAGES,
				  locale),
		    "apr\350s %d minutes")
	    == 0);
  for (n = 0; n < 2; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_TIME,
				  locale),
		    "apr\350s %d min.")
	    == 0);
  for (n = 2; n < 5; n++)
    ASSERT (strcmp (gl_dcngettext("test",
				  "after %d'",
				  "after %d'",
				  n,
				  LC_TIME,
				  locale),
		    "apr\350s %d min.s")
	    == 0);
}

int
main ()
{
  bindtextdomain ("test", ".");
  test_dgettext ();
  test_dngettext ();
  test_dcgettext ();
  test_dcngettext ();

  return 0;
}
