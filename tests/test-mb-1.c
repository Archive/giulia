/* Test of elementary multibyte functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/stdlib.h>
#include <glocale/wchar.h>
#include <glocale/locale.h>
#include <langinfo.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

static void
test_encoding (const char *encoding, const char *localename,
	       size_t cur_max, unsigned int unibyte_limit)
{
  gl_locale_t locale;
  unsigned int c;

  locale = gl_newlocale ();
  gl_setlocale (locale, LC_ALL, localename);

  /* Verify the encoding.  */
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), encoding) == 0);

  /* Verify gl_mb_cur_max.  */
  ASSERT (gl_mb_cur_max (locale) == cur_max);

  /* Verify gl_btowc.  */
  ASSERT (gl_btowc (EOF, locale) == WEOF);
  for (c = 0; c < unibyte_limit; c++)
    if (c < 0x80)
      ASSERT (gl_btowc (c, locale) == c);
    else
      ASSERT (gl_btowc (c, locale) != WEOF);

  /* Verify gl_wctob.  */
  ASSERT (gl_wctob (WEOF, locale) == EOF);
  for (c = 0; c < 0x80; c++)
    ASSERT (gl_wctob (c, locale) == c);
}

int
main ()
{
  test_encoding ("ASCII",       "C",                 1, 128);
  test_encoding ("ISO-8859-1",  "de_DE.ISO-8859-1",  1, 256);
  test_encoding ("ISO-8859-2",  "pl_PL.ISO-8859-2",  1, 256);
  test_encoding ("ISO-8859-3",  "mt_MT.ISO-8859-3",  1, 165);
  test_encoding ("ISO-8859-5",  "mk_MK.ISO-8859-5",  1, 256);
  test_encoding ("ISO-8859-6",  "ar_SA.ISO-8859-6",  1, 161);
  test_encoding ("ISO-8859-7",  "el_GR.ISO-8859-7",  1, 174);
  test_encoding ("ISO-8859-8",  "he_IL.ISO-8859-8",  1, 161);
  test_encoding ("ISO-8859-9",  "tr_TR.ISO-8859-9",  1, 256);
  test_encoding ("ISO-8859-10", "lg_UG.ISO-8859-10", 1, 256);
  test_encoding ("ISO-8859-13", "lv_LV.ISO-8859-13", 1, 256);
  test_encoding ("ISO-8859-14", "cy_GB.ISO-8859-14", 1, 256);
  test_encoding ("ISO-8859-15", "et_EE.ISO-8859-15", 1, 256);
  test_encoding ("KOI8-R",      "ru_RU.KOI8-R",      1, 256);
  test_encoding ("KOI8-U",      "uk_UA.KOI8-U",      1, 256);
  test_encoding ("KOI8-T",      "tg_TJ.KOI8-T",      1, 136);
  test_encoding ("CP1251",      "bg_BG.CP1251",      1, 152);
  test_encoding ("CP1255",      "yi_US.CP1255",      1, 129);
  test_encoding ("GB2312",      "zh_CN.GB2312",      2, 128);
  test_encoding ("EUC-JP",      "ja_JP.EUC-JP",      3, 128);
  test_encoding ("EUC-KR",      "ko_KR.EUC-KR",      2, 128);
  test_encoding ("EUC-TW",      "zh_TW.EUC-TW",      4, 128);
  test_encoding ("BIG5",        "zh_TW.BIG5",        2, 128);
  test_encoding ("BIG5-HKSCS",  "zh_SG.BIG5-HKSCS",  2, 128);
  test_encoding ("GBK",         "zh_CN.GBK",         2, 128);
  test_encoding ("GB18030",     "zh_CN.GB18030",     4, 128);
#if 0 /* JOHAB is not ASCII compatible -> locale is not ISO C compliant */
  test_encoding ("JOHAB",       "ko_KR.JOHAB",       2, 128);
#endif
  test_encoding ("TIS-620",     "th_TH.TIS-620",     1, 128);
#if 0 /* VISCII is not ASCII compatible -> locale is not ISO C compliant */
  test_encoding ("VISCII",      "vi_VN.VISCII",      1, 256);
#endif
#if 0 /* TCVN5712-1 is not ASCII compatible -> locale is not ISO C compliant */
  test_encoding ("TCVN5712-1",  "vi_VN.TCVN5712-1",  1, 256);
#endif
  test_encoding ("GEORGIAN-PS", "ka_GE.GEORGIAN-PS", 1, 256);
  test_encoding ("UTF-8",       "de_DE.UTF-8",       4, 128);

  return 0;
}
