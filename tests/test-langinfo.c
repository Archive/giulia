/* Test of locale-dependent strings.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/langinfo.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;

  locale = gl_newlocale ();

  /* Test C locale.  */

  /* LC_CTYPE items.  */
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "ASCII") == 0);

  /* LC_MESSAGES items.  */
  ASSERT (strcmp (gl_nl_langinfo (YESEXPR, locale), "^[yY]") == 0);
  ASSERT (strcmp (gl_nl_langinfo (NOEXPR, locale), "^[nN]") == 0);
#ifdef YESSTR
  ASSERT (strcmp (gl_nl_langinfo (YESSTR, locale), "") == 0);
#endif
#ifdef NOSTR
  ASSERT (strcmp (gl_nl_langinfo (NOSTR, locale), "") == 0);
#endif

  /* Test tr_TR locale.  */

  gl_setlocale (locale, LC_ALL, "tr_TR");

  /* LC_CTYPE items.  */
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "UTF-8") == 0);

  /* LC_MESSAGES items.  */
  ASSERT (strcmp (gl_nl_langinfo (YESEXPR, locale), "^[yYeE]") == 0);
  ASSERT (strcmp (gl_nl_langinfo (NOEXPR, locale), "^[nNhH]") == 0);
#ifdef YESSTR
  ASSERT (strcmp (gl_nl_langinfo (YESSTR, locale), "evet") == 0);
#endif
#ifdef NOSTR
  ASSERT (strcmp (gl_nl_langinfo (NOSTR, locale), "hay\304\261r") == 0);
#endif

  /* Test tr_TR.ISO-8859-9 locale.  */

  gl_setencoding (locale, "ISO-8859-9");

  /* LC_CTYPE items.  */
  ASSERT (strcmp (gl_nl_langinfo (CODESET, locale), "ISO-8859-9") == 0);

  /* LC_MESSAGES items.  */
  ASSERT (strcmp (gl_nl_langinfo (YESEXPR, locale), "^[yYeE]") == 0);
  ASSERT (strcmp (gl_nl_langinfo (NOEXPR, locale), "^[nNhH]") == 0);
#ifdef YESSTR
  ASSERT (strcmp (gl_nl_langinfo (YESSTR, locale), "evet") == 0);
#endif
#ifdef NOSTR
  ASSERT (strcmp (gl_nl_langinfo (NOSTR, locale), "hay\375r") == 0);
#endif

  return 0;
}
