/* Test of character classification functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/ctype.h>
#include <glocale/wctype.h>
#include <stdlib.h>

#include "c-ctype.h"

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;
  unsigned int c;

  locale = gl_newlocale ();

  /* The default locale has only ASCII characters.  */

#if GLOCALE_ENABLE_WIDE_CHAR
  for (c = 0; c < 0x10000; c++)
    {
      ASSERT (gl_iswalnum (c, locale) == (c < 0x80 && c_isalnum (c)));
      ASSERT (gl_iswalpha (c, locale) == (c < 0x80 && c_isalpha (c)));
      ASSERT (gl_iswcntrl (c, locale) == (c < 0x80 && c_iscntrl (c)));
      ASSERT (gl_iswdigit (c, locale) == (c < 0x80 && c_isdigit (c)));
      ASSERT (gl_iswgraph (c, locale) == (c < 0x80 && c_isgraph (c)));
      ASSERT (gl_iswlower (c, locale) == (c < 0x80 && c_islower (c)));
      ASSERT (gl_iswprint (c, locale) == (c < 0x80 && c_isprint (c)));
      ASSERT (gl_iswpunct (c, locale) == (c < 0x80 && c_ispunct (c)));
      ASSERT (gl_iswspace (c, locale) == (c < 0x80 && c_isspace (c)));
      ASSERT (gl_iswupper (c, locale) == (c < 0x80 && c_isupper (c)));
      ASSERT (gl_iswxdigit (c, locale) == (c < 0x80 && c_isxdigit (c)));
      ASSERT (gl_iswblank (c, locale) == (c < 0x80 && c_isblank (c)));
    }
#endif

  for (c = 0; c < 0x100; c++)
    {
      ASSERT (gl_isalnum (c, locale) == c_isalnum (c));
      ASSERT (gl_isalpha (c, locale) == c_isalpha (c));
      ASSERT (gl_iscntrl (c, locale) == c_iscntrl (c));
      ASSERT (gl_isdigit (c, locale) == c_isdigit (c));
      ASSERT (gl_isgraph (c, locale) == c_isgraph (c));
      ASSERT (gl_islower (c, locale) == c_islower (c));
      ASSERT (gl_isprint (c, locale) == c_isprint (c));
      ASSERT (gl_ispunct (c, locale) == c_ispunct (c));
      ASSERT (gl_isspace (c, locale) == c_isspace (c));
      ASSERT (gl_isupper (c, locale) == c_isupper (c));
      ASSERT (gl_isxdigit (c, locale) == c_isxdigit (c));
      ASSERT (gl_isblank (c, locale) == c_isblank (c));
    }

  /* Other locales have all sorts of Unicode characters.  */

  gl_setlocale (locale, LC_ALL, "de_DE");

#if GLOCALE_ENABLE_WIDE_CHAR
  for (c = 0; c < 0x80; c++)
    {
      ASSERT (gl_iswalnum (c, locale) == c_isalnum (c));
      ASSERT (gl_iswalpha (c, locale) == c_isalpha (c));
      ASSERT (gl_iswcntrl (c, locale) == c_iscntrl (c));
      ASSERT (gl_iswdigit (c, locale) == c_isdigit (c));
      ASSERT (gl_iswgraph (c, locale) == c_isgraph (c));
      ASSERT (gl_iswlower (c, locale) == c_islower (c));
      ASSERT (gl_iswprint (c, locale) == c_isprint (c));
      ASSERT (gl_iswpunct (c, locale) == c_ispunct (c));
      ASSERT (gl_iswspace (c, locale) == c_isspace (c));
      ASSERT (gl_iswupper (c, locale) == c_isupper (c));
      ASSERT (gl_iswxdigit (c, locale) == c_isxdigit (c));
      ASSERT (gl_iswblank (c, locale) == c_isblank (c));
    }
#endif

  for (c = 0; c < 0x80; c++)
    {
      ASSERT (gl_isalnum (c, locale) == c_isalnum (c));
      ASSERT (gl_isalpha (c, locale) == c_isalpha (c));
      ASSERT (gl_iscntrl (c, locale) == c_iscntrl (c));
      ASSERT (gl_isdigit (c, locale) == c_isdigit (c));
      ASSERT (gl_isgraph (c, locale) == c_isgraph (c));
      ASSERT (gl_islower (c, locale) == c_islower (c));
      ASSERT (gl_isprint (c, locale) == c_isprint (c));
      ASSERT (gl_ispunct (c, locale) == c_ispunct (c));
      ASSERT (gl_isspace (c, locale) == c_isspace (c));
      ASSERT (gl_isupper (c, locale) == c_isupper (c));
      ASSERT (gl_isxdigit (c, locale) == c_isxdigit (c));
      ASSERT (gl_isblank (c, locale) == c_isblank (c));
    }

#if GLOCALE_ENABLE_WIDE_CHAR

  for (c = 0x0100; c < 0x0138; c++)
    ASSERT (gl_iswalnum (c, locale));
  for (c = 0x2500; c < 0x2600; c++)
    ASSERT (!gl_iswalnum (c, locale));

  for (c = 0x0100; c < 0x0138; c++)
    ASSERT (gl_iswalpha (c, locale));
  for (c = 0x2500; c < 0x2600; c++)
    ASSERT (!gl_iswalpha (c, locale));

  for (c = 0x0080; c < 0x00a0; c++)
    ASSERT (gl_iswcntrl (c, locale));
  for (c = 0x00a0; c < 0x2000; c++)
    ASSERT (!gl_iswcntrl (c, locale));

  for (c = 0x0080; c < 0x10000; c++)
    ASSERT (!gl_iswdigit (c, locale));

  for (c = 0x2500; c < 0x2600; c++)
    ASSERT (gl_iswgraph (c, locale));
  for (c = 0xfffe; c < 0x10000; c++)
    ASSERT (!gl_iswgraph (c, locale));

  for (c = 0x0101; c < 0x0138; c += 2)
    ASSERT (gl_iswlower (c, locale));
  for (c = 0x0100; c < 0x0138; c += 2)
    ASSERT (!gl_iswlower (c, locale));

  for (c = 0x2500; c < 0x2600; c++)
    ASSERT (gl_iswprint (c, locale));
  for (c = 0xfffe; c < 0x10000; c++)
    ASSERT (!gl_iswprint (c, locale));

  for (c = 0x2018; c < 0x2020; c++)
    ASSERT (gl_iswpunct (c, locale));
  for (c = 0x0100; c < 0x0138; c++)
    ASSERT (!gl_iswpunct (c, locale));

  for (c = 0x2000; c < 0x2007; c++)
    ASSERT (gl_iswspace (c, locale));
  for (c = 0x00a0; c < 0x00a1; c++)
    ASSERT (!gl_iswspace (c, locale));

  for (c = 0x0100; c < 0x0138; c += 2)
    ASSERT (gl_iswupper (c, locale));
  for (c = 0x0101; c < 0x0138; c += 2)
    ASSERT (!gl_iswupper (c, locale));

  for (c = 0x0080; c < 0x10000; c++)
    ASSERT (!gl_iswxdigit (c, locale));

  for (c = 0x2000; c < 0x2007; c++)
    ASSERT (gl_iswblank (c, locale));
  for (c = 0x00a0; c < 0x00a1; c++)
    ASSERT (!gl_iswblank (c, locale));

#endif

  /* Also test the <glocale/ctype.h> functions in the non-ASCII part.  */

  gl_setencoding (locale, "UTF-8");

  for (c = 0x80; c < 0x100; c++)
    {
      ASSERT (!gl_isalnum (c, locale));
      ASSERT (!gl_isalpha (c, locale));
      ASSERT (!gl_iscntrl (c, locale));
      ASSERT (!gl_isdigit (c, locale));
      ASSERT (!gl_isgraph (c, locale));
      ASSERT (!gl_islower (c, locale));
      ASSERT (!gl_isprint (c, locale));
      ASSERT (!gl_ispunct (c, locale));
      ASSERT (!gl_isspace (c, locale));
      ASSERT (!gl_isupper (c, locale));
      ASSERT (!gl_isxdigit (c, locale));
      ASSERT (!gl_isblank (c, locale));
    }

  gl_setencoding (locale, "ISO-8859-1");

  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (gl_isalnum (c, locale));
  for (c = 0xa0; c < 0xa9; c++)
    ASSERT (!gl_isalnum (c, locale));

  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (gl_isalpha (c, locale));
  for (c = 0xa0; c < 0xa9; c++)
    ASSERT (!gl_isalpha (c, locale));

  for (c = 0x80; c < 0xa0; c++)
    ASSERT (gl_iscntrl (c, locale));
  for (c = 0xa0; c < 0x100; c++)
    ASSERT (!gl_iscntrl (c, locale));

  for (c = 0x80; c < 0x100; c++)
    ASSERT (!gl_isdigit (c, locale));

  for (c = 0xa0; c < 0x100; c++)
    ASSERT (gl_isgraph (c, locale));
  for (c = 0x80; c < 0xa0; c++)
    ASSERT (!gl_isgraph (c, locale));

  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (gl_islower (c, locale));
  for (c = 0xc0; c < 0xdf; c++)
    ASSERT (!gl_islower (c, locale));

  for (c = 0xa0; c < 0x100; c++)
    ASSERT (gl_isprint (c, locale));
  for (c = 0x80; c < 0xa0; c++)
    ASSERT (!gl_isprint (c, locale));

  for (c = 0xa0; c < 0xa2; c++)
    ASSERT (gl_ispunct (c, locale));
  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (!gl_ispunct (c, locale));

  for (c = 0xa0; c < 0xa1; c++)
    ASSERT (!gl_isspace (c, locale));

  for (c = 0xc0; c < 0xd7; c++)
    ASSERT (gl_isupper (c, locale));
  for (c = 0xe0; c < 0xff; c++)
    ASSERT (!gl_isupper (c, locale));

  for (c = 0x80; c < 0x100; c++)
    ASSERT (!gl_isxdigit (c, locale));

  for (c = 0xa0; c < 0xa1; c++)
    ASSERT (!gl_isblank (c, locale));

  return 0;
}
