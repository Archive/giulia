/* Test of case-insensitive string comparison functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/string.h>
#include <glocale/wchar.h>
#include <stdlib.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;

  /* Test the gl_strcasecmp function.  */

  locale = gl_newlocale ();

  ASSERT (gl_strcasecmp ("abi", "ABI", locale) == 0);
  ASSERT (gl_strcasecmp ("ABI", "abi", locale) == 0);

  ASSERT (gl_strcasecmp ("Abi", "BcD", locale) < 0);
  ASSERT (gl_strcasecmp ("BcD", "Abi", locale) > 0);

  ASSERT (gl_strcasecmp ("boo", "Boogie", locale) < 0);
  ASSERT (gl_strcasecmp ("Boogie", "boo", locale) > 0);

  gl_setlocale (locale, LC_ALL, "de_DE");

  ASSERT (gl_strcasecmp ("abi", "ABI", locale) == 0);
  ASSERT (gl_strcasecmp ("ABI", "abi", locale) == 0);

  ASSERT (gl_strcasecmp ("Abi", "BcD", locale) < 0);
  ASSERT (gl_strcasecmp ("BcD", "Abi", locale) > 0);

  ASSERT (gl_strcasecmp ("boo", "Boogie", locale) < 0);
  ASSERT (gl_strcasecmp ("Boogie", "boo", locale) > 0);

  /* In the turkish locale, there is no correspondence between 'i' and 'I',
     but rather between 'i' and 'İ' and between 'ı' and 'I'.  */

  gl_setlocale (locale, LC_ALL, "tr_TR");

  ASSERT (gl_strcasecmp ("abi", "ABI", locale) < 0);
  ASSERT (gl_strcasecmp ("ABI", "abi", locale) > 0);

  ASSERT (gl_strcasecmp ("abi", "AB\304\260", locale) == 0);
  ASSERT (gl_strcasecmp ("AB\304\260", "abi", locale) == 0);

  ASSERT (gl_strcasecmp ("ab\304\261", "ABI", locale) == 0);
  ASSERT (gl_strcasecmp ("ABI", "ab\304\261", locale) == 0);

  ASSERT (gl_strcasecmp ("Abi", "BcD", locale) < 0);
  ASSERT (gl_strcasecmp ("BcD", "Abi", locale) > 0);

  ASSERT (gl_strcasecmp ("boo", "Boogie", locale) < 0);
  ASSERT (gl_strcasecmp ("Boogie", "boo", locale) > 0);

#if GLOCALE_ENABLE_WIDE_CHAR

  {
    static const wchar_t test1_arg1[] = { 'a', 'b', 'i', 0 };
    static const wchar_t test1_arg2[] = { 'A', 'B', 'I', 0 };
    static const wchar_t test1a_arg1[] = { 'a', 'b', 'i', 0 };
    static const wchar_t test1a_arg2[] = { 'A', 'B', 0x0130, 0 };
    static const wchar_t test1b_arg1[] = { 'a', 'b', 0x0131, 0 };
    static const wchar_t test1b_arg2[] = { 'A', 'B', 'I', 0 };
    static const wchar_t test2_arg1[] = { 'A', 'b', 'i', 0 };
    static const wchar_t test2_arg2[] = { 'B', 'c', 'D', 0 };
    static const wchar_t test3_arg1[] = { 'b', 'o', 'o', 0 };
    static const wchar_t test3_arg2[] = { 'B', 'o', 'o', 'g', 'i', 'e', 0 };

    /* Test the gl_wcscasecmp function.  */

    locale = gl_newlocale ();

    ASSERT (gl_wcscasecmp (test1_arg1, test1_arg2, locale) == 0);
    ASSERT (gl_wcscasecmp (test1_arg2, test1_arg1, locale) == 0);

    ASSERT (gl_wcscasecmp (test2_arg1, test2_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test2_arg2, test2_arg1, locale) > 0);

    ASSERT (gl_wcscasecmp (test3_arg1, test3_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test3_arg2, test3_arg1, locale) > 0);

    gl_setlocale (locale, LC_ALL, "de_DE");

    ASSERT (gl_wcscasecmp (test1_arg1, test1_arg2, locale) == 0);
    ASSERT (gl_wcscasecmp (test1_arg2, test1_arg1, locale) == 0);

    ASSERT (gl_wcscasecmp (test2_arg1, test2_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test2_arg2, test2_arg1, locale) > 0);

    ASSERT (gl_wcscasecmp (test3_arg1, test3_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test3_arg2, test3_arg1, locale) > 0);

    /* In the turkish locale, there is no correspondence between 'i' and 'I',
       but rather between 'i' and 'İ' and between 'ı' and 'I'.  */

    gl_setlocale (locale, LC_ALL, "tr_TR");

    ASSERT (gl_wcscasecmp (test1_arg1, test1_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test1_arg2, test1_arg1, locale) > 0);

    ASSERT (gl_wcscasecmp (test1a_arg1, test1a_arg2, locale) == 0);
    ASSERT (gl_wcscasecmp (test1a_arg2, test1a_arg1, locale) == 0);

    ASSERT (gl_wcscasecmp (test1b_arg1, test1b_arg2, locale) == 0);
    ASSERT (gl_wcscasecmp (test1b_arg2, test1b_arg1, locale) == 0);

    ASSERT (gl_wcscasecmp (test2_arg1, test2_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test2_arg2, test2_arg1, locale) > 0);

    ASSERT (gl_wcscasecmp (test3_arg1, test3_arg2, locale) < 0);
    ASSERT (gl_wcscasecmp (test3_arg2, test3_arg1, locale) > 0);

    /* Test the gl_wcsncasecmp function.  */

    locale = gl_newlocale ();

    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 100000000, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 100000000, locale) == 0);

    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 1, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 1, locale) > 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 3, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 3, locale) > 0);

    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 4, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 4, locale) > 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 100000000, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 100000000, locale) > 0);

    gl_setlocale (locale, LC_ALL, "de_DE");

    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 100000000, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 100000000, locale) == 0);

    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 1, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 1, locale) > 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 3, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 3, locale) > 0);

    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 4, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 4, locale) > 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 100000000, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 100000000, locale) > 0);

    /* In the turkish locale, there is no correspondence between 'i' and 'I',
       but rather between 'i' and 'İ' and between 'ı' and 'I'.  */

    gl_setlocale (locale, LC_ALL, "tr_TR");

    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 2, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 2, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 3, locale) < 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 3, locale) > 0);
    ASSERT (gl_wcsncasecmp (test1_arg1, test1_arg2, 4, locale) < 0);
    ASSERT (gl_wcsncasecmp (test1_arg2, test1_arg1, 4, locale) > 0);

    ASSERT (gl_wcsncasecmp (test1a_arg1, test1a_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1a_arg2, test1a_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1a_arg1, test1a_arg2, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1a_arg2, test1a_arg1, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1a_arg1, test1a_arg2, 100000000, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1a_arg2, test1a_arg1, 100000000, locale) == 0);

    ASSERT (gl_wcsncasecmp (test1b_arg1, test1b_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1b_arg2, test1b_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1b_arg1, test1b_arg2, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1b_arg2, test1b_arg1, 4, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1b_arg1, test1b_arg2, 100000000, locale) == 0);
    ASSERT (gl_wcsncasecmp (test1b_arg2, test1b_arg1, 100000000, locale) == 0);

    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 1, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 1, locale) > 0);
    ASSERT (gl_wcsncasecmp (test2_arg1, test2_arg2, 3, locale) < 0);
    ASSERT (gl_wcsncasecmp (test2_arg2, test2_arg1, 3, locale) > 0);

    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 0, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 3, locale) == 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 4, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 4, locale) > 0);
    ASSERT (gl_wcsncasecmp (test3_arg1, test3_arg2, 100000000, locale) < 0);
    ASSERT (gl_wcsncasecmp (test3_arg2, test3_arg1, 100000000, locale) > 0);
  }

#endif

  return 0;
}
