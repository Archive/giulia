/* Test of character mapping functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/ctype.h>
#include <glocale/wctype.h>
#include <stdlib.h>

#include "c-ctype.h"

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;
  unsigned int c;

  locale = gl_newlocale ();

  /* The default locale has only ASCII characters.  */

#if GLOCALE_ENABLE_WIDE_CHAR
  for (c = 0; c < 0x10000; c++)
    {
      ASSERT (gl_towlower (c, locale) == (c < 0x80 ? c_tolower (c) : c));
      ASSERT (gl_towupper (c, locale) == (c < 0x80 ? c_toupper (c) : c));
    }
#endif

  for (c = 0; c < 0x100; c++)
    {
      ASSERT (gl_tolower (c, locale) == c_tolower (c));
      ASSERT (gl_toupper (c, locale) == c_toupper (c));
    }

  /* Other locales have all sorts of Unicode characters.  */

  gl_setlocale (locale, LC_ALL, "de_DE");

#if GLOCALE_ENABLE_WIDE_CHAR
  for (c = 0; c < 0x80; c++)
    {
      ASSERT (gl_towlower (c, locale) == c_tolower (c));
      ASSERT (gl_towupper (c, locale) == c_toupper (c));
    }
#endif

  for (c = 0; c < 0x80; c++)
    {
      ASSERT (gl_tolower (c, locale) == c_tolower (c));
      ASSERT (gl_toupper (c, locale) == c_toupper (c));
    }

#if GLOCALE_ENABLE_WIDE_CHAR

  for (c = 0x0100; c < 0x0130; c += 2)
    ASSERT (gl_towlower (c, locale) == c + 1);
  for (c = 0x0101; c < 0x0130; c += 2)
    ASSERT (gl_towlower (c, locale) == c);

  for (c = 0x0100; c < 0x0130; c += 2)
    ASSERT (gl_towupper (c, locale) == c);
  for (c = 0x0101; c < 0x0130; c += 2)
    ASSERT (gl_towupper (c, locale) == c - 1);

#endif

  /* Also test the <glocale/ctype.h> functions in the non-ASCII part.  */

  gl_setencoding (locale, "UTF-8");

  for (c = 0x80; c < 0x100; c++)
    {
      ASSERT (gl_tolower (c, locale) == c);
      ASSERT (gl_toupper (c, locale) == c);
    }

  gl_setencoding (locale, "ISO-8859-1");

  for (c = 0xc0; c < 0xd7; c++)
    ASSERT (gl_tolower (c, locale) == c + 0x20);
  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (gl_tolower (c, locale) == c);

  for (c = 0xc0; c < 0xd7; c++)
    ASSERT (gl_toupper (c, locale) == c);
  for (c = 0xe0; c < 0xf7; c++)
    ASSERT (gl_toupper (c, locale) == c - 0x20);

  return 0;
}
