/* Test of wide-character to multibyte conversion functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/stdlib.h>
#include <glocale/wchar.h>
#include <glocale/locale.h>
#include <errno.h>
#include <langinfo.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

#if GLOCALE_ENABLE_WIDE_CHAR

static void
test_wctomb (void)
{
  gl_locale_t locale = gl_newlocale ();
  char buf[6];

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 'x', locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x9C, locale) == -1
	  && buf[0] == 0x55);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 'x', locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x0116, locale) == 2
	  && buf[0] == (char)0xC4 && buf[1] == (char)0x96 && buf[2] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x30000000, locale) == -1
	  && buf[0] == 0x55);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 'x', locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x00AA, locale) == 3
	  && buf[0] == (char)0x8F && buf[1] == (char)0xA2
	  && buf[2] == (char)0xEC && buf[3] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x965D, locale) == 2
	  && buf[0] == (char)0xF0 && buf[1] == (char)0xA1 && buf[2] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  ASSERT (gl_wctomb (buf, 0x00AB, locale) == -1
	  && buf[0] == 0x55);
}

static void
test_wcrtomb (void)
{
  gl_locale_t locale = gl_newlocale ();
  char buf[6];
  mbstate_t state;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0, &state, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 'x', &state, locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x9C, &state, locale) == (size_t)-1
	  && errno == EILSEQ && buf[0] == 0x55);

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0, &state, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 'x', &state, locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x0116, &state, locale) == 2
	  && buf[0] == (char)0xC4 && buf[1] == (char)0x96 && buf[2] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x30000000, &state, locale) == (size_t)-1
	  && errno == EILSEQ && buf[0] == 0x55);

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0, &state, locale) == 1
	  && buf[0] == 0 && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 'x', &state, locale) == 1
	  && buf[0] == 'x' && buf[1] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x00AA, &state, locale) == 3
	  && buf[0] == (char)0x8F && buf[1] == (char)0xA2
	  && buf[2] == (char)0xEC && buf[3] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x965D, &state, locale) == 2
	  && buf[0] == (char)0xF0 && buf[1] == (char)0xA1 && buf[2] == 0x55);

  memset (buf, 0x55, sizeof (buf));
  memset (&state, 0, sizeof (mbstate_t));
  ASSERT (gl_wcrtomb (buf, 0x00AB, &state, locale) == (size_t)-1
	  && errno == EILSEQ && buf[0] == 0x55);
}

static void
test_wcstombs (void)
{
  gl_locale_t locale = gl_newlocale ();
  char buf[10];

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  {
    wchar_t src[] = { 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 1, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t src[] = { 'S', 'u', 'e', 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 3);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 4, locale) == 3
	    && buf[0] == 'S' && buf[1] == 'u' && buf[2] == 'e' && buf[3] == 0
	    && buf[4] == 0x55);
  }

  {
    wchar_t src[] = { 'S', '\235', 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 2, locale) == -1);
  }

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  {
    wchar_t src[] = { 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 1, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t src[] = { 'S', 0x00FC, 0x00DF, 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 5);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 6, locale) == 5
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0
	    && buf[6] == 0x55);
  }

  {
    wchar_t src[] = { 'S', 0x30000000, 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 2, locale) == -1);
  }

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  {
    wchar_t src[] = { 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 1, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t src[] = { 0x65E5, 0x672C, 0x8A9E, 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == 6);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 0, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 7, locale) == 6
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0 && buf[7] == 0x55);
  }

  {
    wchar_t src[] = { 'S', 0x00AB, 0 };

    ASSERT (gl_wcstombs (NULL, src, 0, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    ASSERT (gl_wcstombs (buf, src, 2, locale) == -1);
  }
}

static void
test_wcsrtombs (void)
{
  gl_locale_t locale = gl_newlocale ();
  char buf[10];
  mbstate_t state;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 'u', 'e', 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 3);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 4, &state, locale) == 3
	    && buf[0] == 'S' && buf[1] == 'u' && buf[2] == 'e' && buf[3] == 0
	    && buf[4] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', '\235', 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 2, &state, locale) == -1);
  }

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x00FC, 0x00DF, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 5);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 6, &state, locale) == 5
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0
	    && buf[6] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 4, &state, locale) == 4
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == 0x55);
    ASSERT (gl_wcsrtombs (buf + 4, &src, 2, &state, locale) == 1
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0
	    && buf[6] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x30000000, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 2, &state, locale) == -1);
  }

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 0x65E5, 0x672C, 0x8A9E, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == 6);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 7, &state, locale) == 6
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0 && buf[7] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 3, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == 0x55);
    ASSERT (gl_wcsrtombs (buf + 3, &src, 4, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0 && buf[7] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x00AB, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (NULL, &src, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsrtombs (buf, &src, 2, &state, locale) == -1);
  }
}

static void
test_wcsnrtombs (void)
{
  gl_locale_t locale = gl_newlocale ();
  char buf[10];
  mbstate_t state;

  /* Test ASCII encoding.  */

  gl_setlocale (locale, LC_ALL, "C");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 1, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 'u', 'e', 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 4, 0, &state, locale) == 3);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 4, &state, locale) == 3
	    && buf[0] == 'S' && buf[1] == 'u' && buf[2] == 'e' && buf[3] == 0
	    && buf[4] == 0x55);
  }

  {
    /* Same thing, limited to nwc = 3 wide characters.  */
    wchar_t srcorig[] = { 'S', 'u', 'e', '\235', 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == 3);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 3, &state, locale) == 3
	    && buf[0] == 'S' && buf[1] == 'u' && buf[2] == 'e'
	    && buf[3] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', '\235', 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 2, &state, locale) == -1);
  }

  /* Test UTF-8 encoding.  */

  gl_setlocale (locale, LC_ALL, "de_DE.UTF-8");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 1, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x00FC, 0x00DF, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 4, 0, &state, locale) == 5);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 6, &state, locale) == 5
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0
	    && buf[6] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, srcorig + 4 - src, 4, &state, locale) == 4
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == 0x55);
    ASSERT (gl_wcsnrtombs (buf + 4, &src, srcorig + 4 - src, 2, &state, locale) == 1
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0
	    && buf[6] == 0x55);
  }

  {
    /* Same thing, limited to nwc = 3 wide characters.  */
    wchar_t srcorig[] = { 'S', 0x00FC, 0x00DF, 0x30000000, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == 5);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 6, &state, locale) == 5
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, srcorig + 3 - src, 4, &state, locale) == 4
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == 0x55);
    ASSERT (gl_wcsnrtombs (buf + 4, &src, srcorig + 3 - src, 2, &state, locale) == 1
	    && buf[0] == 'S' && buf[1] == (char)0xC3 && buf[2] == (char)0xBC
	    && buf[3] == (char)0xC3 && buf[4] == (char)0x9F && buf[5] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x30000000, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 2, &state, locale) == -1);
  }

  /* Test iconv() based encoding.  */

  gl_setlocale (locale, LC_ALL, "ja_JP.EUC-JP");

  {
    wchar_t srcorig[] = { 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 1, 0, &state, locale) == 0);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 1, 1, &state, locale) == 0
	    && buf[0] == 0 && buf[1] == 0x55);
  }

  {
    wchar_t srcorig[] = { 0x65E5, 0x672C, 0x8A9E, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 4, 0, &state, locale) == 6);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 4, 7, &state, locale) == 6
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0 && buf[7] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, srcorig + 4 - src, 3, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == 0x55);
    ASSERT (gl_wcsnrtombs (buf + 3, &src, srcorig + 4 - src, 4, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0 && buf[7] == 0x55);
  }

  {
    /* Same thing, limited to nwc = 3 wide characters.  */
    wchar_t srcorig[] = { 0x65E5, 0x672C, 0x8A9E, 0x00AB, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == 6);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 0, &state, locale) == 0
	    && buf[0] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 7, &state, locale) == 6
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0x55);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, srcorig + 3 - src, 3, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == 0x55);
    ASSERT (gl_wcsnrtombs (buf + 3, &src, srcorig + 3 - src, 4, &state, locale) == 3
	    && buf[0] == (char)0xC6 && buf[1] == (char)0xFC
	    && buf[2] == (char)0xCB && buf[3] == (char)0xDC
	    && buf[4] == (char)0xB8 && buf[5] == (char)0xEC
	    && buf[6] == 0x55);
  }

  {
    wchar_t srcorig[] = { 'S', 0x00AB, 0 };
    const wchar_t *src;

    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (NULL, &src, 3, 0, &state, locale) == -1);

    memset (buf, 0x55, sizeof (buf));
    src = srcorig;
    memset (&state, 0, sizeof (mbstate_t));
    ASSERT (gl_wcsnrtombs (buf, &src, 3, 2, &state, locale) == -1);
  }
}

#endif

int
main ()
{
#if GLOCALE_ENABLE_WIDE_CHAR
  test_wctomb ();
  test_wcrtomb ();
  test_wcstombs ();
  test_wcsrtombs ();
  test_wcsnrtombs ();
#endif

  return 0;
}
