/* Test of character width functions.
   Copyright (C) 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

/* Written by Bruno Haible <bruno@clisp.org>, 2005.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <glocale/wchar.h>
#include <glocale/string.h>
#include <stdlib.h>

#include "c-ctype.h"
#include "ucs4-utf8.h"

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

int
main ()
{
  gl_locale_t locale;
  unsigned int c;

  locale = gl_newlocale ();

  /* Test gl_wcwidth function.  */

#if GLOCALE_ENABLE_WIDE_CHAR

  /* The default locale has only ASCII characters.  */

  ASSERT (gl_wcwidth (0, locale) == 0);
  for (c = 1; c < 0x80; c++)
    ASSERT (gl_wcwidth (c, locale) == (c_isprint (c) ? 1 : -1));
  for (c = 0x80; c < 0x10000; c++)
    ASSERT (gl_wcwidth (c, locale) == -1);

  /* Other locales have all sorts of Unicode characters.  */

  gl_setlocale (locale, LC_ALL, "de_DE");

  ASSERT (gl_wcwidth (0, locale) == 0);
  for (c = 1; c < 0x80; c++)
    ASSERT (gl_wcwidth (c, locale) == (c_isprint (c) ? 1 : -1));
  for (c = 0x0080; c < 0x00a0; c++)
    ASSERT (gl_wcwidth (c, locale) == -1);
  for (c = 0x00a0; c < 0x0200; c++)
    ASSERT (gl_wcwidth (c, locale) == 1);
  for (c = 0x2500; c < 0x2600; c++)
    ASSERT (gl_wcwidth (c, locale) == 1);
  for (c = 0xac00; c < 0xd7a4; c++)
    ASSERT (gl_wcwidth (c, locale) == 2);
  ASSERT (gl_wcwidth (0xfeff, locale) == 0);
  for (c = 0x20000; c < 0x2a6d7; c++)
    ASSERT (gl_wcwidth (c, locale) == 2);
  for (c = 0xe0020; c < 0xe0080; c++)
    ASSERT (gl_wcwidth (c, locale) == 0);

#endif

  /* Test gl_strwidth function.  */

  gl_setlocale (locale, LC_ALL, "de_DE");

  ASSERT (gl_strwidth ("", locale) == 0);

  {
    char buf[6];

    for (c = 1; c < 0x80; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == (c_isprint (c) ? 1 : 0));
      }

    for (c = 0x0080; c < 0x00a0; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 0);
      }

    for (c = 0x00a0; c < 0x0200; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 1);
      }

    for (c = 0x2500; c < 0x2600; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 1);
      }

    for (c = 0xac00; c < 0xd7a4; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 2);
      }

    for (c = 0xfeff; c <= 0xfeff; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 0);
      }

    for (c = 0x20000; c < 0x2a6d7; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 2);
      }

    for (c = 0xe0020; c < 0xe0080; c++)
      {
	buf[u8_uctomb ((unsigned char *) buf, c, 6)] = '\0';
	ASSERT (gl_strwidth (buf, locale) == 0);
      }
  }

  {
    const char *pango = "\316\240\316\261\316\275\350\252\236"; /* "Παν語" */

    ASSERT (gl_strwidth (pango, locale) == 5);

    /* Test gl_strnwidth function.  */

    ASSERT (gl_strnwidth (pango, 0, locale) == 0);
    ASSERT (gl_strnwidth (pango, 1, locale) == 1); /* incomplete at the end */
    ASSERT (gl_strnwidth (pango, 2, locale) == 1);
    ASSERT (gl_strnwidth (pango, 3, locale) == 2); /* incomplete at the end */
    ASSERT (gl_strnwidth (pango, 4, locale) == 2);
    ASSERT (gl_strnwidth (pango, 5, locale) == 3); /* incomplete at the end */
    ASSERT (gl_strnwidth (pango, 6, locale) == 3);
    ASSERT (gl_strnwidth (pango, 7, locale) == 4); /* incomplete at the end */
    ASSERT (gl_strnwidth (pango, 8, locale) == 4   /* incomplete at the end */
	    || gl_strnwidth (pango, 8, locale) == 5);
    ASSERT (gl_strnwidth (pango, 9, locale) == 5);
    ASSERT (gl_strnwidth (pango, 10, locale) == 5);
  }

  return 0;
}
