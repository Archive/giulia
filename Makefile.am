## Makefile for the glocale directory of GNU gettext
## Copyright (C) 2005 Free Software Foundation, Inc.
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2, or (at your option)
## any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software Foundation,
## Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

## Process this file with automake to produce Makefile.in.

AUTOMAKE_OPTIONS = 1.5 gnu no-dependencies
ACLOCAL_AMFLAGS = -I m4
EXTRA_DIST =
BUILT_SOURCES =
MOSTLYCLEANFILES =

SUBDIRS = include m4 lib . cook localedata tests

localedir = $(datadir)/locale
aliaspath = $(localedir)

lib_LTLIBRARIES = libglocale.la

libglocale_la_SOURCES =

# General utilities.
libglocale_la_SOURCES += \
  lib/obstack.h lib/obstack.c \
  lib/hash.h lib/hash.c \
  lib/hashuniq.h lib/hashuniq.c \
  lib/lock.h lib/lock.c \
  lib/tls.h lib/tls.c \
  lib/struniq.h lib/struniq.c \
  lib/strnlen1.c \
  lib/utf8-ucs4.h \
  lib/ucs4-utf8.h

# Private header files.
libglocale_la_SOURCES += \
  glocale-private.h

# The gl_locale_t datatype.
libglocale_la_SOURCES += \
  libintl/localealias.c \
  locale/facet-address.c \
  locale/facet-collate.c \
  locale/facet-ctype-encoding.c locale/facet-ctype.c \
  locale/facet-identification.c \
  locale/facet-measurement.c \
  locale/facet-messages.c \
  locale/facet-monetary.c \
  locale/facet-name.c \
  locale/facet-numeric.c \
  locale/facet-paper.c \
  locale/facet-telephone.c \
  locale/facet-time.c \
  locale/C.c \
  locale/newlocale.c \
  locale/duplocale.c \
  locale/setlocale.c \
  locale/setencoding.c \
  locale/freelocale.c

# The locale data file formats.
libglocale_la_SOURCES += \
  locale/file-ctype.h

# Loading locale data files.
libglocale_la_SOURCES += \
  locale/loadfile.c \
  locale/file-ctype.c

# Retrieving translations from .mo files.
libglocale_la_SOURCES += \
  libintl/explodename.c \
  libintl/l10nflist.c \
  libintl/plural.c \
  libintl/plural-exp.c \
  libintl/hash-string.c \
  libintl/loadmsgcat.c \
  libintl/finddomain.c \
  libintl/log.c \
  libintl/dcigettext.c \
  libintl/_dcgettext.c \
  libintl/_dcngettext.c \
  libintl/dcgettext.c \
  libintl/dcngettext.c \
  libintl/dgettext.c \
  libintl/dngettext.c

# Elementary and auxiliary functions for multibyte string handling.
libglocale_la_SOURCES += \
  lib/small.h \
  wchar/btowc.c \
  wchar/wctob.c \
  stdlib/mb_cur_max.c \
  wchar/mbtouc_ascii.c \
  wchar/mbtouc_utf8.c \
  wchar/mbtouc_iconv.c \
  wchar/uctomb_ascii.c \
  wchar/uctomb_utf8.c \
  wchar/uctomb_iconv.c \
  wchar/mbsinit.c \
  wchar/thread_specific.c

# Multibyte character to wide character conversion.
libglocale_la_SOURCES += \
  wchar/mbrtowc.c \
  wchar/mbrlen.c \
  stdlib/mbtowc.c \
  stdlib/mblen.c \
  wchar/mbsnrtowcs.c \
  wchar/mbsrtowcs.c \
  stdlib/mbstowcs.c

# Wide character to multibyte character conversion.
libglocale_la_SOURCES += \
  wchar/wcrtomb.c \
  stdlib/wctomb.c \
  wchar/wcsnrtombs.c \
  wchar/wcsrtombs.c \
  stdlib/wcstombs.c

# Character type classification.
libglocale_la_SOURCES += \
  ctype/wchar-lookup.h \
  ctype/init-ctype-table.c \
  ctype/init-ctype-array.c \
  ctype/isalnum.c \
  ctype/isalpha.c \
  ctype/iscntrl.c \
  ctype/isdigit.c \
  ctype/isgraph.c \
  ctype/islower.c \
  ctype/isprint.c \
  ctype/ispunct.c \
  ctype/isspace.c \
  ctype/isupper.c \
  ctype/isxdigit.c \
  ctype/isblank.c \
  wctype/iswalnum.c \
  wctype/iswalpha.c \
  wctype/iswcntrl.c \
  wctype/iswdigit.c \
  wctype/iswgraph.c \
  wctype/iswlower.c \
  wctype/iswprint.c \
  wctype/iswpunct.c \
  wctype/iswspace.c \
  wctype/iswupper.c \
  wctype/iswxdigit.c \
  wctype/iswblank.c \
  wctype/wctype.c \
  wctype/iswctype.c

# Character mappings.
libglocale_la_SOURCES += \
  ctype/init-trans-table.c \
  ctype/init-trans-array.c \
  ctype/tolower.c \
  ctype/toupper.c \
  wctype/towlower.c \
  wctype/towupper.c \
  wctype/wctrans.c \
  wctype/towctrans.c

# Character width.
libglocale_la_SOURCES += \
  ctype/init-width-table.c \
  wchar/wcwidth.c \
  wchar/wcswidth.c

# Multibyte character datatype.
libglocale_la_SOURCES += \
  string/mbchar.h string/mbchar.c \
  string/mbiter.h \
  string/mbuiter.h \
  string/mbfile.h

# String comparison.
libglocale_la_SOURCES += \
  string/strcasecmp.c \
  wchar/wcscasecmp.c \
  wchar/wcsncasecmp.c

# String search.
libglocale_la_SOURCES += \
  string/strstr.c \
  string/strcasestr.c

# String width.
libglocale_la_SOURCES += \
  string/strnwidth.c \
  string/strwidth.c

# Accessing miscellaneous locale dependent information.
libglocale_la_SOURCES += \
  locale/defaulted.c \
  string/strerror_aux.c \
  string/strerror_r.c \
  string/strerror.c \
  langinfo/nl_langinfo.c

libglocale_la_LIBADD = lib/liblib.la @LTLIBINTL@

# -DBUILDING_LIBGLOCALE: Change expansion of LIBGLOCALE_DLL_EXPORTED macro.
# -DBUILDING_DLL: Change expansion of RELOCATABLE_DLL_EXPORTED macro.
AM_CPPFLAGS = \
  -Ibuild-include -Ilib -I$(srcdir)/lib -I$(srcdir)/locale \
  -Ilibintl -I$(srcdir)/libintl -I$(srcdir)/ctype \
  -DIN_LIBGLOCALE -DDOMAIN=\"glocale\" \
  -DBUILDING_LIBGLOCALE -DBUILDING_DLL \
  -DGLOCDIR=\"$(exec_prefix)\" \
  -DLOCALEDIR=\"$(localedir)\" -DLOCALE_ALIAS_PATH=\"$(aliaspath)\"
# TODO: Support for relocatability.

AM_CFLAGS = @CFLAG_VISIBILITY@

# Temporary include files, only for the build.

BUILTHEADERS = \
  build-include/glocale.h \
  build-include/glocale/config.h \
  build-include/glocale/ctype.h \
  build-include/glocale/langinfo.h \
  build-include/glocale/libintl.h \
  build-include/glocale/locale.h \
  build-include/glocale/monetary.h \
  build-include/glocale/regex.h \
  build-include/glocale/stdio.h \
  build-include/glocale/stdlib.h \
  build-include/glocale/string.h \
  build-include/glocale/time.h \
  build-include/glocale/wchar.h \
  build-include/glocale/wctype.h

BUILT_SOURCES += $(BUILTHEADERS)

build-include/glocale.h : $(srcdir)/include/glocale.h include/export.h
	@$(mkdir_p) build-include
	sed -e '/#define _GNU_LOCALE_H/r include/export.h' \
	  < $(srcdir)/include/glocale.h > build-include/glocale.h

build-include/glocale/config.h : include/glocale/config.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < include/glocale/config.h > build-include/glocale/config.h

build-include/glocale/ctype.h : $(srcdir)/include/glocale/ctype.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/ctype.h > build-include/glocale/ctype.h

build-include/glocale/langinfo.h : $(srcdir)/include/glocale/langinfo.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/langinfo.h > build-include/glocale/langinfo.h

build-include/glocale/libintl.h : $(srcdir)/include/glocale/libintl.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/libintl.h > build-include/glocale/libintl.h

build-include/glocale/locale.h : $(srcdir)/include/glocale/locale.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/locale.h > build-include/glocale/locale.h

build-include/glocale/monetary.h : $(srcdir)/include/glocale/monetary.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/monetary.h > build-include/glocale/monetary.h

build-include/glocale/regex.h : $(srcdir)/include/glocale/regex.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/regex.h > build-include/glocale/regex.h

build-include/glocale/stdio.h : $(srcdir)/include/glocale/stdio.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/stdio.h > build-include/glocale/stdio.h

build-include/glocale/stdlib.h : $(srcdir)/include/glocale/stdlib.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/stdlib.h > build-include/glocale/stdlib.h

build-include/glocale/string.h : $(srcdir)/include/glocale/string.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/string.h > build-include/glocale/string.h

build-include/glocale/time.h : $(srcdir)/include/glocale/time.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/time.h > build-include/glocale/time.h

build-include/glocale/wchar.h : $(srcdir)/include/glocale/wchar.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/wchar.h > build-include/glocale/wchar.h

build-include/glocale/wctype.h : $(srcdir)/include/glocale/wctype.h include/export.h
	@$(mkdir_p) build-include/glocale
	sed -e '/#define _GNU_LOCALE_/r include/export.h' \
	    -e 's/extern \([^"]\)/extern LIBGLOCALE_DLL_EXPORTED \1/' \
	  < $(srcdir)/include/glocale/wctype.h > build-include/glocale/wctype.h

MOSTLYCLEANFILES += $(BUILTHEADERS)

# Built source files.

BUILT_SOURCES += libintl/libgnuintl.h
MOSTLYCLEANFILES += libintl/libgnuintl.h
libintl/libgnuintl.h : $(srcdir)/libintl/libgnuintl.h.in
	@test -d libintl || mkdir libintl
	sed -e 's,@''HAVE_POSIX_PRINTF''@,1,g' \
	    -e 's,@''HAVE_ASPRINTF''@,0,g' \
	    -e 's,@''HAVE_SNPRINTF''@,0,g' \
	    -e 's,@''HAVE_WPRINTF''@,0,g' \
	  < $(srcdir)/libintl/libgnuintl.h.in \
	| sed -e 's,_LIBINTL_H,_LIBGNUINTL_H,' \
	| sed -e 's,@''HAVE_VISIBILITY''@,@HAVE_VISIBILITY@,g' \
	  > libintl/libgnuintl.h

BUILT_SOURCES += libintl/plural.c
MOSTLYCLEANFILES += libintl/plural.c
libintl/plural.c : $(srcdir)/libintl/plural.y
	@test -d libintl || mkdir libintl
	@INTLBISON@ -y -d --name-prefix=__gettext --output $@ $<
	rm -f libintl/plural.h
