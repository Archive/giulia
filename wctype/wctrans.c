/* Look up a wide character mapping.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wctype.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <string.h>
#include "glocale-private.h"

gl_wctrans_t
gl_wctrans (const char *property, gl_locale_t locale)
{
  int index;
  struct gl_locale_ctype *facet;
  wctype_table table;

  if (strcmp (property, "tolower") == 0)
    index = TRANS_TOLOWER;
  else if (strcmp (property, "toupper") == 0)
    index = TRANS_TOUPPER;
  else
    return NULL;

  facet = locale->facet_LC_CTYPE;
  table = facet->table_trans[index];
  if (table == NULL)
    table = _gl_init_trans_table (facet, index, property, locale);

  return (gl_wctrans_t) table;
}

#endif
