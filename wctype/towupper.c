/* Mapping a wide character.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wctype.h>
#include "glocale-private.h"

#include "wchar-lookup.h"

#if GLOCALE_ENABLE_WIDE_CHAR
wint_t
gl_towupper (wint_t wc, gl_locale_t locale)
#else
unsigned int
_gl_touupper (unsigned int wc, gl_locale_t locale)
#endif
{
  struct gl_locale_ctype *facet = locale->facet_LC_CTYPE;
  wctrans_table table = facet->table_trans[TRANS_TOUPPER];

  if (table == NULL)
    table = _gl_init_trans_table (facet, TRANS_TOUPPER, "toupper", locale);

  return wctrans_table_lookup (table, wc);
}
