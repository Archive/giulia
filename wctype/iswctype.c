/* Testing a wide character property.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wctype.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include "glocale-private.h"
#include "wchar-lookup.h"

int
gl_iswctype (wint_t wc, gl_wctype_t desc, gl_locale_t locale)
{
  /* We can ignore the locale here, because it has already been taken into
     account in the gl_wctype() function.  */
  wctype_table table = (wctype_table) desc;

  return wctype_table_lookup (table, wc);
}

#endif
