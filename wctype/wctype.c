/* Look up a wide character property.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/wctype.h>

#if GLOCALE_ENABLE_WIDE_CHAR

#include <string.h>
#include "glocale-private.h"

gl_wctype_t
gl_wctype (const char *property, gl_locale_t locale)
{
  int index;
  struct gl_locale_ctype *facet;
  wctype_table table;

  if (strcmp (property, "alnum") == 0)
    index = CTYPE_ALNUM;
  else if (strcmp (property, "alpha") == 0)
    index = CTYPE_ALPHA;
  else if (strcmp (property, "cntrl") == 0)
    index = CTYPE_CNTRL;
  else if (strcmp (property, "digit") == 0)
    index = CTYPE_DIGIT;
  else if (strcmp (property, "graph") == 0)
    index = CTYPE_GRAPH;
  else if (strcmp (property, "lower") == 0)
    index = CTYPE_LOWER;
  else if (strcmp (property, "print") == 0)
    index = CTYPE_PRINT;
  else if (strcmp (property, "punct") == 0)
    index = CTYPE_PUNCT;
  else if (strcmp (property, "space") == 0)
    index = CTYPE_SPACE;
  else if (strcmp (property, "upper") == 0)
    index = CTYPE_UPPER;
  else if (strcmp (property, "xdigit") == 0)
    index = CTYPE_XDIGIT;
  else if (strcmp (property, "blank") == 0)
    index = CTYPE_BLANK;
  else
    return NULL;

  facet = locale->facet_LC_CTYPE;
  table = facet->table_ctype[index];
  if (table == NULL)
    table = _gl_init_ctype_table (facet, index, property, locale);

  return (gl_wctype_t) table;
}

#endif
