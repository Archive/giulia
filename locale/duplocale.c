/* Copying a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/locale.h>

#include <stdlib.h>
#include <string.h>
#include "glocale-private.h"


gl_locale_t
gl_duplocale (gl_locale_t old_locale)
{
  gl_locale_t new_locale;

  new_locale = (struct glocale *) malloc (sizeof (struct glocale));
  if (new_locale != NULL)
    {
      new_locale->facet_LC_CTYPE = old_locale->facet_LC_CTYPE;
      new_locale->facet_LC_NUMERIC = old_locale->facet_LC_NUMERIC;
      new_locale->facet_LC_TIME = old_locale->facet_LC_TIME;
      new_locale->facet_LC_COLLATE = old_locale->facet_LC_COLLATE;
      new_locale->facet_LC_MONETARY = old_locale->facet_LC_MONETARY;
      new_locale->facet_LC_MESSAGES = old_locale->facet_LC_MESSAGES;
      new_locale->facet_LC_PAPER = old_locale->facet_LC_PAPER;
      new_locale->facet_LC_NAME = old_locale->facet_LC_NAME;
      new_locale->facet_LC_ADDRESS = old_locale->facet_LC_ADDRESS;
      new_locale->facet_LC_TELEPHONE = old_locale->facet_LC_TELEPHONE;
      new_locale->facet_LC_MEASUREMENT = old_locale->facet_LC_MEASUREMENT;
      new_locale->facet_LC_IDENTIFICATION = old_locale->facet_LC_IDENTIFICATION;
      new_locale->fullname = old_locale->fullname;
#if GLOCALE_ENABLE_WIDE_CHAR
      gl_lock_init (new_locale->thread_specific_lock);
      new_locale->thread_specific_initialized = 0;
      memset (&new_locale->thread_specific_fallback, '\0',
	      sizeof (struct gl_thread_specific));
#endif
    }
  return new_locale;
}
