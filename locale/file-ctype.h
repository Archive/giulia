/* File format of mmapable LC_CTYPE locale data file.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef FILE_CTYPE_H
#define FILE_CTYPE_H

/* All numbers are in the machine's endianness.
   All strings are UTF-8 strings.  */

/* A wctype entry consists of
   1) In the header:
        - 4-byte alignment,
        - The wctype's name, as a NUL-terminated ASCII string,
        - Padding to 4-byte alignment,
        - The file offset of the table, as a 32-bit number.
   2) At the specified file offset:
        A 3-level table, as specified in ctype/wchar-lookup.h
        for wctype_table_lookup.  */

/* A wctrans entry consists of
   1) In the header:
        - 4-byte alignment,
        - The wctrans's name, as a NUL-terminated ASCII string,
        - Padding to 4-byte alignment,
        - The file offset of the table, as a 32-bit number.
   2) At the specified file offset:
        A 3-level table, as specified in ctype/wchar-lookup.h
        for wctrans_table_lookup.  */

/* A wcwidth entry consists of
   1) In the header: The file offset of the table, as a 32-bit number.
   2) At the specified file offset:
        A 3-level table, as specified in ctype/wchar-lookup.h
        for wcwidth_table_lookup.  */

/* The header consists of
     - The file offset at which the wctype entries in the header start.
     - The file offset at which the wctrans entries in the header start.
     - The file offset of the wcwidth entry.
     - The wctype entries, followed by a an empty string and padding.
     - The wctrans entries, followed by a an empty string and padding.
   Large tables come afterwards.  */

struct file_ctype
{
  unsigned int header_wctype_offset;
  unsigned int header_wctrans_offset;
  unsigned int wcwidth_offset;
};

#endif /* FILE_CTYPE_H */
