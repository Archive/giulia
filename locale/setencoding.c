/* Changing the encoding of a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/locale.h>

#include <stdlib.h>
#include <string.h>

#include "glocale-private.h"
#include "struniq.h"

void
gl_setencoding (gl_locale_t locale, const char *encoding)
{
  const char *old_name = locale->facet_LC_CTYPE->name;
  size_t encoding_len = strlen (encoding);
  char *new_name = (char *) malloc (strlen (old_name) + 1 + encoding_len + 1);
  if (new_name == NULL)
    return;

  /* Copy all parts of old_name except the encoding, and insert the given
     encoding.  */
  {
    const char *p = old_name;
    char *q = new_name;

    while (*p != '\0' && *p != '.' && *p != '@')
      *q++ = *p++;

    if (*p == '.')
      do
	p++;
      while (*p != '\0' && *p != '@');

    *q++ = '.';
    memcpy (q, encoding, encoding_len);
    q += encoding_len;

    while ((*q++ = *p++) != '\0')
      ;
  }

  /* Uniquify the resulting name, to avoid a memory leak if this function
     gets called repeatedly.  Then change the LC_CTYPE facet.  */
  gl_setlocale (locale, LC_CTYPE, struniq (new_name));
}
