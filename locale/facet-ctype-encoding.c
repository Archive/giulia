/* Initializing the encoding dependent parts of a LC_CTYPE facet of a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <errno.h>
#include <iconv.h>
#include <stdlib.h>

/* Determine whether the encoding is UTF-8.  */
static inline int
guess_is_utf8 (iconv_t cd_from_utf8)
{
  /* UTF-8 is the only encoding which converts 0xE4B887 as a single character
     and doesn't convert 0xE4B8.  */
  if (cd_from_utf8 != (iconv_t)(-1))
    {
      char buf[3];
      char out[3];
      const char *inbuf;
      size_t inbytesleft;
      char *outbuf;
      size_t outbytesleft;
      size_t result;

      buf[0] = 0xe4; buf[1] = 0xb8; buf[2] = 0x87; /* U+4E07 */
      iconv (cd_from_utf8, NULL, NULL, NULL, NULL);
      inbuf = buf; inbytesleft = 3; outbuf = out; outbytesleft = 2;
      result = iconv (cd_from_utf8,
		      (ICONV_CONST char **)&inbuf, &inbytesleft,
		      &outbuf, &outbytesleft);
      if (result == (size_t)(-1) && errno == E2BIG)
	{
	  inbuf = buf; inbytesleft = 3; outbuf = out; outbytesleft = 3;
	  result = iconv (cd_from_utf8,
			  (ICONV_CONST char **)&inbuf, &inbytesleft,
			  &outbuf, &outbytesleft);
	  if (result == 0 && inbytesleft == 0 && outbytesleft == 0
	      && out[0] == buf[0] && out[1] == buf[1] && out[2] == buf[2])
	    return 1;
	}
    }
  return 0;
}

/* Guess/compute the maximum number of bytes per multi-byte character.  */
static inline size_t
guess_mb_cur_max (iconv_t cd_from_utf8)
{
  /* MB_CUR_MAX is the MAX_NEEDED_FROM value of the glibc/iconvdata
     converters.  Most of them are 1, except a few which we can
     recognize from their conversion results.

                                              U+0100      U+4E07
       European:
         ANSI_X3.110-1983 (ISO-IR-99)      2  0xC541
         ISO-IR-90 (ISO_6937-2)            2  0xC541
         ISO_6937 (ISO-IR-156)             2  0xC541
       Chinese:
         BIG5, BIG5-HKSCS, CP950           2              0xC945
         EUC-TW                            4              0x8EA2A1A6
         GB2312 (EUC-CN)                   2              0xCDF2
         GBK                               2              0xCDF2
         GB18030                           4  0x81308B38  0xCDF2
       Japanese:
         EUC-JP                            3  0x8FAAA7    0xCBFC
         SHIFT_JIS, CP932, IBM932, IBM943  2              0x969C
       Korean:
         EUC-KR, UHC (CP949)               2              0xD8B2
         JOHAB                             2              0xE742

     So we need to convert just two Unicode characters!  */
  size_t max = 1;

  if (cd_from_utf8 != (iconv_t)(-1))
    {
      char buf[3];
      char out[64];
      const char *inbuf;
      size_t inbytesleft;
      char *outbuf;
      size_t outbytesleft;
      size_t result;

      buf[0] = 0xc4; buf[1] = 0x80; /* U+0100 */
      iconv (cd_from_utf8, NULL, NULL, NULL, NULL);
      inbuf = buf; inbytesleft = 2; outbuf = out; outbytesleft = sizeof (out);
      result = iconv (cd_from_utf8,
		      (ICONV_CONST char **)&inbuf, &inbytesleft,
		      &outbuf, &outbytesleft);
      if (result == 0 && inbytesleft == 0)
	{
	  size_t outbytes = sizeof (out) - outbytesleft;
	  if (outbytes > max)
	    max = outbytes;
	}

      buf[0] = 0xe4; buf[1] = 0xb8; buf[2] = 0x87; /* U+4E07 */
      iconv (cd_from_utf8, NULL, NULL, NULL, NULL);
      inbuf = buf; inbytesleft = 3; outbuf = out; outbytesleft = sizeof (out);
      result = iconv (cd_from_utf8,
		      (ICONV_CONST char **)&inbuf, &inbytesleft,
		      &outbuf, &outbytesleft);
      if (result == 0 && inbytesleft == 0)
	{
	  size_t outbytes = sizeof (out) - outbytesleft;
	  if (outbytes > max)
	    max = outbytes;
	}
    }

  return max;
}

/* Initialize the fields of the facet that depend only on the encoding.  */
void
_gl_init_facet_ctype_encoding_dependents (struct gl_locale_ctype *facet)
{
  gl_lock_init (facet->iconv_to_lock);
  gl_lock_init (facet->iconv_from_lock);
  facet->cd_to_utf8 = iconv_open ("UTF-8", facet->encoding);
  facet->cd_from_utf8 = iconv_open (facet->encoding, "UTF-8");
  facet->is_utf8 = guess_is_utf8 (facet->cd_from_utf8);
  facet->mb_cur_max =
    (facet->is_utf8 ? 4 : guess_mb_cur_max (facet->cd_from_utf8));
  if (facet->is_utf8)
    {
      facet->mbtouc = _gl_mbtouc_utf8;
      facet->uctomb = _gl_uctomb_utf8;
    }
  else
    {
      facet->mbtouc =
	(facet->cd_to_utf8 != (iconv_t)(-1) ? _gl_mbtouc_iconv :
	 /* Assume facet->encoding is equivalent to ASCII.  */
	 _gl_mbtouc_ascii);
      facet->uctomb =
	(facet->cd_from_utf8 != (iconv_t)(-1) ? _gl_uctomb_iconv :
	 /* Assume facet->encoding is equivalent to ASCII.  */
	 _gl_uctomb_ascii);
    }
}

/* Destroy the fields of the facet that depend only on the encoding.  */
void
_gl_fini_facet_ctype_encoding_dependents (struct gl_locale_ctype *facet)
{
  if (facet->cd_to_utf8 != (iconv_t)(-1))
    iconv_close (facet->cd_to_utf8);
  if (facet->cd_from_utf8 != (iconv_t)(-1))
    iconv_close (facet->cd_from_utf8);
  gl_lock_destroy (facet->iconv_from_lock);
  gl_lock_destroy (facet->iconv_to_lock);
}
