/* Loading the LC_NAME facet of a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "struniq.h"


gl_lock_define_initialized(static, lock)
static int initialized;
static hash_table facet_name_table;

/* Forward declaration of local functions.  */
static struct gl_locale_name *
get_facet_name_locked (const char *name, const char *name_end,
		       const char *encoding);

static struct gl_locale_name *
create_facet_name (const char *name, const char *name_end,
		   const char *encoding)
{
  struct gl_locale_name *facet;

  if ((name_end - name == 1 && memcmp (name, "C", 1) == 0)
      || (name_end - name == 5 && memcmp (name, "POSIX", 5) == 0))
    return &gl_locale_name_C;

  /* Expand locale alias.  */
  if (*name_end == '\0')
    {
      const char *expanded_name = _gl_expand_alias (name);
      if (expanded_name != NULL)
	{
	  const char *expanded_name_end = expanded_name + strlen (expanded_name);
	  return get_facet_name_locked (expanded_name, expanded_name_end,
					encoding);
	}
    }

  /* Allocate the result.  */
  facet = (struct gl_locale_name *) malloc (sizeof (struct gl_locale_name));
  if (facet == NULL)
    return NULL;

  /* Canonicalize the name.  */
  {
    char *name_copy;

    name_copy = (char *) malloc (name_end - name + 1);
    if (name_copy == NULL)
      {
	free (facet);
	return NULL;
      }
    memcpy (name_copy, name, name_end - name);
    name_copy[name_end - name] = '\0';

    name = struniq (name_copy);
  }
  facet->name = name;

  return facet;
}

static struct gl_locale_name *
get_facet_name_locked (const char *name, const char *name_end,
		       const char *encoding)
{
  size_t len;
  char *key;
  void *found;
  struct gl_locale_name *result;

  if (!initialized)
    {
      if (gl_hash_init (&facet_name_table, 10) < 0)
	return NULL;
      initialized = 1;
    }
  len = (name_end - name) + 1 + strlen (encoding);
  key = (char *) malloc (len);
  if (key == NULL)
    return create_facet_name (name, name_end, encoding);
  memcpy (key, name, name_end - name);
  key[name_end - name] = '\0';
  memcpy (key + (name_end - name) + 1, encoding, strlen (encoding));
  if (gl_hash_find_entry (&facet_name_table, key, len, &found) == 0)
    result = (struct gl_locale_name *) found;
  else
    {
      result = create_facet_name (name, name_end, encoding);
      if (result != NULL)
	gl_hash_insert_entry (&facet_name_table, key, len, result);
    }
  free (key);
  return result;
}

struct gl_locale_name *
gl_get_facet_name (const char *name, const char *encoding)
{
  const char *name_end = name + strlen (name);
  struct gl_locale_name *result;

  /* Recognize mixed locale syntax "category=value; ...".  */
  {
    const char *p;

    for (p = name; *p != '\0'; )
      {
	const char *next_semicolon = strchr (p, ';');
	if (next_semicolon == NULL)
	  break;
	if (strncmp (p, "LC_NAME=", strlen ("LC_NAME=")) == 0)
	  {
	    name = p + strlen ("LC_NAME=");
	    name_end = next_semicolon;
	    break;
	  }
	p = next_semicolon + 1;
      }
  }

  gl_lock_lock (lock);
  result = get_facet_name_locked (name, name_end, encoding);
  gl_lock_unlock (lock);
  return result;
}

