/* Loading of mmapable LC_CTYPE locale data file.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

/* Cache of preceding lookup results.  */
static struct file_cache file_ctype_cache = FILE_CACHE_INITIALIZER;

/* Initializes facet->mmaped_file_ctype, where facet belongs to locale.
   Must only be called with facet->data_lock being held.
   Returns the new value of facet->mmaped_file_ctype.  It may be
   (struct file_ctype *)-1 to indicate that the mapped file was not found.  */
const struct file_ctype *
_gl_init_file_ctype_locked (struct gl_locale_ctype *facet)
{
  /* If another thread already initialized it, return it.  */
  const struct file_ctype *file_contents = facet->mmaped_file_ctype;
  if (file_contents != NULL)
    return file_contents;

  return facet->mmaped_file_ctype =
	   (const struct file_ctype *)
	   _gl_load_file (&file_ctype_cache, "LC_CTYPE/ctype.loc",
			  facet->name);
}
