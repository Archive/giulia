/* Loading of mmapable locale data file.
   Copyright (C) 1996-2003, 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005,
   based on code by Ulrich Drepper <drepper@cygnus.com>, 1996.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#if HAVE_UNISTD_H
# include <unistd.h>
#endif
#if HAVE_MMAP
# include <sys/mman.h>
#endif

#include "loadinfo.h"

/* For systems that distinguish between text and binary I/O.
   O_BINARY is usually declared in <fcntl.h>. */
#if !defined O_BINARY && defined _O_BINARY
  /* For MSC-compatible compilers.  */
# define O_BINARY _O_BINARY
# define O_TEXT _O_TEXT
#endif
#ifdef __BEOS__
  /* BeOS 5 has O_BINARY and O_TEXT, but they have no effect.  */
# undef O_BINARY
# undef O_TEXT
#endif
/* On reasonable systems, binary I/O is the default.  */
#ifndef O_BINARY
# define O_BINARY 0
#endif

/* Load a file's data into memory.
   Sets file->data.
   Sets file->decided to 1.  */
static void
load_file (struct loaded_l10nfile *file)
{
  /* See whether the file exists.  */
  int fd;

  file->decided = 1;
  file->data = NULL;
  fd = open (file->filename, O_RDONLY | O_BINARY);
  if (fd >= 0)
    {
      struct stat statbuf;
      size_t size;

      /* See whether the file is usable.  */
      if (fstat (fd, &statbuf) >= 0
	  && !S_ISDIR (statbuf.st_mode)
	  && (size = (size_t) statbuf.st_size) == statbuf.st_size
	  && size >= 4)
	{
	  /* Get the file contents.  */
	  const void *data;
#if HAVE_MMAP
	  data =
	    (const void *) mmap (NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
	  if (data == (const void *) -1)
#endif
	    {
	      void *memory = malloc (size);
	      if (memory != NULL)
		{
		  /* Read size bytes into memory.
		     We cannot use the gnulib module 'safe-read'
		     because it's GPL.  */
		  size_t to_read = size;
		  char *read_ptr = (char *) memory;
		  do
		    {
		      long nb = read (fd, read_ptr, to_read);
		      if (nb <= 0)
			{
#ifdef EINTR
			  if (nb == -1 && errno == EINTR)
			    continue;
#endif
			  /* Some I/O error occurred.  */
			  free (memory);
			  memory = NULL;
			  break;
			}
		      read_ptr += nb;
		      to_read -= nb;
		    }
		  while (to_read > 0);
		}
	      data = memory;
	    }
	  /* data now points to the file's contents, or is NULL if
	     out of memory or some I/O error occurred.  */
	  file->data = data;
	}

      close (fd);
    }
}

/* Load a machine-dependent file into read-only memory and return a pointer
   to its contents.  The PATHNAME is relative to the locale's directory.  */
const void *
_gl_load_file (struct file_cache *cache, const char *pathname,
	       const char *localename)
{
  /* The base directory for the locale data files is $GLOCDIR/lib/glocale.
     We use "lib", not "share", because the files are endianness dependent.
     We use $GLOCDIR/lib, not $(libdir), because there's no need to make a
     distinction between 32-bit and 64-bit environments on platforms like
     sparc64 or amd64.  */
  const char *glocdir = getenv ("GLOCDIR");

  if (glocdir == NULL || glocdir[0] == '\0')
    glocdir = GLOCDIR;

  /* Construct the base directory name.  */
  {
    char *dir = (char *) malloc (strlen (glocdir) + 1 + 3 + 1 + 7 + 1);

    if (dir == NULL)
      /* Out of memory.  */
      goto failed;

    {
      char *p = dir;

      memcpy (p, glocdir, strlen (glocdir));
      p += strlen (glocdir);

      if (p == dir || p[-1] != '/')
	*p++ = '/';

      memcpy (p, "lib", 3);
      p += 3;

      *p++ = '/';

      memcpy (p, "glocale", 7);
      p += 7;

      *p = '\0';
    }

    /* See if the locale name is actually an alias.  */
    {
      const char *alias = _nl_expand_alias (localename);
      if (alias != NULL)
	localename = alias;
    }

    /* Split the locale name into pieces.  */
    {
      char *localename_copy;
      int mask;
      const char *language;
      const char *modifier;
      const char *territory;
      const char *codeset;
      const char *normalized_codeset;
      const char *special;
      const char *sponsor;
      const char *revision;

      localename_copy = (char *) malloc (strlen (localename) + 1);
      if (localename_copy == NULL)
	/* Out of memory.  */
	goto failed1;
      strcpy (localename_copy, localename);

      mask = _nl_explode_name (localename_copy, &language, &modifier,
			       &territory, &codeset, &normalized_codeset,
			       &special, &sponsor, &revision);

      /* The space for normalized_codeset is dynamically allocated.
	 Free it.  */
      if (mask & XPG_NORM_CODESET)
	free ((void *) normalized_codeset);

      /* Our data files are codeset independent.  Therefore ignore
	 codeset specifications in the locale name.  */
      mask &= ~(XPG_CODESET | XPG_NORM_CODESET);
      codeset = NULL;
      normalized_codeset = NULL;

      /* Lookup the locale file at several possible locations.  */
      gl_lock_lock (cache->lock);
      {
	struct loaded_l10nfile *file =
	  _nl_make_l10nflist (&cache->results, dir, strlen (dir) + 1,
			      mask, language, territory, codeset,
			      normalized_codeset, modifier, special, sponsor,
			      revision, pathname, 1);

	if (file == NULL)
	  /* Unknown locale or out of memory.  */
	  goto failed2;

	if (file->decided == 0)
	  load_file (file);
	ASSERT (file->decided);
	if (file->data == NULL)
	  {
	    int cnt;
	    for (cnt = 0; file->successor[cnt] != NULL; cnt++)
	      {
		if (file->successor[cnt]->decided == 0)
		  load_file (file->successor[cnt]);
		if (file->successor[cnt]->data != NULL)
		  break;
	      }
	    /* Move the entry we found (or NULL) to the first place of
	       successors, to speed up later searches.  */
	    file->successor[0] = file->successor[cnt];
	    file = file->successor[cnt];
	    if (file == NULL)
	      /* Unknown locale or out of memory.  */
	      goto failed2;
	  }
	ASSERT (file->data != NULL);
	gl_lock_unlock (cache->lock);
	return file->data;
      }
     failed2:
      gl_lock_unlock (cache->lock);
      free (localename_copy);
    }
   failed1:
    free (dir);
  }
 failed:
  return (const void *)-1;
}
