/* Retrieving locale-dependent string values.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <glocale/libintl.h>
#include <stdlib.h>
#include <string.h>
#include "lock.h"

/* Need to call bindtextdomain() before the first use of DOMAIN.  */
gl_once_define(static, init_domain_dir_once)
static void
init_domain_dir (void)
{
  const char *dir = getenv ("GLOCDIR");
  if (dir != NULL && dir[0] != '\0')
    {
      /* The base directory for the locale message catalog files is
	 $GLOCDIR/share/locale.  We use "share", because GNU .mo files are
	 endianness independent.  */
      char *localedir = (char *) malloc (strlen (dir) + 13 + 1);

      if (localedir != NULL)
	{
	  char *p;

	  strcpy (localedir, dir);
	  p = localedir + strlen (dir);
	  if (p[-1] == '/')
	    p--;
	  memcpy (p, "/share/locale", 13 + 1);

	  bindtextdomain (DOMAIN, localedir);

	  free (localedir);
	}
    }
  /* Otherwise the default dir corresponds to the installation location
     of glocale; this is what we need.  */
}

/* Looks up a translation in a message catalog.
   The default_value is taken from share/locale/C/<category>/glocale.mo.  */
const char *
_gl_defaulted_dcgettext (const char *msgid, const char *default_value,
			 int category, gl_locale_t locale)
{
  const char *translation;

  /* Ensure that GLOCDIR is taken into account.  */
  gl_once (init_domain_dir_once, init_domain_dir);

  translation = gl_dcgettext (DOMAIN, msgid, category, locale);
  if (translation != msgid)
    return translation;
  else
    return default_value;
}
