/* Creation of a new locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/locale.h>

#include <stdlib.h>
#include <string.h>
#include "glocale-private.h"


gl_locale_t
gl_newlocale (void)
{
  gl_locale_t locale;

  locale = (struct glocale *) malloc (sizeof (struct glocale));
  if (locale != NULL)
    {
      locale->facet_LC_CTYPE = &gl_locale_ctype_C;
      locale->facet_LC_NUMERIC = &gl_locale_numeric_C;
      locale->facet_LC_TIME = &gl_locale_time_C;
      locale->facet_LC_COLLATE = &gl_locale_collate_C;
      locale->facet_LC_MONETARY = &gl_locale_monetary_C;
      locale->facet_LC_MESSAGES = &gl_locale_messages_C;
      locale->facet_LC_PAPER = &gl_locale_paper_C;
      locale->facet_LC_NAME = &gl_locale_name_C;
      locale->facet_LC_ADDRESS = &gl_locale_address_C;
      locale->facet_LC_TELEPHONE = &gl_locale_telephone_C;
      locale->facet_LC_MEASUREMENT = &gl_locale_measurement_C;
      locale->facet_LC_IDENTIFICATION = &gl_locale_identification_C;
      locale->fullname = "C";
#if GLOCALE_ENABLE_WIDE_CHAR
      gl_lock_init (locale->thread_specific_lock);
      locale->thread_specific_initialized = 0;
      memset (&locale->thread_specific_fallback, '\0',
	      sizeof (struct gl_thread_specific));
#endif
    }
  return locale;
}
