/* Free a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/locale.h>

#include <stdlib.h>


void
gl_freelocale (gl_locale_t locale)
{
#if GLOCALE_ENABLE_WIDE_CHAR
  /* No need to lock on locale->thread_specific_lock here, because if some
     other thread is still using this locale, havoc will happen anyway.  */
  if (locale->thread_specific_initialized > 0)
    /* TODO: free() each per-thread value of locale->thread_specific_key.  */
    gl_tls_key_destroy (locale->thread_specific_key);
#endif
  free (locale);
}
