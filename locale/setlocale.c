/* Changing the facets of a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include <glocale/locale.h>

#include <stdlib.h>
#include <string.h>

#include "glocale-private.h"
#include "struniq.h"


/* Construct the full name of a locale from the names of each facet.
   Return NULL upon memory allocation failure.  */
static const char *
construct_fullname (const char *name_LC_CTYPE,
		    const char *name_LC_NUMERIC,
		    const char *name_LC_TIME,
		    const char *name_LC_COLLATE,
		    const char *name_LC_MONETARY,
		    const char *name_LC_MESSAGES,
		    const char *name_LC_PAPER,
		    const char *name_LC_NAME,
		    const char *name_LC_ADDRESS,
		    const char *name_LC_TELEPHONE,
		    const char *name_LC_MEASUREMENT,
		    const char *name_LC_IDENTIFICATION)
{
  if (strcmp (name_LC_CTYPE, name_LC_NUMERIC) == 0
      && strcmp (name_LC_CTYPE, name_LC_TIME) == 0
      && strcmp (name_LC_CTYPE, name_LC_COLLATE) == 0
      && strcmp (name_LC_CTYPE, name_LC_MONETARY) == 0
      && strcmp (name_LC_CTYPE, name_LC_MESSAGES) == 0
      && strcmp (name_LC_CTYPE, name_LC_PAPER) == 0
      && strcmp (name_LC_CTYPE, name_LC_NAME) == 0
      && strcmp (name_LC_CTYPE, name_LC_ADDRESS) == 0
      && strcmp (name_LC_CTYPE, name_LC_TELEPHONE) == 0
      && strcmp (name_LC_CTYPE, name_LC_MEASUREMENT) == 0
      && strcmp (name_LC_CTYPE, name_LC_IDENTIFICATION) == 0)
    return name_LC_CTYPE;
  else
    {
      size_t len = strlen ("LC_CTYPE") + 1 + strlen (name_LC_CTYPE) + 1
		   + strlen ("LC_NUMERIC") + 1 + strlen (name_LC_NUMERIC) + 1
		   + strlen ("LC_TIME") + 1 + strlen (name_LC_TIME) + 1
		   + strlen ("LC_COLLATE") + 1 + strlen (name_LC_COLLATE) + 1
		   + strlen ("LC_MONETARY") + 1 + strlen (name_LC_MONETARY) + 1
		   + strlen ("LC_MESSAGES") + 1 + strlen (name_LC_MESSAGES) + 1
		   + strlen ("LC_PAPER") + 1 + strlen (name_LC_PAPER) + 1
		   + strlen ("LC_NAME") + 1 + strlen (name_LC_NAME) + 1
		   + strlen ("LC_ADDRESS") + 1 + strlen (name_LC_ADDRESS) + 1
		   + strlen ("LC_TELEPHONE") + 1 + strlen (name_LC_TELEPHONE) + 1
		   + strlen ("LC_MEASUREMENT") + 1 + strlen (name_LC_MEASUREMENT) + 1
		   + strlen ("LC_IDENTIFICATION") + 1 + strlen (name_LC_IDENTIFICATION) + 1
		   + 1;
      char *result = (char *) malloc (len);

      if (result != NULL)
	{
	  char *p = result;

	  strcpy (p, "LC_CTYPE"); p += strlen ("LC_CTYPE");
	  *p++ = '=';
	  strcpy (p, name_LC_CTYPE); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_NUMERIC"); p += strlen ("LC_NUMERIC");
	  *p++ = '=';
	  strcpy (p, name_LC_NUMERIC); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_TIME"); p += strlen ("LC_TIME");
	  *p++ = '=';
	  strcpy (p, name_LC_TIME); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_COLLATE"); p += strlen ("LC_COLLATE");
	  *p++ = '=';
	  strcpy (p, name_LC_COLLATE); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_MONETARY"); p += strlen ("LC_MONETARY");
	  *p++ = '=';
	  strcpy (p, name_LC_MONETARY); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_MESSAGES"); p += strlen ("LC_MESSAGES");
	  *p++ = '=';
	  strcpy (p, name_LC_MESSAGES); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_PAPER"); p += strlen ("LC_PAPER");
	  *p++ = '=';
	  strcpy (p, name_LC_PAPER); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_NAME"); p += strlen ("LC_NAME");
	  *p++ = '=';
	  strcpy (p, name_LC_NAME); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_ADDRESS"); p += strlen ("LC_ADDRESS");
	  *p++ = '=';
	  strcpy (p, name_LC_ADDRESS); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_TELEPHONE"); p += strlen ("LC_TELEPHONE");
	  *p++ = '=';
	  strcpy (p, name_LC_TELEPHONE); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_MEASUREMENT"); p += strlen ("LC_MEASUREMENT");
	  *p++ = '=';
	  strcpy (p, name_LC_MEASUREMENT); p += strlen (p);
	  *p++ = ';';

	  strcpy (p, "LC_IDENTIFICATION"); p += strlen ("LC_IDENTIFICATION");
	  *p++ = '=';
	  strcpy (p, name_LC_IDENTIFICATION); p += strlen (p);
	  *p++ = ';';

	  *p = '\0';

	  return struniq (result);
	}

      return result;
    }
}

/* Construct the full name of a locale from the names of each facet,
   anticipating the change of a single category.
   Return NULL upon memory allocation failure.  */
static const char *
construct_new_fullname (gl_locale_t locale, int category, const char *value)
{
  return
    construct_fullname (
      category == LC_CTYPE ? value : locale->facet_LC_CTYPE->name,
      category == LC_NUMERIC ? value : locale->facet_LC_NUMERIC->name,
      category == LC_TIME ? value : locale->facet_LC_TIME->name,
      category == LC_COLLATE ? value : locale->facet_LC_COLLATE->name,
      category == LC_MONETARY ? value : locale->facet_LC_MONETARY->name,
      category == LC_MESSAGES ? value : locale->facet_LC_MESSAGES->name,
      category == LC_PAPER ? value : locale->facet_LC_PAPER->name,
      category == LC_NAME ? value : locale->facet_LC_NAME->name,
      category == LC_ADDRESS ? value : locale->facet_LC_ADDRESS->name,
      category == LC_TELEPHONE ? value : locale->facet_LC_TELEPHONE->name,
      category == LC_MEASUREMENT ? value : locale->facet_LC_MEASUREMENT->name,
      category == LC_IDENTIFICATION ? value : locale->facet_LC_IDENTIFICATION->name);
}

const char *
gl_setlocale (gl_locale_t locale, int category, const char *value)
{
  switch (category)
    {
    case LC_CTYPE:
      if (value == NULL)
	return locale->facet_LC_CTYPE->name;
      {
	struct gl_locale_ctype *facet_LC_CTYPE = gl_get_facet_ctype (value);
	if (facet_LC_CTYPE != NULL)
	  {
	    struct gl_locale_numeric *facet_LC_NUMERIC =
	      gl_get_facet_numeric (locale->facet_LC_NUMERIC->name,
				    facet_LC_CTYPE->encoding);
	    struct gl_locale_time *facet_LC_TIME =
	      gl_get_facet_time (locale->facet_LC_TIME->name,
				 facet_LC_CTYPE->encoding);
	    struct gl_locale_collate *facet_LC_COLLATE =
	      gl_get_facet_collate (locale->facet_LC_COLLATE->name,
				    facet_LC_CTYPE->encoding);
	    struct gl_locale_monetary *facet_LC_MONETARY =
	      gl_get_facet_monetary (locale->facet_LC_MONETARY->name,
				     facet_LC_CTYPE->encoding);
	    struct gl_locale_messages *facet_LC_MESSAGES =
	      gl_get_facet_messages (locale->facet_LC_MESSAGES->name,
				     facet_LC_CTYPE->encoding);
	    struct gl_locale_paper *facet_LC_PAPER =
	      gl_get_facet_paper (locale->facet_LC_PAPER->name,
				  facet_LC_CTYPE->encoding);
	    struct gl_locale_name *facet_LC_NAME =
	      gl_get_facet_name (locale->facet_LC_NAME->name,
				 facet_LC_CTYPE->encoding);
	    struct gl_locale_address *facet_LC_ADDRESS =
	      gl_get_facet_address (locale->facet_LC_ADDRESS->name,
				    facet_LC_CTYPE->encoding);
	    struct gl_locale_telephone *facet_LC_TELEPHONE =
	      gl_get_facet_telephone (locale->facet_LC_TELEPHONE->name,
				      facet_LC_CTYPE->encoding);
	    struct gl_locale_measurement *facet_LC_MEASUREMENT =
	      gl_get_facet_measurement (locale->facet_LC_MEASUREMENT->name,
					facet_LC_CTYPE->encoding);
	    struct gl_locale_identification *facet_LC_IDENTIFICATION =
	      gl_get_facet_identification (locale->facet_LC_IDENTIFICATION->name,
					   facet_LC_CTYPE->encoding);
	    if (facet_LC_NUMERIC != NULL && facet_LC_TIME != NULL
		&& facet_LC_COLLATE != NULL && facet_LC_MONETARY != NULL
		&& facet_LC_MESSAGES != NULL && facet_LC_PAPER != NULL
		&& facet_LC_NAME != NULL && facet_LC_ADDRESS != NULL
		&& facet_LC_TELEPHONE != NULL && facet_LC_MEASUREMENT != NULL
		&& facet_LC_IDENTIFICATION != NULL)
	      {
		const char *fullname =
		  construct_fullname (
		    facet_LC_CTYPE->name,
		    facet_LC_NUMERIC->name,
		    facet_LC_TIME->name,
		    facet_LC_COLLATE->name,
		    facet_LC_MONETARY->name,
		    facet_LC_MESSAGES->name,
		    facet_LC_PAPER->name,
		    facet_LC_NAME->name,
		    facet_LC_ADDRESS->name,
		    facet_LC_TELEPHONE->name,
		    facet_LC_MEASUREMENT->name,
		    facet_LC_IDENTIFICATION->name);
		if (fullname != NULL)
		  {
		    locale->facet_LC_CTYPE = facet_LC_CTYPE;
		    locale->facet_LC_NUMERIC = facet_LC_NUMERIC;
		    locale->facet_LC_TIME = facet_LC_TIME;
		    locale->facet_LC_COLLATE = facet_LC_COLLATE;
		    locale->facet_LC_MONETARY = facet_LC_MONETARY;
		    locale->facet_LC_MESSAGES = facet_LC_MESSAGES;
		    locale->facet_LC_PAPER = facet_LC_PAPER;
		    locale->facet_LC_NAME = facet_LC_NAME;
		    locale->facet_LC_ADDRESS = facet_LC_ADDRESS;
		    locale->facet_LC_TELEPHONE = facet_LC_TELEPHONE;
		    locale->facet_LC_MEASUREMENT = facet_LC_MEASUREMENT;
		    locale->facet_LC_IDENTIFICATION = facet_LC_IDENTIFICATION;
		    locale->fullname = fullname;
		    return locale->fullname;
		  }
	      }
	  }
	return NULL;
      }
    case LC_NUMERIC:
      if (value == NULL)
	return locale->facet_LC_NUMERIC->name;
      {
	struct gl_locale_numeric *facet =
	  gl_get_facet_numeric (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_NUMERIC, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_NUMERIC = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_TIME:
      if (value == NULL)
	return locale->facet_LC_TIME->name;
      {
	struct gl_locale_time *facet =
	  gl_get_facet_time (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_TIME, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_TIME = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_COLLATE:
      if (value == NULL)
	return locale->facet_LC_COLLATE->name;
      {
	struct gl_locale_collate *facet =
	  gl_get_facet_collate (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_COLLATE, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_COLLATE = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_MONETARY:
      if (value == NULL)
	return locale->facet_LC_MONETARY->name;
      {
	struct gl_locale_monetary *facet =
	  gl_get_facet_monetary (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_MONETARY, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_MONETARY = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_MESSAGES:
      if (value == NULL)
	return locale->facet_LC_MESSAGES->name;
      {
	struct gl_locale_messages *facet =
	  gl_get_facet_messages (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_MESSAGES, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_MESSAGES = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_PAPER:
      if (value == NULL)
	return locale->facet_LC_PAPER->name;
      {
	struct gl_locale_paper *facet =
	  gl_get_facet_paper (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_PAPER, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_PAPER = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_NAME:
      if (value == NULL)
	return locale->facet_LC_NAME->name;
      {
	struct gl_locale_name *facet =
	  gl_get_facet_name (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_NAME, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_NAME = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_ADDRESS:
      if (value == NULL)
	return locale->facet_LC_ADDRESS->name;
      {
	struct gl_locale_address *facet =
	  gl_get_facet_address (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_ADDRESS, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_ADDRESS = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_TELEPHONE:
      if (value == NULL)
	return locale->facet_LC_TELEPHONE->name;
      {
	struct gl_locale_telephone *facet =
	  gl_get_facet_telephone (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_TELEPHONE, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_TELEPHONE = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_MEASUREMENT:
      if (value == NULL)
	return locale->facet_LC_MEASUREMENT->name;
      {
	struct gl_locale_measurement *facet =
	  gl_get_facet_measurement (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_MEASUREMENT, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_MEASUREMENT = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_IDENTIFICATION:
      if (value == NULL)
	return locale->facet_LC_IDENTIFICATION->name;
      {
	struct gl_locale_identification *facet =
	  gl_get_facet_identification (value, locale->facet_LC_CTYPE->encoding);
	if (facet != NULL)
	  {
	    const char *fullname =
	      construct_new_fullname (locale, LC_IDENTIFICATION, facet->name);
	    if (fullname != NULL)
	      {
		locale->facet_LC_IDENTIFICATION = facet;
		locale->fullname = fullname;
		return facet->name;
	      }
	  }
	return NULL;
      }
    case LC_ALL:
      if (value == NULL)
	return locale->fullname;
      {
	struct gl_locale_ctype *facet_LC_CTYPE = gl_get_facet_ctype (value);
	if (facet_LC_CTYPE != NULL)
	  {
	    struct gl_locale_numeric *facet_LC_NUMERIC =
	      gl_get_facet_numeric (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_time *facet_LC_TIME =
	      gl_get_facet_time (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_collate *facet_LC_COLLATE =
	      gl_get_facet_collate (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_monetary *facet_LC_MONETARY =
	      gl_get_facet_monetary (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_messages *facet_LC_MESSAGES =
	      gl_get_facet_messages (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_paper *facet_LC_PAPER =
	      gl_get_facet_paper (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_name *facet_LC_NAME =
	      gl_get_facet_name (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_address *facet_LC_ADDRESS =
	      gl_get_facet_address (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_telephone *facet_LC_TELEPHONE =
	      gl_get_facet_telephone (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_measurement *facet_LC_MEASUREMENT =
	      gl_get_facet_measurement (value, facet_LC_CTYPE->encoding);
	    struct gl_locale_identification *facet_LC_IDENTIFICATION =
	      gl_get_facet_identification (value, facet_LC_CTYPE->encoding);
	    if (facet_LC_NUMERIC != NULL && facet_LC_TIME != NULL
		&& facet_LC_COLLATE != NULL && facet_LC_MONETARY != NULL
		&& facet_LC_MESSAGES != NULL && facet_LC_PAPER != NULL
		&& facet_LC_NAME != NULL && facet_LC_ADDRESS != NULL
		&& facet_LC_TELEPHONE != NULL && facet_LC_MEASUREMENT != NULL
		&& facet_LC_IDENTIFICATION != NULL)
	      {
		const char *fullname =
		  construct_fullname (
		    facet_LC_CTYPE->name,
		    facet_LC_NUMERIC->name,
		    facet_LC_TIME->name,
		    facet_LC_COLLATE->name,
		    facet_LC_MONETARY->name,
		    facet_LC_MESSAGES->name,
		    facet_LC_PAPER->name,
		    facet_LC_NAME->name,
		    facet_LC_ADDRESS->name,
		    facet_LC_TELEPHONE->name,
		    facet_LC_MEASUREMENT->name,
		    facet_LC_IDENTIFICATION->name);
		if (fullname != NULL)
		  {
		    locale->facet_LC_CTYPE = facet_LC_CTYPE;
		    locale->facet_LC_NUMERIC = facet_LC_NUMERIC;
		    locale->facet_LC_TIME = facet_LC_TIME;
		    locale->facet_LC_COLLATE = facet_LC_COLLATE;
		    locale->facet_LC_MONETARY = facet_LC_MONETARY;
		    locale->facet_LC_MESSAGES = facet_LC_MESSAGES;
		    locale->facet_LC_PAPER = facet_LC_PAPER;
		    locale->facet_LC_NAME = facet_LC_NAME;
		    locale->facet_LC_ADDRESS = facet_LC_ADDRESS;
		    locale->facet_LC_TELEPHONE = facet_LC_TELEPHONE;
		    locale->facet_LC_MEASUREMENT = facet_LC_MEASUREMENT;
		    locale->facet_LC_IDENTIFICATION = facet_LC_IDENTIFICATION;
		    locale->fullname = fullname;
		    return locale->fullname;
		  }
	      }
	  }
	return NULL;
      }
    default:
      /* Invalid argument.  */
      abort ();
    }
}
