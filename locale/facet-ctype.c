/* Loading the LC_CTYPE facet of a locale.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "glocale-private.h"

#include <stdlib.h>
#include <string.h>

#include "hash.h"
#include "struniq.h"


gl_lock_define_initialized(static, lock)
static int initialized;
static hash_table facet_ctype_table;

/* Forward declaration of local functions.  */
static struct gl_locale_ctype *
get_facet_ctype_locked (const char *name, const char *name_end);

static inline struct gl_locale_ctype *
create_facet_ctype (const char *name, const char *name_end)
{
  struct gl_locale_ctype *facet;

  if ((name_end - name == 1 && memcmp (name, "C", 1) == 0)
      || (name_end - name == 5 && memcmp (name, "POSIX", 5) == 0))
    return &gl_locale_ctype_C;

  /* Expand locale alias.  */
  if (*name_end == '\0')
    {
      const char *expanded_name = _gl_expand_alias (name);
      if (expanded_name != NULL)
	{
	  const char *expanded_name_end = expanded_name + strlen (expanded_name);
	  return get_facet_ctype_locked (expanded_name, expanded_name_end);
	}
    }

  /* Allocate the result.  */
  facet = (struct gl_locale_ctype *) malloc (sizeof (struct gl_locale_ctype));
  if (facet == NULL)
    return NULL;

  /* Canonicalize the name.  */
  {
    char *name_copy;

    name_copy = (char *) malloc (name_end - name + 1);
    if (name_copy == NULL)
      {
	free (facet);
	return NULL;
      }
    memcpy (name_copy, name, name_end - name);
    name_copy[name_end - name] = '\0';

    name = struniq (name_copy);
  }
  facet->name = name;

  /* Extract the encoding.  */
  {
    const char *encoding = NULL;
    const char *encoding_end = NULL;
    const char *p;

    name_end = name + strlen (name);
    for (p = name; p < name_end; p++)
      if (*p == '.')
	{
	  encoding = ++p;
	  while (*p != '\0' && *p != '@')
	    p++;
	  encoding_end = p;
	}
    if (encoding != NULL && encoding_end - encoding > 0)
      {
	char *encoding_copy;

	encoding_copy = (char *) malloc (encoding_end - encoding + 1);
	if (encoding_copy == NULL)
	  {
	    free (facet);
	    return NULL;
	  }
	memcpy (encoding_copy, encoding, encoding_end - encoding);
	encoding_copy[encoding_end - encoding] = '\0';

	facet->encoding = struniq (encoding_copy);
      }
    else
      /* Encoding defaults to UTF-8 nowadays.  */
      facet->encoding = "UTF-8";
  }

  _gl_init_facet_ctype_encoding_dependents (facet);

  gl_lock_init (facet->data_lock);
  facet->mmaped_file_ctype = NULL;
  facet->array_ctype[CTYPE_ALNUM] = NULL;
  facet->array_ctype[CTYPE_ALPHA] = NULL;
  facet->array_ctype[CTYPE_CNTRL] = NULL;
  facet->array_ctype[CTYPE_DIGIT] = NULL;
  facet->array_ctype[CTYPE_GRAPH] = NULL;
  facet->array_ctype[CTYPE_LOWER] = NULL;
  facet->array_ctype[CTYPE_PRINT] = NULL;
  facet->array_ctype[CTYPE_PUNCT] = NULL;
  facet->array_ctype[CTYPE_SPACE] = NULL;
  facet->array_ctype[CTYPE_UPPER] = NULL;
  facet->array_ctype[CTYPE_XDIGIT] = NULL;
  facet->array_ctype[CTYPE_BLANK] = NULL;
  facet->table_ctype[CTYPE_ALNUM] = NULL;
  facet->table_ctype[CTYPE_ALPHA] = NULL;
  facet->table_ctype[CTYPE_CNTRL] = NULL;
  facet->table_ctype[CTYPE_DIGIT] = NULL;
  facet->table_ctype[CTYPE_GRAPH] = NULL;
  facet->table_ctype[CTYPE_LOWER] = NULL;
  facet->table_ctype[CTYPE_PRINT] = NULL;
  facet->table_ctype[CTYPE_PUNCT] = NULL;
  facet->table_ctype[CTYPE_SPACE] = NULL;
  facet->table_ctype[CTYPE_UPPER] = NULL;
  facet->table_ctype[CTYPE_XDIGIT] = NULL;
  facet->table_ctype[CTYPE_BLANK] = NULL;
  facet->array_trans[TRANS_TOLOWER] = NULL;
  facet->array_trans[TRANS_TOUPPER] = NULL;
  facet->table_trans[TRANS_TOLOWER] = NULL;
  facet->table_trans[TRANS_TOUPPER] = NULL;
  facet->table_width = NULL;

  return facet;
}

static struct gl_locale_ctype *
get_facet_ctype_locked (const char *name, const char *name_end)
{
  size_t len;
  void *found;
  struct gl_locale_ctype *result;

  if (!initialized)
    {
      if (gl_hash_init (&facet_ctype_table, 10) < 0)
	return NULL;
      initialized = 1;
    }
  len = name_end - name;
  if (gl_hash_find_entry (&facet_ctype_table, name, len, &found) == 0)
    result = (struct gl_locale_ctype *) found;
  else
    {
      result = create_facet_ctype (name, name_end);
      if (result != NULL)
	gl_hash_insert_entry (&facet_ctype_table, name, len, result);
    }
  return result;
}

struct gl_locale_ctype *
gl_get_facet_ctype (const char *name)
{
  const char *name_end = name + strlen (name);
  struct gl_locale_ctype *result;

  /* Recognize mixed locale syntax "category=value; ...".  */
  {
    const char *p;

    for (p = name; *p != '\0'; )
      {
	const char *next_semicolon = strchr (p, ';');
	if (next_semicolon == NULL)
	  break;
	if (strncmp (p, "LC_CTYPE=", strlen ("LC_CTYPE=")) == 0)
	  {
	    name = p + strlen ("LC_CTYPE=");
	    name_end = next_semicolon;
	    break;
	  }
	p = next_semicolon + 1;
      }
  }

  gl_lock_lock (lock);
  result = get_facet_ctype_locked (name, name_end);
  gl_lock_unlock (lock);
  return result;
}
