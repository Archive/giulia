/* GNU locale private declarations.
   Copyright (C) 2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2005.

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public License as published
   by the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _GNU_LOCALE_PRIVATE_H
#define _GNU_LOCALE_PRIVATE_H

#include <glocale/config.h>
#include <glocale/locale.h>
#if GLOCALE_ENABLE_WIDE_CHAR
# include <wchar.h>
#endif
#include <iconv.h>

#include "lock.h"
#include "tls.h"

#define ASSERT(x) ((x) ? 0 : (abort (), 0))

#if GLOCALE_ENABLE_WIDE_CHAR

/* Maximum number of bytes needed for a multi-byte character.
   4 is enough for all known stateless encodings (cf. gl_mb_cur_max).  */
#define GL_MB_LEN_MAX 4

/* Number of bytes in a gl_mbstate_t.
   We max with GL_MB_LEN_MAX because some systems have a huge mbstate_t.  */
#define MBSTATE_SIZE  \
  (sizeof (mbstate_t) < GL_MB_LEN_MAX ? sizeof (mbstate_t) : GL_MB_LEN_MAX)

/* The part of a mbstate_t that we make use of.  */
typedef struct { char mbuf[MBSTATE_SIZE]; } gl_mbstate_t;
/* The byte count is stored in the last byte.  */
#define mbcount mbuf[MBSTATE_SIZE-1]

struct gl_thread_specific
{
  gl_mbstate_t state_for_mbrtowc;
  gl_mbstate_t state_for_mbrlen;
  gl_mbstate_t state_for_mbsrtowcs;
  gl_mbstate_t state_for_mbsnrtowcs;
  gl_mbstate_t state_for_wcrtomb;
  gl_mbstate_t state_for_wcsrtombs;
  gl_mbstate_t state_for_wcsnrtombs;
};

#endif

struct glocale
{
  struct gl_locale_ctype *          facet_LC_CTYPE;
  struct gl_locale_numeric *        facet_LC_NUMERIC;
  struct gl_locale_time *           facet_LC_TIME;
  struct gl_locale_collate *        facet_LC_COLLATE;
  struct gl_locale_monetary *       facet_LC_MONETARY;
  struct gl_locale_messages *       facet_LC_MESSAGES;
  struct gl_locale_paper *          facet_LC_PAPER;
  struct gl_locale_name *           facet_LC_NAME;
  struct gl_locale_address *        facet_LC_ADDRESS;
  struct gl_locale_telephone *      facet_LC_TELEPHONE;
  struct gl_locale_measurement *    facet_LC_MEASUREMENT;
  struct gl_locale_identification * facet_LC_IDENTIFICATION;
  const char *fullname;

#if GLOCALE_ENABLE_WIDE_CHAR
  gl_lock_define(, thread_specific_lock) /* protects the initialization of thread_specific_key */
  volatile int thread_specific_initialized;
  gl_tls_key_t thread_specific_key;
  struct gl_thread_specific thread_specific_fallback;
#endif
};

/* Tables for <glocale/ctype.h> and <glocale/wctype.h> functionality.  */
typedef const unsigned int *ctype_bitarray; /* indexed by an 'unsigned char' */
typedef const char *wctype_table; /* 3-level bit table */
typedef const unsigned char *trans_array; /* indexed by an 'unsigned char' */
typedef const char *wctrans_table; /* 3-level table of 'short' */
typedef const char *wcwidth_table; /* 3-level table of 'signed char` */

/* Predefined ctype and wctype properties.  */
#define CTYPE_ALNUM   0
#define CTYPE_ALPHA   1
#define CTYPE_CNTRL   2
#define CTYPE_DIGIT   3
#define CTYPE_GRAPH   4
#define CTYPE_LOWER   5
#define CTYPE_PRINT   6
#define CTYPE_PUNCT   7
#define CTYPE_SPACE   8
#define CTYPE_UPPER   9
#define CTYPE_XDIGIT 10
#define CTYPE_BLANK  11
#define NUM_CTYPE 12

/* Predefined wctrans mappings.  */
#define TRANS_TOLOWER 0
#define TRANS_TOUPPER 1
#define NUM_TRANS 2

struct gl_locale_ctype
{
  const char *name;
  const char *encoding;

  gl_lock_define(, iconv_to_lock) /* protects the use of cd_to_utf8 */
  gl_lock_define(, iconv_from_lock) /* protects the use of cd_from_utf8 */
  iconv_t cd_to_utf8;
  iconv_t cd_from_utf8;
  int is_utf8;
  size_t mb_cur_max;
  size_t (*mbtouc) (unsigned int *puc, const char *s, size_t n, size_t nmin, iconv_t cd);
  size_t (*uctomb) (char *s, unsigned int uc, size_t n, iconv_t cd);

  gl_lock_define(, data_lock) /* protects the initialization of the volatile fields */
  const struct file_ctype * volatile mmaped_file_ctype;
  ctype_bitarray volatile array_ctype[NUM_CTYPE];
  wctype_table volatile table_ctype[NUM_CTYPE];
  trans_array volatile array_trans[NUM_TRANS];
  wctrans_table volatile table_trans[NUM_TRANS];
  wcwidth_table volatile table_width;
};
extern struct gl_locale_ctype gl_locale_ctype_C;

struct gl_locale_numeric
{
  const char *name;
};
extern struct gl_locale_numeric gl_locale_numeric_C;

struct gl_locale_time
{
  const char *name;
};
extern struct gl_locale_time gl_locale_time_C;

struct gl_locale_collate
{
  const char *name;
};
extern struct gl_locale_collate gl_locale_collate_C;

struct gl_locale_monetary
{
  const char *name;
};
extern struct gl_locale_monetary gl_locale_monetary_C;

struct gl_locale_messages
{
  const char *name;
};
extern struct gl_locale_messages gl_locale_messages_C;

struct gl_locale_paper
{
  const char *name;
};
extern struct gl_locale_paper gl_locale_paper_C;

struct gl_locale_name
{
  const char *name;
};
extern struct gl_locale_name gl_locale_name_C;

struct gl_locale_address
{
  const char *name;
};
extern struct gl_locale_address gl_locale_address_C;

struct gl_locale_telephone
{
  const char *name;
};
extern struct gl_locale_telephone gl_locale_telephone_C;

struct gl_locale_measurement
{
  const char *name;
};
extern struct gl_locale_measurement gl_locale_measurement_C;

struct gl_locale_identification
{
  const char *name;
};
extern struct gl_locale_identification gl_locale_identification_C;

extern const char * _gl_expand_alias (const char *name);

extern void _gl_init_facet_ctype_encoding_dependents (struct gl_locale_ctype *facet);
extern void _gl_fini_facet_ctype_encoding_dependents (struct gl_locale_ctype *facet);
extern struct gl_locale_ctype * gl_get_facet_ctype (const char *name);
extern struct gl_locale_numeric * gl_get_facet_numeric (const char *name, const char *encoding);
extern struct gl_locale_time * gl_get_facet_time (const char *name, const char *encoding);
extern struct gl_locale_collate * gl_get_facet_collate (const char *name, const char *encoding);
extern struct gl_locale_monetary * gl_get_facet_monetary (const char *name, const char *encoding);
extern struct gl_locale_messages * gl_get_facet_messages (const char *name, const char *encoding);
extern struct gl_locale_paper * gl_get_facet_paper (const char *name, const char *encoding);
extern struct gl_locale_name * gl_get_facet_name (const char *name, const char *encoding);
extern struct gl_locale_address * gl_get_facet_address (const char *name, const char *encoding);
extern struct gl_locale_telephone * gl_get_facet_telephone (const char *name, const char *encoding);
extern struct gl_locale_measurement * gl_get_facet_measurement (const char *name, const char *encoding);
extern struct gl_locale_identification * gl_get_facet_identification (const char *name, const char *encoding);

/* Cache of file lookup results.  */
struct file_cache
{
  gl_lock_define(, lock) /* protects results */
  struct loaded_l10nfile *results;
};
#ifdef gl_lock_initializer
# define FILE_CACHE_INITIALIZER  { gl_lock_initializer, NULL }
#else
# define FILE_CACHE_INITIALIZER  { NULL }
#endif
/* Load a machine-dependent file into read-only memory and return a pointer
   to its contents.  The PATHNAME is relative to the locale's directory.  */
extern const void * _gl_load_file (struct file_cache *cache, const char *pathname, const char *localename);

extern size_t gl_strnlen1 (const char *string, size_t maxlen);

extern const char * _gl_dcgettext (const char *domainname, const char *msgid, int category,
				   const char *localename, const char *encoding);
extern const char * _gl_dcngettext (const char *domainname, const char *msgid1, const char *msgid2, unsigned long int n, int category,
				    const char *localename, const char *encoding);

extern unsigned int _gl_btowc (unsigned char c, gl_locale_t locale);
extern int _gl_wctob (unsigned int c, gl_locale_t locale);

/* Convert a single multibyte character to Unicode.
   Assumes puc != NULL, s != NULL, n > 0.
   It is known that the multibyte character has at least nmin bytes,
   0 < nmin <= n.
   Return value is the number of bytes or (size_t)(-1) or (size_t)(-2), like
   for mbrtowc(), except no special return value for the U+0000 character.  */
extern size_t _gl_mbtouc_ascii (unsigned int *puc, const char *s, size_t n, size_t nmin, iconv_t);
extern size_t _gl_mbtouc_utf8 (unsigned int *puc, const char *s, size_t n, size_t nmin, iconv_t);
/* The caller must lock cd against concurrent access.  */
extern size_t _gl_mbtouc_iconv (unsigned int *puc, const char *s, size_t n, size_t nmin, iconv_t cd);

/* Convert a single Unicode character to multibyte.
   Assumes s != NULL, n > 0.
   It is known that room for n bytes is available at s.
   Return value is the number of bytes or (size_t)(-1) or (size_t)(-2).  */
extern size_t _gl_uctomb_ascii (char *s, unsigned int uc, size_t n, iconv_t);
extern size_t _gl_uctomb_utf8 (char *s, unsigned int uc, size_t n, iconv_t);
/* The caller must lock cd against concurrent access.  */
extern size_t _gl_uctomb_iconv (char *s, unsigned int uc, size_t n, iconv_t cd);

#if GLOCALE_ENABLE_WIDE_CHAR
extern struct gl_thread_specific * _gl_thread_specific (gl_locale_t locale);
#endif

/* Looks up a translation in a message catalog.
   The default_value is taken from share/locale/C/<category>/glocale.mo.  */
extern const char * _gl_defaulted_dcgettext (const char *msgid, const char *default_value, int category, gl_locale_t locale);

extern const struct file_ctype * _gl_init_file_ctype_locked (struct gl_locale_ctype *facet);

extern wctype_table _gl_init_ctype_table_locked (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);
extern wctype_table _gl_init_ctype_table (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);
extern ctype_bitarray _gl_init_ctype_array (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);

/* Unicode character properties.  */
#if GLOCALE_ENABLE_WIDE_CHAR
/* Use <glocale/wctype.h>.  */
# define _gl_isualnum gl_iswalnum
# define _gl_isualpha gl_iswalpha
# define _gl_isucntrl gl_iswcntrl
# define _gl_isudigit gl_iswdigit
# define _gl_isugraph gl_iswgraph
# define _gl_isulower gl_iswlower
# define _gl_isuprint gl_iswprint
# define _gl_isupunct gl_iswpunct
# define _gl_isuspace gl_iswspace
# define _gl_isuupper gl_iswupper
# define _gl_isuxdigit gl_iswxdigit
# define _gl_isublank gl_iswblank
#else
extern int _gl_isualnum (unsigned int uc, gl_locale_t locale);
extern int _gl_isualpha (unsigned int uc, gl_locale_t locale);
extern int _gl_isucntrl (unsigned int uc, gl_locale_t locale);
extern int _gl_isudigit (unsigned int uc, gl_locale_t locale);
extern int _gl_isugraph (unsigned int uc, gl_locale_t locale);
extern int _gl_isulower (unsigned int uc, gl_locale_t locale);
extern int _gl_isuprint (unsigned int uc, gl_locale_t locale);
extern int _gl_isupunct (unsigned int uc, gl_locale_t locale);
extern int _gl_isuspace (unsigned int uc, gl_locale_t locale);
extern int _gl_isuupper (unsigned int uc, gl_locale_t locale);
extern int _gl_isuxdigit (unsigned int uc, gl_locale_t locale);
extern int _gl_isublank (unsigned int uc, gl_locale_t locale);
#endif

extern wctrans_table _gl_init_trans_table_locked (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);
extern wctrans_table _gl_init_trans_table (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);
extern trans_array _gl_init_trans_array (struct gl_locale_ctype *facet, int index, const char *name, gl_locale_t locale);

/* Unicode character mappings.  */
#if GLOCALE_ENABLE_WIDE_CHAR
/* Use <glocale/wctype.h>.  */
# define _gl_toulower gl_towlower
# define _gl_touupper gl_towupper
#else
extern unsigned int _gl_toulower (unsigned int uc, gl_locale_t locale);
extern unsigned int _gl_touupper (unsigned int uc, gl_locale_t locale);
#endif

extern wcwidth_table _gl_init_width_table (struct gl_locale_ctype *facet, gl_locale_t locale);

/* Unicode character width.  */
#if GLOCALE_ENABLE_WIDE_CHAR
/* Use <glocale/wchar.h>.  */
# define _gl_ucwidth gl_wcwidth
#else
extern int _gl_ucwidth (unsigned int c, gl_locale_t locale);
#endif

/* Return an error message for a given errno value, or NULL if not found in
   the message catalog.  */
extern const char * _gl_strerror_aux (int errnum, gl_locale_t locale);

#endif /* _GNU_LOCALE_PRIVATE_H */
