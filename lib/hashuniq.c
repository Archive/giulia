/* hash - implement simple hashed uniquifying table with string based keys.
   Copyright (C) 1994-1995, 2000-2005 Free Software Foundation, Inc.
   Written by Ulrich Drepper <drepper@gnu.ai.mit.edu>, October 1994.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#if HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "hashuniq.h"

#include <stdlib.h> 
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <sys/types.h>

#include "obstack.h"


#define gl_obstack_chunk_alloc malloc
#define gl_obstack_chunk_free free

typedef struct hashuniq_entry
{
  unsigned long used;
  void *key;
  size_t keylen;
  struct hashuniq_entry *next;
}
hashuniq_entry;

/* Forward declaration of local functions.  */
static void insert_entry_2 (hashuniq_table *htab,
			    void *key, size_t keylen,
			    unsigned long int hval, size_t idx);
static int resize (hashuniq_table *htab);
static size_t lookup (hashuniq_table *htab,
		      const void *key, size_t keylen,
		      unsigned long int hval);
static unsigned long compute_hashval (const void *key, size_t keylen);
static unsigned long int next_prime (unsigned long int seed);
static int is_prime (unsigned long int candidate);


int
gl_hashuniq_init (hashuniq_table *htab, unsigned long int init_size)
{
  /* We need the size to be a prime.  */
  init_size = next_prime (init_size);

  /* Initialize the data structure.  */
  htab->size = init_size;
  htab->filled = 0;
  htab->first = NULL;
  htab->table = calloc (init_size + 1, sizeof (hashuniq_entry));
  if (htab->table == NULL)
    return -1;

  gl_obstack_init (&htab->mem_pool);

  return 0;
}


int
gl_hashuniq_destroy (hashuniq_table *htab)
{
  free (htab->table);
  gl_obstack_free (&htab->mem_pool);
  return 0;
}


int
gl_hashuniq_find_entry (hashuniq_table *htab, const void *key, size_t keylen,
			void **result)
{
  hashuniq_entry *table = (hashuniq_entry *) htab->table;
  size_t idx = lookup (htab, key, keylen, compute_hashval (key, keylen));

  if (table[idx].used == 0)
    return -1;

  *result = table[idx].key;
  return 0;
}


int
gl_hashuniq_find_or_insert_entry (hashuniq_table *htab,
				  void *key, size_t keylen,
				  void **result)
{
  unsigned long int hval = compute_hashval (key, keylen);
  hashuniq_entry *table = (hashuniq_entry *) htab->table;
  size_t idx = lookup (htab, key, keylen, hval);

  if (table[idx].used)
    {
      /* Found the key.  */
      *result = table[idx].key;
      return 0;
    }
  else
    {
      /* An empty bucket has been found.
	 Here we could just use the key itself, and don't use the mem_pool
	 at all.  But moving the key into the mem_pool has a compaction
	 effect and thus improves the locality of references during later
	 lookups.  */
      void *keycopy = gl_obstack_copy (&htab->mem_pool, key, keylen);

      if (keycopy == NULL)
	{
	  /* Memory allocation failure.  */
	  *result = key;
	  return -1;
	}

      *result = keycopy;
      insert_entry_2 (htab, keycopy, keylen, hval, idx);
      if (100 * htab->filled > 75 * htab->size)
	/* Table is filled more than 75%.  Resize the table.  */
	if (resize (htab) < 0)
	  /* Memory allocation failure.  */
	  return -1;

      return 0;
    }
}

static void
insert_entry_2 (hashuniq_table *htab,
		void *key, size_t keylen,
		unsigned long int hval, size_t idx)
{
  hashuniq_entry *table = (hashuniq_entry *) htab->table;

  table[idx].used = hval;
  table[idx].key = key;
  table[idx].keylen = keylen;

  /* List the new value in the list.  */
  if ((hashuniq_entry *) htab->first == NULL)
    {
      table[idx].next = &table[idx];
      *(hashuniq_entry **) &htab->first = &table[idx];
    }
  else
    {
      table[idx].next = ((hashuniq_entry *) htab->first)->next;
      ((hashuniq_entry *) htab->first)->next = &table[idx];
      *(hashuniq_entry **) &htab->first = &table[idx];
    }

  ++htab->filled;
}


static int
resize (hashuniq_table *htab)
{
  unsigned long int old_size = htab->size;
  hashuniq_entry *table = (hashuniq_entry *) htab->table;
  unsigned long int new_size = next_prime (htab->size * 2);
  void *new_table = calloc (new_size + 1, sizeof (hashuniq_entry));
  size_t idx;

  if (new_table == NULL)
    /* Memory allocation failure.  */
    return -1;

  htab->size = new_size;
  htab->filled = 0;
  htab->first = NULL;
  htab->table = new_table;

  for (idx = 1; idx <= old_size; ++idx)
    if (table[idx].used)
      insert_entry_2 (htab, table[idx].key, table[idx].keylen,
		      table[idx].used,
		      lookup (htab, table[idx].key, table[idx].keylen,
			      table[idx].used));

  free (table);

  return 0;
}


int
gl_hashuniq_iterate (hashuniq_table *htab, void **ptr, void **key, size_t *keylen)
{
  if (*ptr == NULL)
    {
      if (htab->first == NULL)
	return -1;
      *ptr = (void *) ((hashuniq_entry *) htab->first)->next;
    }
  else
    {
      if (*ptr == htab->first)
	return -1;
      *ptr = (void *) (((hashuniq_entry *) *ptr)->next);
    }

  *key = ((hashuniq_entry *) *ptr)->key;
  *keylen = ((hashuniq_entry *) *ptr)->keylen;
  return 0;
}


/* References:
   [Aho,Sethi,Ullman] Compilers: Principles, Techniques and Tools, 1986
   [Knuth]	      The Art of Computer Programming, part3 (6.4) */

static size_t
lookup (hashuniq_table *htab,
	const void *key, size_t keylen,
	unsigned long int hval)
{
  unsigned long int hash;
  size_t idx;
  hashuniq_entry *table = (hashuniq_entry *) htab->table;

  /* First hash function: simply take the modul but prevent zero.  */
  hash = 1 + hval % htab->size;

  idx = hash;

  if (table[idx].used)
    {
      if (table[idx].used == hval && table[idx].keylen == keylen
	  && memcmp (table[idx].key, key, keylen) == 0)
	return idx;

      /* Second hash function as suggested in [Knuth].  */
      hash = 1 + hval % (htab->size - 2);

      do
	{
	  if (idx <= hash)
	    idx = htab->size + idx - hash;
	  else
	    idx -= hash;

	  /* If entry is found use it.  */
	  if (table[idx].used == hval && table[idx].keylen == keylen
	      && memcmp (table[idx].key, key, keylen) == 0)
	    return idx;
	}
      while (table[idx].used);
    }
  return idx;
}


static unsigned long
compute_hashval (const void *key, size_t keylen)
{
  size_t cnt;
  unsigned long int hval;

  /* Compute the hash value for the given string.  The algorithm
     is taken from [Aho,Sethi,Ullman].  */
  cnt = 0;
  hval = keylen;
  while (cnt < keylen)
    {
      hval = (hval << 9) | (hval >> (sizeof (unsigned long) * CHAR_BIT - 9));
      hval += (unsigned long int) *(((const char *) key) + cnt++);
    }
  return hval != 0 ? hval : ~((unsigned long) 0);
}


static unsigned long
next_prime (unsigned long int seed)
{
  /* Make it definitely odd.  */
  seed |= 1;

  while (!is_prime (seed))
    seed += 2;

  return seed;
}


static int
is_prime (unsigned long int candidate)
{
  /* No even number and none less than 10 will be passed here.  */
  unsigned long int divn = 3;
  unsigned long int sq = divn * divn;

  while (sq < candidate && candidate % divn != 0)
    {
      ++divn;
      sq += 4 * divn;
      ++divn;
    }

  return candidate % divn != 0;
}
