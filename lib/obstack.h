/* Simplified object stack.
   Copyright (C) 2004 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2004.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _OBSTACK_H
#define _OBSTACK_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

struct obstack
{
  void *chunk_list;
  char *chunk_ptr;
  char *chunk_limit;
  void * (*allocfun) (size_t);
  void (*freefun) (void *);
};

/* Initialize an obstack.  */
#define gl_obstack_init(obstackp) \
  ((obstackp)->chunk_list = NULL,			\
   (obstackp)->chunk_ptr = NULL,			\
   (obstackp)->chunk_limit = NULL,			\
   (obstackp)->allocfun = (gl_obstack_chunk_alloc),	\
   (obstackp)->freefun = (gl_obstack_chunk_free),	\
   0)

/* Initializer for a statically allocated obstack.  */
#define GL_OBSTACK_INITIALIZER(gl_obstack_chunk_alloc, gl_obstack_chunk_free) \
  { NULL, NULL, NULL, gl_obstack_chunk_alloc, gl_obstack_chunk_free }

/* Add a piece of data to an obstack.
   Return NULL upon memory allocation failure.  */
extern void * gl_obstack_copy (struct obstack *obstackp,
			       const void *data, size_t datasize);

/* Free the contents of an obstack.  */
extern void gl_obstack_free (struct obstack *obstackp);

#ifdef __cplusplus
}
#endif

#endif /* _OBSTACK_H */
