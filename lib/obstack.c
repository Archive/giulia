/* Simplified object stack.
   Copyright (C) 2004-2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2004.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#if HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "obstack.h"

#include <string.h>


struct chunk
{
  struct chunk *next;
  /* char data[]; */
};

/* Allocations smaller than this are cumulated into a single chunk, as long
   as the chunk has room.
   Allocations larger than this cause a chunk of its own to be used.
   This threshold is chosen so that a chunk carrying this much data fits
   into a single data page, including the chunk's header and the malloc()
   overhead.  */
#define SIZE_THRESHOLD  4050

void *
gl_obstack_copy (struct obstack *obstackp, const void *data, size_t datasize)
{
  if (datasize < SIZE_THRESHOLD)
    {
      if (obstackp->chunk_ptr != NULL
	  && obstackp->chunk_limit - obstackp->chunk_ptr >= datasize)
	{
	  /* Take room from the current chunk.  */
	  void *room = obstackp->chunk_ptr;
	  obstackp->chunk_ptr += datasize;
	  memcpy (room, data, datasize);
	  return room;
	}
      else
	{
	  /* Start a new chunk.  */
	  size_t chunksize = SIZE_THRESHOLD;
	  void *memory = obstackp->allocfun (sizeof (struct chunk) + chunksize);
	  if (memory != NULL)
	    {
	      ((struct chunk *)memory)->next = obstackp->chunk_list;
	      obstackp->chunk_list = (struct chunk *)memory;
	      obstackp->chunk_ptr = (char *)&((struct chunk *)memory)[1] + datasize;
	      obstackp->chunk_limit = (char *)&((struct chunk *)memory)[1] + chunksize;
	      memcpy (&((struct chunk *)memory)[1], data, datasize);
	      return &((struct chunk *)memory)[1];
	    }
	  else
	    return NULL;
	}
    }
  else
    {
      /* A chunk of its own.  */
      void *memory = obstackp->allocfun (sizeof (struct chunk) + datasize);
      if (memory != NULL)
	{
	  ((struct chunk *)memory)->next = obstackp->chunk_list;
	  obstackp->chunk_list = (struct chunk *)memory;
	  memcpy (&((struct chunk *)memory)[1], data, datasize);
	  return &((struct chunk *)memory)[1];
	}
      else
	return NULL;
    }
}

void
gl_obstack_free (struct obstack *obstackp)
{
  struct chunk *list = (struct chunk *) obstackp->chunk_list;

  while (list != NULL)
    {
      struct chunk *rest = list->next;
      obstackp->freefun (list);
      list = rest;
    }
}
