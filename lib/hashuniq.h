/* Copyright (C) 1995, 2000-2005 Free Software Foundation, Inc.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the GNU C Library; see the file COPYING.LIB.  If
   not, write to the Free Software Foundation, Inc., 51 Franklin Street,
   Fifth Floor, Boston, MA 02110-1301, USA.  */

#ifndef _HASH_H
#define _HASH_H

#include "obstack.h"

#ifdef __cplusplus
extern "C" {
#endif

/* A hashuniq_table is like a hash_table, except the value is by definition
   always the same as the key.  To save memory, the table stores only the
   keys; the value is implicit.  */

typedef struct hashuniq_table
{
  unsigned long int size;
  unsigned long int filled;
  void *first;
  void *table;
  struct obstack mem_pool;
}
hashuniq_table;

extern int gl_hashuniq_init (hashuniq_table *htab, unsigned long int init_size);
extern int gl_hashuniq_destroy (hashuniq_table *htab);
extern int gl_hashuniq_find_entry (hashuniq_table *htab,
				   const void *key, size_t keylen,
				   void **result);
extern int gl_hashuniq_find_or_insert_entry (hashuniq_table *htab,
					     void *key, size_t keylen,
					     void **result);

extern int gl_hashuniq_iterate (hashuniq_table *htab, void **ptr,
				void **key, size_t *keylen);

#ifdef __cplusplus
}
#endif

#endif /* not _HASH_H */
