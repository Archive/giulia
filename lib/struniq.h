/* Uniquifying strings.
   Copyright (C) 2004 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2004.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#ifndef _STRUNIQ_H
#define _STRUNIQ_H

/* Given a freshly allocated string, struniq returns a canonical copy of
   this string.  Either struniq was already called with an equal string,
   then the earlier struniq result is returned, and the argument string is
   freed.  Or the argument string is remembered for future struniq calls.
   Upon memory allocation failure, return the argument string without
   remembering it for later.  */
extern const char * struniq (char *string);

#endif /* _STRUNIQ_H */
