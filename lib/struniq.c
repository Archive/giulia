/* Uniquifying strings.
   Copyright (C) 2004-2005 Free Software Foundation, Inc.
   Written by Bruno Haible <bruno@clisp.org>, 2004.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2, or (at
   your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301,
   USA.  */

#if HAVE_CONFIG_H
# include <config.h>
#endif

/* Specification.  */
#include "struniq.h"

#include <stdlib.h>
#include <string.h>

#include "hashuniq.h"
#include "lock.h"


gl_lock_define_initialized(static, lock)
static int initialized;
static hashuniq_table uniq_table;

static inline const char *
struniq_locked (char *string)
{
  size_t len;
  void *found;

  if (!initialized)
    {
      if (gl_hashuniq_init (&uniq_table, 10) < 0)
	return string;
      initialized = 1;
    }
  len = strlen (string) + 1;
  if (gl_hashuniq_find_entry (&uniq_table, string, len, &found) == 0)
    {
      /* Found an earlier equal string.  Return it and free this one.  */
      free (string);
      return (const char *) found;
    }
  else
    {
      /* First time use of this string.  Put it into the table.  */
      gl_hashuniq_find_or_insert_entry (&uniq_table, string, len, &found);
      if (found != string)
	free (string);
      return (const char *) found;
    }
}

const char *
struniq (char *string)
{
  const char *result;

  gl_lock_lock (lock);
  result = struniq_locked (string);
  gl_lock_unlock (lock);
  return result;
}
